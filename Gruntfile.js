module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);


    var srcFiles = 'src/*.js',
        widgetsSrcFiles = 'src/widgets/*.js',
        componentsSrcFiles = 'src/components/*.js',
        initializersSrcFiles = 'src/widgets/initializers/*.js';

    grunt.initConfig({


        babel: {
            options: {
                sourceMap: false
            },
            
            dist: {
                 files: [{
                    expand: true,
                    cwd: 'src/',
                    src: ['*.js'],
                    dest: 'dist/',
                    ext: '.js'
                }]
            },
     
            dist_initializers: {
                 files: [{
                    expand: true,
                    cwd: 'src/widgets/initializers',
                    src: ['*.js'],
                    dest: 'dist/widgets/initializers',
                    ext: '.js'
                }]
            }
           
        },

        sass: {
            options: {
                sourceMap: false,
                style: 'expanded'
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: "src/css",
                        src: ["*.scss"],
                        dest: "dist/css",
                        ext: ".css"
                    }
                ]
            }
        },

        watch: {
          ws: {
              files: srcFiles,
              tasks: ['browserify:bundle_ws']
          },

          components: {
              files: componentsSrcFiles,
              tasks: ['browserify:bundle_components']
          },
          
          widgets: {
              files: widgetsSrcFiles,
              tasks: ['browserify:standalone_widgets', 'browserify:bundle_widgets']
              //tasks: ['newer:mycustomtask']
          },

          es_initializers: {
              files: initializersSrcFiles,
              tasks: ['newer:babel:dist_initializers']
          },
          sass: {
              files: "src/css/*.scss",
              tasks: ['newer:sass:dist']
          }
        },
        browserify: {

          bundle_components: {
            options: {
              transform: [ require('grunt-react').browserify, 'debowerify', 'decomponentify', 'deamdify', 'deglobalify' ]
            },
            files: {
              'dist/bundles/components.bundle.js' :'src/mmcafereact/globalize/Components.js'
            }
          },

          bundle_widgets: {
            options: {
              transform: [ require('grunt-react').browserify, 'debowerify', 'decomponentify', 'deamdify', 'deglobalify' ]
            },
            files: {
              'dist/bundles/widgets.bundle.js' :'src/mmcafereact/globalize/Widgets.js'
            }
          },

          bundle_ws: {
            options: {
              transform: [ require('grunt-react').browserify, 'debowerify', 'decomponentify', 'deamdify', 'deglobalify' ]
            },
            files: {
              'dist/bundles/ws.bundle.js' :'src/mmcafereact/globalize/WS.js'
            }
          },

          standalone_widgets: {
            options: {
              transform: [ require('grunt-react').browserify, 'debowerify', 'decomponentify', 'deamdify', 'deglobalify' ]
            },
            files: [{
                  expand: true,
                  cwd: 'src/mmcafereact/globalize/widgets',
                  src: ['*.js'],
                  dest: 'dist/widgets',
                  ext: '.js'
              }]
          }

        },

        'http-server': {
 
          'dev': {
   
              root: "./",
              port: 8000,
              ext: "*",
   
              runInBackground: true,
   
          }
   
      }
    });




    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-http-server');

    //grunt.registerTask('default', ['watch']);

    grunt.registerTask("default", ['browserify', 'http-server', 'watch']);

    grunt.registerTask('build', ['babel' , 'buildStandaloneComponents', 'browserify']);

};