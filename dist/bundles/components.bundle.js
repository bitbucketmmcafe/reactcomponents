(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

/**
 * Componente para accordion
 */
var Accordion = React.createClass({displayName: "Accordion",

    propTypes: {
        /**
         * Array com items do accordion, cada lista é um json: data = [ { label:"nome do accordeon", title:"título dentro do accordeon", external: false, thumb: "http://urldeimagem", description: "texto adicional", date: "2015-09-01T00:00:00Z", "info":array } ]
         * O array info espera o seguinte json {name:'titulo', date:'data', link:'url', description:'texto descritivo', observation:'texto', publishedBy:'publicador/organizador', guests:'convidados/texto', external: 'boolean para link externo ou interno' }
         */
        items: React.PropTypes.array,
        /**
         * id para identificar o accordion
         */
        id: React.PropTypes.string
    },


    
	render: function() {

        var heading = this.props.items.map(function(item, index) {
            return  React.createElement("div", {className: "panel-heading"}, 
                        React.createElement("h4", {className: "panel-title"}, 
                            React.createElement("a", {"data-toggle": "collapse", "data-parent": "#list_"+item.parent_id, "data-target":  "#collapse_" + item.parent_id+"_"+item.key, dangerouslySetInnerHTML: { __html: item.label}, className: "collapsed"})
                        )
                    )
        });

        var items = this.props.items.map(function(item, index) {

            var span = null, thumb = null, datelabel = null, bodyItems = null, title = null, target = item.external ? "_blank" : "";
            if(item.description)
                span = React.createElement("span", {dangerouslySetInnerHTML: { __html: item.description}});

            if(item.thumb)
                thumb = React.createElement("img", {src:  item.thumb});

            if(item.date) {
                datelabel = React.createElement(MMCafeReact.Components.DateLabel, {iso8061date:  item.date, pattern:  item.date_pattern || "DD/MM/YYYY"})
            }

            if(item.title) {
                title = React.createElement("a", {href:  bodyItem.link, target:  target, dangerouslySetInnerHTML: { __html: item.title}, title:  item.title});
            }

            if(item.info){
                
                bodyItems = item.info.map(function(bodyItem, b_index) {
                    var info_date = null, info_link = null, info_description = null, info_observation = null, info_publishedBy = null, info_guests = null,  target = bodyItem.external ? "_blank" : "";
                    
                    if(bodyItem.date)
                        info_date = React.createElement("p", {dangerouslySetInnerHTML: { __html: bodyItem.date}});
                    
                    if(bodyItem.link)
                        info_link = React.createElement("a", {href:  bodyItem.link, target:  target, dangerouslySetInnerHTML: { __html: bodyItem.name}, title:  bodyItem.name});
                    
                    if(bodyItem.description)
                        info_description = React.createElement("p", {className: "description", dangerouslySetInnerHTML: { __html: bodyItem.description}});
                    
                    if(bodyItem.observation)
                        info_description = React.createElement("p", {className: "observation", dangerouslySetInnerHTML: { __html: bodyItem.observation}});
                    
                    if(bodyItem.publishedBy && bodyItem.publishedBy.indexOf("null") <= -1)
                        info_publishedBy = React.createElement("p", {className: "publishedBy", dangerouslySetInnerHTML: { __html: bodyItem.publishedBy}});
                    
                    if(bodyItem.guests && bodyItem.guests.indexOf("null") <= -1)
                        info_guests = React.createElement("p", {className: "guests", dangerouslySetInnerHTML: { __html: bodyItem.guests}});
                                        
                    return React.createElement("div", {key:  "info_" + b_index}, 
                                info_date, 
                                info_link, 
                                info_publishedBy, 
                                info_description, 
                                info_guests, 
                                info_observation
                            );
                });
            }

        	
            return React.createElement("div", {key:  "item_" + index, id: "collapse_" + item.parent_id+"_"+item.key, className: "panel-collapse collapse "}, 
                        React.createElement("div", {className: "panel-body"}, 
                             bodyItems==null &&
                                React.createElement("div", null, 
                                     thumb, 
                                     datelabel, 
                                     title, 
                                     span 
                                ), 
                            
                            bodyItems
                        )
                    )
        });
        return React.createElement("div", {className: "panel panel-default"}, 
                heading, 
                items
                );

	}

});

module.exports = Accordion;

},{}],2:[function(require,module,exports){
/**
 * Component para autocomplete de usuário
 */

var AutocompleteUser = React.createClass({displayName: "AutocompleteUser",

	propTypes: {
		
		//source: React.PropTypes.object,
		
	},

    _randString: function(x){
        var s = "";
        while(s.length<x&&x>0){
            var r = Math.random();
            s+= (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
        }
        return s;
    },

    innerId: null,

	timeout: null,

	_keyUp: function(event) {
        if(this.props.disableautocomplete)
            return false;

    	var value = event.target.value;

        var input = this.refs.input.getDOMNode();
        console.log("1 - "+ input)

    	var self = this;
        var userClick = this._userClick;

        PubSub.publish("components.loader.add", { key: "componente.autocompleteuser.search" } );

        if(this.props.pubsub && this.props.pubsub)
            PubSub.publishSync( this.props.pubsub.keyupinput, { value: value } );
            console.log("2 - "+ key)

    	if(value == "") {
    		clearTimeout(this.timeout);
    		this.setState({ suggestions: [] });
    		event.stopPropagation();
    	}

    	clearTimeout(this.timeout);

    	this.timeout = setTimeout(function() {
            var query = "";

            if(self.props.contextualSearch && self.props.business_fields==false){
              query += "pageSize=3&search=true&autocomplete=true&keywords=" + value + "&searchFieldsUser=" + self.props.sourceParams2;
                console.log("3 -" + query)
                console.log("4 -" + value)
              
            }else{
                
                var pars = self.props.sourceParams();

                for(key in pars) {
                    if(pars[key]!=null)
                        query+= key + "=" + pars[key] + "&"                        
                }

                if(self.props.contextualSearch && self.props.business_fields==true){
                    query += "pageSize=3&search=true&autocomplete=true&keywords=" + value + "&searchFieldsUser=" + self.props.sourceParams2;
                    
                }else{
                    query += "keywords=" + value  + "&searchFieldsUser=" + self.props.sourceParams2;
                  
                }

            }

    		$
	    	.ajax({ url: self.props.source + query })
	    	.then(function(response) {
                $("#mm-autocomplete-clone")
                .css({
                    top: $(input).offset().top + $(input).outerHeight(),
                    left: $(input).offset().left,
                    width: $(input).outerWidth()
                });


                var suggestions = response.map(function(s) {

                    console.log(s);

                    return React.createElement("li", {key:  "suggestion_" + s.id, "data-userid":  s.id, onClick:  userClick.bind(null, s)  }, 
                        React.createElement("img", {src:  s.avatar}), 
                            React.createElement("span", {className: "mm-username"},  s.preferedname), 
                             s.phone && 
                            React.createElement("span", null, 
                                React.createElement(MMCafeReact.Components.Translator, {value:  "component.autocomplete.phone" }), ": ", React.createElement("span", {className: "mm-phone"},  s.areaPrefix && (s.areaPrefix), " ",  s.phone)
                            ), 
                            
                             s.extension &&
                            React.createElement("span", null, 
                                React.createElement(MMCafeReact.Components.Translator, {value:  "component.autocomplete.extension" }), ": ", React.createElement("span", {className: "mm-phone-extension"},  s.extension)
                            )
                        
                    );
                });

                var root = React.createElement("ul", null, 
                      suggestions 
                   );

                React.render(root, document.getElementById('mm-autocomplete-clone'));


                PubSub.publish("components.loader.remove", { key: "componente.autocompleteuser.search" } );
	    	})
    	}, 200);

    	
    },

    _userClick: function(user, event) {
		PubSub.publishSync( ( this.props.pubsub && this.props.pubsub.selectuser ?  this.props.pubsub.selectuser : "component.autocompleteuser.selected"), { user: user } );
        $("#mm-autocomplete-clone").html("");
   	},
    
    getInitialState: function() {
        return {suggestions: [], required: this.props.required };
    },


    componentDidMount: function() {

        if($("#mm-autocomplete-clone").length == 0) {
            $("body").append("<div id='mm-autocomplete-clone'></div>");
            $("body").on("click.mmautocomplete", function(event) {
                var target = $(event.target); 
                if(target.parents('div#mm-autocomplete-clone').length == 0) {
                    $("#mm-autocomplete-clone").html("");
                }
                
            });
        }
    },


    render: function() {
        
        return React.createElement("div", {className:  "mm-autocomplete-user" }, 
        			React.createElement("input", {name:  this.props.inputname || "keywords", ref: "input", maxLength:  this.props.maxlength, required:  this.state.required, onKeyUp:  this._keyUp})
               );
    }
});

module.exports = AutocompleteUser;
},{}],3:[function(require,module,exports){
/**
 * Component para branch do usuário
 */
var BranchLabel = React.createClass({displayName: "BranchLabel",

	propTypes: {
		/**
		 * Dados da company: data = { name: "text for link", id: 1 }
		 */
		data: React.PropTypes.object,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string
	},

    render: function() {
        return React.createElement("span", {className:  this.props.classname},  this.props.data.name);
    }

});

module.exports = BranchLabel;

},{}],4:[function(require,module,exports){
/**
 * Component para selects aninhados de Company/Branch/Department
 */
var CBD = React.createClass({displayName: "CBD",
    _firstrender : true,

	query_params: {
        companyId: null,
        branchId: null,
        departmentId: null,
    },

	propTypes: {
		/**
		 * Object with sources = { source: { branchesbycompany: "someurl", companies: "otherurl", departmentsbybranch: "anotherurl" } }
		 */
		data: React.PropTypes.object
	},

	getCompanies: function() {

        var self = this;
        this.query_params.companyId = self.props.data.company

        $
        .ajax({ url: self.props.data.source.companies })
        .then(function(response) {
            self.state.branches = [];
            self.state.departments = [];

            self.setState({ companies: response });

            if(response.length == 1) {
                var node = React.findDOMNode(self.refs.companyselect);
                $(node).prop('selectedIndex', 1);
                var event = document.createEvent('Event');
                event.initEvent('change', true, true);
                node.dispatchEvent(event);
            } 
            else if(self.props.data.company) {
                var node = React.findDOMNode(self.refs.companyselect);
                
            }
            else if(self._firstrender && self.props.data.initializers && self.props.data.initializers.company) {
                var node = React.findDOMNode(self.refs.companyselect), usercompany = self.props.data.initializers.company;


                $(node).find("option").each(function(index, item) {
                    if($(item).val() == usercompany.id) {
                        $(node).prop('selectedIndex', index);
                        var event = document.createEvent('Event');
                        event.initEvent('change', true, true);
                        node.dispatchEvent(event);
                    }
                })
            }
        });

    },

    getBranches: function(event) {
        var self = this;

        this.query_params.companyId = event.target.value;
        this.query_params.branchId = null;
        this.query_params.departmentId = null;

        $
        .ajax({ url: self.props.data.source.branchesbycompany + "companyId=" + event.target.value})
        .then(function(response) {
            self.setState({ branches: response, departments: [] });

            if(response.length == 1) {
                var node = React.findDOMNode(self.refs.branchselect);
                $(node).prop('selectedIndex', 1);
                //var event = new Event('change', { bubbles: true });
                //node.dispatchEvent(event);
                var event = document.createEvent('Event');
                event.initEvent('change', true, true);
                node.dispatchEvent(event);
            } else if(self._firstrender && self.props.data.initializers && self.props.data.initializers.branch) {
                var node = React.findDOMNode(self.refs.branchselect), userbranch = self.props.data.initializers.branch;

                $(node).find("option").each(function(index, item) {
                    if($(item).val() == userbranch.id) {
                        $(node).prop('selectedIndex', index);
                        var event = document.createEvent('Event');
                        event.initEvent('change', true, true);
                        node.dispatchEvent(event);
                    }
                })
            }
            

        });
    },

    getDepartments: function(event) {

        var self = this;

        this.query_params.branchId = event.target.value;
        this.query_params.departmentId = null;

        $
        .ajax({ url: this.props.data.source.departmentsbybranch + "branchId=" + event.target.value})
        .then(function(response) {

            self.setState({ departments: response });

            if(response.length == 1) {
                var node = React.findDOMNode(self.refs.departmentselect);
                $(node).prop('selectedIndex', 1);
                //var event = new Event('change', { bubbles: true });
                //node.dispatchEvent(event);
                var event = document.createEvent('Event');
                event.initEvent('change', true, true);
                node.dispatchEvent(event);
            } else if(self._firstrender && self.props.data.initializers && self.props.data.initializers.department) {
                
                var node = React.findDOMNode(self.refs.departmentselect), userdepartment = self.props.data.initializers.department;
                $(node).find("option").each(function(index, item) {
                    if($(item).val() == userdepartment.id) {
                        $(node).prop('selectedIndex', index);
                    }
                });

                self._firstrender = false;
            }

        });
    },

    departmentSelected: function(event) {
        this.query_params.departmentId = event.target.value;
    },

    sourceParams: function() {
        return this.query_params;
    },

    componentWillMount: function() {
    	this.getCompanies();
    },

	getInitialState: function() {
		return {data: this.props.data, companies: null, branches: null, departments: null };
    },

    render: function() {

    	var data = {}, companies = [], branches = [], departments = [];
        
        if(this.state && this.state.companies) {
            companies = this.state.companies.map(function(company) {
                return React.createElement("option", {value:  company.id, key:  "company_" + company.id},  company.name);
            });

        }

        if(this.state && this.state.branches) {
            branches = this.state.branches.map(function(branch) {
                return React.createElement("option", {value:  branch.id, key:  "branch_" + branch.id},  branch.name);
            });
        }

        if(this.state && this.state.departments) {
            departments = this.state.departments.map(function(department) {
                return React.createElement("option", {value:  department.id, key:  "department_" + department.id},  department.name);
            });
        }
    	
        return React.createElement("div", null, 
                     companies.length > 0 && 
	        		React.createElement("div", {className:  companies.length == 1 ? "hidden": ""}, 
	                React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value:  "cbd.company" })), 
	                React.createElement("select", {name: "companyId", ref: "companyselect", onChange:  this.getBranches}, 
	                    React.createElement("option", null), 
	                     companies 
	                )
	                ), 
                    
	                
                     branches.length > 0 && 
	                React.createElement("div", {className:  branches.length == 1 ? "hidden": ""}, 
	                React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value:  "cbd.branch" })), 
	                React.createElement("select", {name: "branchId", ref: "branchselect", onChange:  this.getDepartments}, 
	                    React.createElement("option", null), 
	                     branches 
	                )
	                ), 
	                

	                 departments.length > 0 &&
	                React.createElement("div", {className:  departments.length == 1 ? "hidden": ""}, 
	                React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value:  "cbd.department" })), 
	                React.createElement("select", {name: "departmentId", ref: "departmentselect", onChange:  this.departmentSelected}, 
	                    React.createElement("option", null), 
	                     departments 
	                )
	                )
	                
        );
    }

});

module.exports = CBD;

},{}],5:[function(require,module,exports){
var CalendarWeek = React.createClass({displayName: "CalendarWeek",

    selectDate: function(date) {
        
        var str = date.date.date().toString() + "_" + date.date.month();
        if($(".mm-day-" + str).hasClass("hasevent")) {
            this.props.selectDate(date);
        }
        if($(".mm-day-" + str).hasClass("clickable")) {
            PubSub.publish(this.props.pubsub.dayclicked, {date: date});
        }

    },

    render: function() {
        var days = [],
            date = this.props.date,
            month = this.props.month;

           date.utc();
        for (var i = 0; i < 7; i++) {

            var day = {
                name: date.format("dd").substring(0, 1),
                number: date.date(),
                isCurrentMonth: date.month() === month.month(),
                isToday: date.isSame(new Date(), "day"),
                date: date
            };
            var str = " mm-day-" + day.number.toString() + "_" + date.month().toString();

            days.push(React.createElement("span", {
                        key: day.date.toString(), 
                        className: "mm-day" + (day.isToday ? " mm-today" : "") + 
                                    (day.isCurrentMonth ? "" : " mm-different-month") + 
                                    //(day.date.isSame(this.props.selected) ? " selected" : "") +
                                    str, 
                                  
                        onClick: this.selectDate.bind(null, day)}, 
                            day.number
                       ));
            date = date.clone();
            date.add(1, "d");

        }

        return React.createElement("div", {className: "week", key: days[0].toString()}, 
            days
        )
    }
});

/**
 * Component para calendário por mês, navegável
 */
var Calendar = React.createClass({displayName: "Calendar",
    
    propTypes: {
        /**
         * Mês e ano selecionados/atual: selected = "09/2015"
         */
        selected: React.PropTypes.object
    },

    
    getInitialState: function() {
        return {
           month: this.props.selected.clone()
        };
    },

    /**
     *
     */
    previous: function() {
       
        var month = this.state.month;
        month.add(-1, "M");

        this.setState({ month: month });
        if(this.props.handleChangeCalendar)
            this.props.handleChangeCalendar(month);

    },

    next: function() {
        var month = this.state.month;
        month.add(1, "M");
        this.setState({ month: month });
        if(this.props.handleChangeCalendar)
            this.props.handleChangeCalendar(month);
    },

    select: function(day) {
        this.props.selected = day.date;
        this.forceUpdate();
    },

    renderWeeks: function() {
        
        var weeks = [],
            done = false,
            date = this.state.month.clone().startOf("month").day("Sunday"),
            monthIndex = date.month(),
            count = 0;


        if(date.date() == 2) {
        	date = date.date(1);
        }

        while (!done) {
            weeks.push(React.createElement(CalendarWeek, {
                        key: "week_" + count, 
                        date: date.clone(), 
                        month: this.state.month, 
                        select: this.select, 
                        dayswithevents:  this.state.dayswithevents, 
                        pubsub:  this.props.pubsub, 
                        selectDate: this.props.selectDate}));
            date.add(1, "w");
            done = count++ > 2 && monthIndex !== date.month();
            monthIndex = date.month();
        }

        return weeks;
    },

    renderMonthLabel: function() {
        return React.createElement("span", null, this.state.month.format("MMMM YYYY"));
    },

    componentDidMount: function() {


        var self = this, node = $(this.getDOMNode());
        //PubSub.subscribe( 'widget.calendar.dayswithevents', function(msg, data) {
        PubSub.subscribe( this.props.subscribedays, function(msg, data) {
            var month = (self.state.month.month() - 1);

            if(data.days) {
                data.days.forEach(function(day) {
                    node.find(".mm-day-" + day + "_" + (month + 1)).addClass("hasevent");
                }); 
            }
        });

        if(this.props.pubsub && this.props.pubsub.overriderule) {
            PubSub.subscribe( this.props.pubsub.overriderule, function(msg, dates) {
                var month = (self.state.month.month() - 1);
                var days = moment(self.state.month).endOf("month").daysInMonth();

                for(var i = 1; i<= days; i++) {

                    node.find(".mm-day-" + i + "_" + (month + 1)).addClass("clickable");
                }

                
            });
        }
    },


    componentDidUpdate: function() {

    	var self = this, node = $(this.getDOMNode()), month = this.state.month.month();

        /*
    	if(this.state.dayswithevents) {
     		this.state.dayswithevents.forEach(function(day) {
                node.find(".mm-day-" + day + "_" + month).addClass("hasevent");
    		});	
    	}
        */
    	node.find(".mm-day.selected").removeClass("selected");


    	if(this.state.selected_date){
	    	node.find(".mm-day-" + this.state.selected_date.date() + "_" + this.state.selected_date.month()).addClass("selected");           
        }


        if(this.props.pubsub && this.props.pubsub.overriderule) {

                var month = (self.state.month.month() - 1);
                var days = moment(self.state.month).endOf("month").daysInMonth();
                for(var i = 1; i<= days; i++) {

                    node.find(".mm-day-" + i + "_" + (month + 1)).addClass("clickable");
                }

        }
    },

    render: function() {

        var days = moment.weekdaysMin(), d = days[0];
        days.push(d);
        //delete days[0];
        days.pop();
        var header = [];
        days.forEach(function(day, index) {
            header.push (React.createElement("span", {className: "mm-day", key:  "day_" + index},  day ));
        });

        var weeks = this.renderWeeks();

        return React.createElement("div", {className: "mm-calendar"}, 
            React.createElement("div", {className: "mm-calendar-header"}, 
                React.createElement("a", {onClick: this.previous, className: "mm-calendar-previous"}), 
                this.renderMonthLabel(), 
                React.createElement("a", {onClick: this.next, className: "mm-calendar-next"})
            ), 
            React.createElement("div", {className: "mm-calendar-week"}, 
                header 
            ), 
            weeks 
        );
    }
});

module.exports = Calendar;


},{}],6:[function(require,module,exports){
/**
 * Component para companhia do usuário
 */
var CompanyLabel = React.createClass({displayName: "CompanyLabel",

	propTypes: {
		/**
		 * Dados da company: data = { name: "text for link", id: 1 }
		 */
		data: React.PropTypes.object,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string
	},

    render: function() {
    	if(this.props.data && this.props.data.name)
	        return React.createElement("span", {className:  this.props.classname || "mm-label-company"},  this.props.data.name);
    	else
    		return React.createElement("span", null);
    }

});

module.exports = CompanyLabel;

},{}],7:[function(require,module,exports){
/**
 * Component para adicionar/remover usuário como contato
 */
var ContactUser = React.createClass({displayName: "ContactUser",

	_isSubmitting: false,

	propTypes: {
		/**
		 * Object target = { id: 1, status: false }
		 */
		target: React.PropTypes.object
	},

	_build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },

    
    _toggleContact: function() {

    	if(!this._isSubmitting) {
    	
	    	this._isSubmitting = true;

	    	var self = this;
	        var url = this.props.source.contactuser({id: this.props.target.id, status: !this.state.contacted, authenticity: this.props.target.authenticity});

	        var str = this.state.contacted ? "remove" : "add";
	        window.PubSub.publish("components.loader.add", { key: "mm.contactuser.toggle." + str, type: "loading"});

	        var builded_data = this._build_data(url)
			$
	        .ajax({url: builded_data.url, data: builded_data.data, method: 'post' })
	        .then(function(responseText) {
	        	self.setState({contacted: !self.state.contacted});
	        	self._isSubmitting = false;
	        	window.PubSub.publish("components.loader.remove", { key: "mm.contactuser.toggle." + str, type: "success"});
	        })
	        .fail(function(jqXHR) {
	        	self._isSubmitting = false;
	        	if(window.PubSub)
				//window.PubSub.publishSync( 'mm.contactuser.error', { error: jqXHR } );
		        	window.PubSub.publish("components.loader.remove", { key: "mm.contactuser.toggle." + str, type: "error", error: jqXHR});
	        });
    	}
    },

	getInitialState: function() {
		var contacted = this.props.target.contacted || false;
        return {contacted: contacted };
    },

    render: function() {
    	
        var classname = this.state.contacted ? "contacted" : "";
        
        return React.createElement("div", {className:  classname, onClick: this._toggleContact, "data-contacted":  this.state.contacted}
        	
        );
    }

});

module.exports = ContactUser;

},{}],8:[function(require,module,exports){
/**
 * Component para label de data. Exemplo em https://jsfiddle.net/chrisbenseler/69z2wepo/16016/
 */
var DateLabel = React.createClass({displayName: "DateLabel",

	propTypes: {
		/**
		 * Data em formato ISO 8061: iso8061date = "2015-09-01T00:00:00Z"
		 */
		iso8061date: React.PropTypes.string,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string,
		/**
		 * pattern de formatação de data 
		 */
		pattern: React.PropTypes.string
	},

	_formatdate: function() {
    	//var txt = this.state.date.getDate() + "/" + (this.state.date.getMonth() + 1) + "/" + this.state.date.getFullYear();
        var txt = moment(this.state.date).utc().format(this.props.pattern);
        return txt;
    },

	getInitialState: function() {
		var utcDate = moment(this.props.iso8061date).utc().format();
		var date = new Date(utcDate);
        return {date: date };
    },

    render: function() {
    	
        return React.createElement("span", null,  this._formatdate() );
    }

});

module.exports = DateLabel;

},{}],9:[function(require,module,exports){
/**
 * Component para companhia do usuário
 */
var DepartmentLabel = React.createClass({displayName: "DepartmentLabel",

	propTypes: {
		/**
		 * Dados da company: data = { name: "text for link", id: 1 }
		 */
		data: React.PropTypes.object,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string
	},

    render: function() {
        return React.createElement("span", {className:  this.props.classname},  this.props.data.name);
    }

});

module.exports = DepartmentLabel;

},{}],10:[function(require,module,exports){
/**
 * Component para favoritar POs, exemplo em https://jsfiddle.net/chrisbenseler/6besahLn/1/
 */
var Favorite = React.createClass({displayName: "Favorite",

    _isSubmitting: false,
    pubSub: PubSub,

	propTypes: {
		/**
		 * Object target = { id: 1, favorited: false }
		 */
		target: React.PropTypes.object
	},

    _build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },

    _toggleFav: function() {

        if(!this._isSubmitting) {

            this._isSubmitting = true;

            var url = this.props.source.favorite({id: this.props.target.id, status: !this.state.favorited});
            if(this.state.favorited)
                this.pubSub.publish("components.loader.add", { key: "po.favorite.remove" } );
            else
                this.pubSub.publish("components.loader.add", { key: "po.favorite.add" } );

            var builded_data = this._build_data(url)

    		$
            .ajax({url: builded_data.url, data: builded_data.data, method: 'post' })
            .then(function(response) {
            	this.setState({favorited: !this.state.favorited, count: response.favorites});
                this._isSubmitting = false;
             }.bind(this))
            .fail(function(jqXHR) {
                this.pubSub.publish("components.loader.remove", { key: "po.favorite.remove", type: "error", error: jqXHR } );
                this.pubSub.publishSync( 'mm.favorite.error', { error: jqXHR } );
                this._isSubmitting = false;
            }.bind(this))
            .done(function() {
                if(!this.state.favorited)
                    this.pubSub.publish("components.loader.remove", { key: "po.favorite.remove" } );
                else
                    this.pubSub.publish("components.loader.remove", { key: "po.favorite.add" } );
            }.bind(this))
        }
    },

	getInitialState: function() {
		var favorited = this.props.target.favorited || false;
        return {favorited: favorited, count: this.props.count };
    },

    render: function() {
    	
        var classname = this.state.favorited ? "favorited mm-tooltip" : "mm-tooltip";
        return React.createElement("div", null, 
                    React.createElement("div", {
                        className:  classname, 
                        onClick: this._toggleFav, 
                        "data-favorited":  this.state.favorited, 
                        "data-desc":  MMCafeReact.Components.Translator.getI18n('favorite.tooltip') 
                    }
        	        ), 
                    React.createElement("span", null,  this.state.count)
                );
    }

});

module.exports = Favorite;
},{}],11:[function(require,module,exports){
/**
 * Component para selects de GRs e GGLs.
 */
var GRGGL = React.createClass({displayName: "GRGGL",

    _firstrender: true,
    _isDashboard: true,

    query_params: {
        grName: null,
        gglName: null,
        storeName: null,
        taskId: null
    },

    propTypes: {
        /**
         * Object with sources = { source: { listGgls: "someurl", listGrs: "otherurl", listStores: "anotherurl", listTasks: "onemoreurl" } }
         */
        data: React.PropTypes.object
    },

    getGRs: function () {

        var self = this;

        $.ajax({url: self.props.data.source.listGrs})
            .then(function (response) {

                self.state.ggls = [];

                self.setState({grs: response});

                if (response.length === 1) {
                    var node = React.findDOMNode(self.refs.grselect);
                    $(node).prop('selectedIndex', 1);
                    var event = document.createEvent('Event');
                    event.initEvent('change', true, true);
                    node.dispatchEvent(event);
                } else if (self._firstrender && self.props.data.initializers && self.props.data.initializers.gr) {
                    var node = React.findDOMNode(self.refs.grselect), usergr = self.props.data.initializers.gr;

                    $(node).find("option").each(function (index, item) {
                        if ($(item).val() === usergr.id) {
                            $(node).prop('selectedIndex', index);
                            var event = document.createEvent('Event');
                            event.initEvent('change', true, true);
                            node.dispatchEvent(event);
                        }
                    })
                }
            });
    },

    getGGLs: function (event) {
        var self = this;
        if (event.target) {
            this.query_params.grName = event.target.value;
        } else {
            this.query_params.grName = event.name;
        }
        this.query_params.gglName = null;
        this.query_params.store = null;

        if (this.query_params.grName === "") {
            self.setState({ggls: null});
            self.setState({stores: null});
            self.setState({tasks: null});
        }


        $.ajax({
            url: self.props.data.source.listGgls,
            data: {
                gr: this.query_params.grName
            }
        })
            .then(function (response) {

                self.setState({ggls: response});

                if (response.length === 1) {
                    var node = React.findDOMNode(self.refs.gglselect);
                    $(node).prop('selectedIndex', 1);
                    var event = document.createEvent('Event');
                    event.initEvent('change', true, true);
                    node.dispatchEvent(event);
                } else if (self._firstrender && self.props.data.initializers && self.props.data.initializers.ggl) {
                    var node = React.findDOMNode(self.refs.gglselect), userggl = self.props.data.initializers.ggl;

                    $(node).find("option").each(function (index, item) {
                        if ($(item).val() === userggl.id) {
                            $(node).prop('selectedIndex', index);
                            var event = document.createEvent('Event');
                            event.initEvent('change', true, true);
                            node.dispatchEvent(event);
                        }
                    })
                }
            });
    },

    getStores: function (event) {

        var self = this;

        if (event.target) {
            this.query_params.gglName = event.target.value;
        } else {
            this.query_params.gglName = event.name;
            this.query_params.grName = null
        }
        this.query_params.store = null;

        if (this.query_params.gglName === "") {
            self.setState({stores: null});
            self.setState({tasks: null});
        }


        self.props.handleQueryParams(this.query_params);

        $.ajax({
            url: self.props.data.source.listStores,
            data: {
                gr: this.query_params.grName,
                ggl: this.query_params.gglName
            }
        })
            .then(function (response) {

                self.setState({stores: response});

                if (response.length === 1) {
                    var node = React.findDOMNode(self.refs.storeselect);
                    $(node).prop('selectedIndex', 1);
                    var event = document.createEvent('Event');
                    event.initEvent('change', true, true);
                    node.dispatchEvent(event);
                } else if (self._firstrender && self.props.data.initializers && self.props.data.initializers.store) {
                    var node = React.findDOMNode(self.refs.storeselect), userstore = self.props.data.initializers.store;

                    $(node).find("option").each(function (index, item) {
                        if ($(item).val() === userstore.id) {
                            $(node).prop('selectedIndex', index);
                            var event = document.createEvent('Event');
                            event.initEvent('change', true, true);
                            node.dispatchEvent(event);
                        }
                    })
                }
            });
    },


    getTasks: function (event) {
        var self = this;

        if (event.target) {
            this.query_params.store = event.target.value;
        } else {
            this.query_params.store = event.name;
            this.query_params.grName = null
            this.query_params.gglName = null
        }
        this.query_params.taskId = null;

        if (this.query_params.store === "") {
            self.setState({tasks: null});
        }

        $.ajax({
            url: self.props.data.source.listTasks,
            data: {
                gr: this.query_params.grName,
                ggl: this.query_params.gglName,
                storeId: this.query_params.store
            }
        })
            .then(function (response) {

                self.setState({tasks: response});

                if (response.length === 1) {
                    var node = React.findDOMNode(self.refs.tasksselect);
                    $(node).prop('selectedIndex', 1);
                    var event = document.createEvent('Event');
                    event.initEvent('change', true, true);
                    node.dispatchEvent(event);
                } else if (self._firstrender && self.props.data.initializers && self.props.data.initializers.task) {
                    var node = React.findDOMNode(self.refs.storeselect), usertask = self.props.data.initializers.task;

                    $(node).find("option").each(function (index, item) {
                        if ($(item).val() === userstore.id) {
                            $(node).prop('selectedIndex', index);
                            var event = document.createEvent('Event');
                            event.initEvent('change', true, true);
                            node.dispatchEvent(event);
                        }
                    })
                }
            });
    },

    gglSelected: function (event) {
        this.query_params.gglId = event.target.value;
    },

    storeSelected: function (event) {
        this.query_params.store = event.target.value;
    },

    taskSelected: function (event) {
        this.query_params.taskId = event.target.value;
    },

    sourceParams: function () {
        return this.query_params;
    },

    componentWillMount: function () {
        if (this.state.data.source.isGGL) {
            this.getStores(this.state.data.source.loggedUser)
        } else if (this.state.data.source.isGr) {
            this.getGGLs(this.state.data.source.loggedUser)
        } else {
            this.getGRs();
        }
    },

    getInitialState: function () {
        return {data: this.props.data, grs: null, ggls: null, stores: null, tasks: null};
    },

    render: function () {

        var data = {}, listGrs = [], listGgls = [], listStores = [], listTasks = [];

        if (this.state && this.state.grs) {
            listGrs = this.state.grs.map(function (gr) {
                return React.createElement("option", {value: gr.name, key: "gr_" + gr.id}, gr.fullname);
            });
        }

        if (this.state && this.state.ggls) {
            listGgls = this.state.ggls.map(function (ggl) {
                return React.createElement("option", {value: ggl.name, key: "ggl_" + ggl.id}, ggl.fullname);
            });
        }

        if (this.state && this.state.stores) {
            listStores = this.state.stores.map(function (store) {
                return React.createElement("option", {value: store.id, key: "store_" + store.id}, store.fullname);
            });
        }

        if (this.state && this.state.tasks) {
            listTasks = this.state.tasks.map(function (task, index) {
                return React.createElement("option", {value: task.name, key: "task_" + index}, task.name);
            });
        }

        return React.createElement("div", null, 
            listGrs.length > 0 && (this.state.data.source.isGr || this.state.data.source.loggedUser.admin) &&
            React.createElement("div", {className: listGrs.length === 1 ? "hidden" : ""}, 
                React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value: "dashboard.gr.label"})), 
                React.createElement("select", {name: "gr", ref: "grselect", onChange: this.getGGLs}, 
                    React.createElement("option", {value: ""}), 
                    listGrs
                )
            ), 
            

            listGgls.length > 0 && (this.state.data.source.isGGL || this.state.data.source.isGr || this.state.data.source.loggedUser.admin) &&
            React.createElement("div", {className: listGgls.length === 1 ? "hidden" : ""}, 
                React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value: "dashboard.ggl.label"})), 
                React.createElement("select", {name: "ggl", ref: "gglselect", onChange: this.getStores}, 
                    React.createElement("option", {value: ""}), 
                    listGgls
                )
            ), 
            

            listStores.length > 0 && !this.state.data.source.isDashboard && (this.state.data.source.isGGL || this.state.data.source.isGr || this.state.data.source.loggedUser.admin) &&
            React.createElement("div", {className: listStores.length === 1 ? "hidden" : ""}, 
                React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value: "dashboard.store.label"})), 
                React.createElement("select", {name: "storeId", ref: "storeselect", onChange: this.getTasks}, 
                    React.createElement("option", null), 
                    listStores
                )
            ), 
            

            listTasks.length > 0 && !this.state.data.source.isDashboard && (this.state.data.source.isGGL || this.state.data.source.isGr || this.state.data.source.loggedUser.name === 'admin') &&
            React.createElement("div", {className: listTasks.length === 1 ? "hidden" : ""}, 
                React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value: "dashboard.task.label"})), 
                React.createElement("select", {name: "task", ref: "taskselect"}, 
                    React.createElement("option", null), 
                    listTasks
                )
            )
            

        );
    }

});

module.exports = GRGGL;

},{}],12:[function(require,module,exports){
var Likeable = React.createClass({displayName: "Likeable",

    _isSubmitting: false,
    pubSub: PubSub,

	propTypes: {
		/**
		 * Object target = { id: 1, likeable: false }
		 */
		target: React.PropTypes.object
	},

    _toggleLike: function() {

        if(!this._isSubmitting) {
        
            this._isSubmitting = true;
            
            var method = this.state.likeId?  "removeLike" : "like"

            if(this.state.liked)
                this.pubSub.publish("components.loader.add", { key: "po.likeable.remove" } );
            else
                this.pubSub.publish("components.loader.add", { key: "po.likeable.add" } );

    		$
            .ajax({
                url: "/ajax/likeable/user.view",
                data:{method: method, objectId: this.props.target.id, objectType: this.props.type, likeId: this.state.likeId}
            })
            .then(function(response) {
                this.setState({liked: !this.state.liked, count: response.likes, likeId: response.likeId});
                this._isSubmitting = false;
             }.bind(this))
            .fail(function(jqXHR) {
                this.pubSub.publish("components.loader.remove", { key: "po.likeable.remove", type: "error", error: jqXHR } );
                this.pubSub.publishSync( 'mm.likeable.error', { error: jqXHR } );
                this._isSubmitting = false;
            }.bind(this))
            .done(function() {
                if(!this.state.liked)
                    this.pubSub.publish("components.loader.remove", { key: "po.likeable.remove" } );
                else
                    this.pubSub.publish("components.loader.remove", { key: "po.likeable.add" } );
            }.bind(this))
        }
    },

	getInitialState: function() {
		var liked = this.props.target.likeable || false ;
        return {liked: liked, count: this.props.count, likeId: this.props.target.likeId};
    },

    render: function() {
    	
        var classname = this.state.liked ? "liked " : "";
        var qtlikes = this.state.count > 0 && !this.state.liked ? "muchlikes" : "";
        return (React.createElement("div", {className: "like-container " + qtlikes}, 
                    this.props.viewOnly==true &&
                        React.createElement("div", {className: "ico-like " + classname, "data-liked":  this.state.liked, "data-liked": this.state.likeId}), 
                    
                    !this.props.viewOnly &&
                        React.createElement("div", {className:  "ico-like " + classname, onClick: this._toggleLike, "data-liked":  this.state.liked, "data-liked": this.state.likeId}), 
                    
                        React.createElement("span", null,  this.state.count)
                ));
    }

});

module.exports = Likeable;
},{}],13:[function(require,module,exports){
/**
 * Component para link tag
 */
var Link = React.createClass({displayName: "Link",

	propTypes: {
		/**
		 * Dados do link: data = { name: "text for link", url: "http://mmcafe.com.br" }
		 */
		data: React.PropTypes.object,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string,
		/**
		 * target do link: target = "_blank"
		 */
		target: React.PropTypes.string
	},

    render: function() {
    	var regex = /(&nbsp;|<([^>]+)>)/ig;
        return React.createElement("a", {href: this.props.data.url, 
        		  title:  this.props.data.name, 
        		  className:  this.props.classname, 
        		  target:  this.props.data.target, 
        		  "data-remote":  this.props.data.remote, 
        		  dangerouslySetInnerHTML: { __html: this.props.data.name}});
    }

});

module.exports = Link;

},{}],14:[function(require,module,exports){

/**
 * Component para lista
 */
var List = React.createClass({displayName: "List",

    propTypes: {
        /**
         * Array com items da lista, cada lista é um json: data = [ { text: "texto do link" }, external: false, thumb: "http://urldeimagem", description: "texto adicional", "date": "2015-09-01T00:00:00Z" } ]
         */
        items: React.PropTypes.array,
        /**
         * classe css: classname = "col-md-3" 
         */
        classname: React.PropTypes.string
    },

	render: function() {

        var items = this.props.items.map(function(item, index) {
            var span = null, thumb = null, datelabel= null;

            if(item.description)
                span = React.createElement("span", {dangerouslySetInnerHTML: { __html: item.description}});

            if(item.thumb){
                thumb = React.createElement("img", {src:  item.thumb});
            }else{
                if(item.image && item.image.url)
                    thumb = React.createElement("img", {src:  item.image.url});
            }


            if(item.date) {
                datelabel = React.createElement(MMCafeReact.Components.DateLabel, {iso8061date:  item.date, pattern:  item.date_pattern || "DD/MM/YYYY"})
            }

        	if(item.link) {
            	var target = item.external ? "_blank" : "";
            	return React.createElement("li", {key:  "item_" + index},  thumb, 
                         datelabel, 
                        React.createElement("a", {href:  item.link, target:  target, dangerouslySetInnerHTML: { __html: item.text}, title:  item.text}), 
                         span )
            }            
            else
	        	return React.createElement("li", {key:  "item_" + index},  thumb, 
                     datelabel, 
                    React.createElement("span", {dangerouslySetInnerHTML: { __html: item.text}}), 
                     span 
                )
        });
        return React.createElement("ul", {className:  this.props.classname}, 
        	 items 
        );
	}

});

module.exports = List;

},{}],15:[function(require,module,exports){
var Loader = React.createClass({displayName: "Loader",

	componentDidMount: function() {

		var self = this;
 		PubSub.subscribe("components.loader.add", function(msg, data) {
 			var msgs = this.state.messages;
 			msgs[data.key] = { key: data.key, type: data.type ? data.type : "loading" };
      this.setState({ messages: msgs });
 		}.bind(this));

 		PubSub.subscribe("components.loader.remove", function(msg,  data) {
 			var msgs = this.state.messages;
      this.setState({ messages: msgs });
 			if(data.type) {
        delete msgs[data.key];
        msgs[data.key + "." + data.type] = { key: data.key + "." + data.type, type: data.type };
        this.setState({ messages: msgs });
        var timeout = data.type == "success" ? 2000 : 4000;

        setTimeout(function() {
          delete msgs[data.key + "." + data.type];
          this.setState({ messages: msgs });
        }.bind(this), timeout);

        if(data.type == "error") {
          console.error("Error on", data.key, data.error);
        }

      } else {
        delete msgs[data.key];
        this.setState({ messages: msgs });
      }
 		}.bind(this));
  },

	getInitialState: function() {
    	var msg = { };
    	return { messages: msg };
    },
    
    render: function() {
  		var messages = this.state.messages,
          nodes = [];
  		for(key in messages) {
              
         	nodes.push(React.createElement("li", {key:  key, className:  "mmloader-item-" + messages[key].type}, 
         			React.createElement(MMCafeReact.Components.Translator, {key:  "translateloading_" + key, value:  "loading." + key})
         			))
    }

    if(nodes.length > 0) {
    return React.createElement("ul", null, 
       	 nodes 
        );
    } else { 
      return React.createElement("span", null);
    }
  }
});

module.exports = Loader;
},{}],16:[function(require,module,exports){
/**
 * Component para internacionalização de mensagens
 */
var Translator = React.createClass({displayName: "Translator",

    
    render: function() {
        return (
            React.createElement("span", null,  Translator.getI18n(this.props.value) )
        );
    }
});
Translator["langs"] = {};
Translator["setLang"] = function(lang) {
	Translator["lang"] = lang;
};

Translator.getI18n = function(key) {
    if(!Translator.langs[Translator.lang])
        return "no i18n[" + Translator.lang + "]: " + key;

    if(Translator.langs[Translator.lang][key] == undefined)
        return "no i18n[" + key + "]";

    return Translator.langs[Translator.lang][key];
}

module.exports = Translator;
},{}],17:[function(require,module,exports){
/**
 * Component para link de usuário
 */
var UserLink = React.createClass({displayName: "UserLink",

    propTypes: {
        /**
         * Usuário, é um json
         */
        user: React.PropTypes.object,
        /**
         * classe css: classname = "col-md-3" 
         */
        classname: React.PropTypes.string
    },

    render: function() {
        var month = (new Date()).getMonth()+1, day = (new Date()).getDate(), classname = this.props.classname || "mm-link-user";

        if(this.props.user.birthdate) {
            var nodes = this.props.user.birthdate.split("/").map(function(item) {
                return parseInt(item);
            });

            if(nodes[0] == day && nodes[1] == month) {
                classname += " mm-user-birthday";
            }

        }

        if(this.props.user.onVacation) {
            classname += " mm-user-vacation";   
        }

        var data = {name: this.props.user.preferedname, url: "/m/usuario/" + this.props.user.id, remote: true};
        return React.createElement(MMCafeReact.Components.Link, {data:  data, classname:  classname });
    }

});

module.exports = UserLink;

},{}],18:[function(require,module,exports){
var Components = {};

Components.Accordion = require("../components/Accordion");
Components.AutocompleteUser = require("../components/AutocompleteUser");
Components.BranchLabel = require("../components/BranchLabel");
Components.Calendar = require("../components/Calendar");
Components.CBD = require("../components/CBD");
Components.CompanyLabel = require("../components/CompanyLabel");
Components.ContactUser = require("../components/ContactUser");
Components.DateLabel = require("../components/DateLabel");
Components.DepartmentLabel = require("../components/DepartmentLabel");
Components.Favorite = require("../components/Favorite");
Components.Likeable = require("./../components/Likeable");
Components.Link = require("./../components/Link");
Components.List = require("../components/List");
Components.Loader = require("../components/Loader");
Components.Translator = require("../components/Translator");
Components.UserLink = require("../components/UserLink");
Components.GRGGL = require("../components/GRGGL");

module.exports = Components;
},{"../components/Accordion":1,"../components/AutocompleteUser":2,"../components/BranchLabel":3,"../components/CBD":4,"../components/Calendar":5,"../components/CompanyLabel":6,"../components/ContactUser":7,"../components/DateLabel":8,"../components/DepartmentLabel":9,"../components/Favorite":10,"../components/GRGGL":11,"../components/List":14,"../components/Loader":15,"../components/Translator":16,"../components/UserLink":17,"./../components/Likeable":12,"./../components/Link":13}],19:[function(require,module,exports){
var Components = require("../Components");
window.MMCafeReact = window.MMCafeReact || {};
MMCafeReact.Components = Components;
},{"../Components":18}]},{},[19]);
