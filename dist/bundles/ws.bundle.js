(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

var Column = React.createClass({displayName: "Column",

	render: function() {
               
        var widgets = [];
        
        if(this.props.widgets && this.props.widgets.length > 0) {
            this.props.widgets.forEach(function(widget_id, index) {
                var url = this.props.widgetSource.replace("#{id}", widget_id)
                if(widget_id!=null)
            	   widgets.push(React.createElement(MMCafeReact.WS.Widget, {
                                key:  "widget_" + widget_id, 
                                ref:  "widget_" + widget_id, 
                                widget_id:  widget_id, 
                                type:  "plain", 
                                remote:  true, 
                                source:  url, 
                                expanded:  true, 
                                draggable:  true, 
                                handleRemoveWidget:  this.props.widgetRemoved
                             }));
            }.bind(this))
        } 
        
        var classname = this.props.col_classname;
        if(widgets.length == 0) {
            classname+= " column_ghost";
            widgets.push(React.createElement("div", {className: "widget widget_ghost", id:  "ghost_" + this.props.col_id, key:  "widget_ghost"}, React.createElement("div", null)));
        }
    	return (React.createElement("div", {className:  classname, "data-id":  this.props.col_id}, 
             widgets
        ));
   
    }
});

module.exports = Column;

},{}],2:[function(require,module,exports){


var ExpandableColumn = React.createClass({displayName: "ExpandableColumn",

    ExpandWidgetButton: React.createClass({displayName: "ExpandWidgetButton",

        handleClick: function() {
            this.setState({ force_expand: !this.state.force_expand })
        },

        getInitialState: function() {
            return { expanded: this.props.expanded, force_expand: false };
        },

        componentDidMount: function() {

        },

        render: function() {
            var Widget = MMCafeReact.WS.Widget,
                className = "btn-toggle-widget btn-toggle-widget-" + this.props.type;

            this.state.expanded ? className +=  " btn-toggle-expanded" : className += " btn-toggle-notexpanded";
            var divclass = this.state.force_expand ? "active" : ""; 

            return (
                React.createElement("div", {key:  "expandablewidgetbutton_" + this.props.widget_id}, 
                React.createElement("button", {className:  className, "data-target": this.props.type, onClick: this.handleClick}), 
                React.createElement("div", {className:  divclass }, 
                React.createElement(Widget, {
                            key:  "widget_" + this.props.widget_id, 
                            ref:  "widget_" + this.props.widget_id, 
                            widget_id:  this.props.widget_id, 
                            type:  this.props.type, 
                            expanded:  true, 
                            title:  this.props.title, 
                            data:  this.props.data, 
                            html:  this.props.html
                         }
                         )
                         )
                )
            );
        }

    }),

    handleBtnClick: function (opts) {
        

    },

    getInitialState: function() {
        return { widgets: this.props.widgets };
    },

    componentDidMount: function() {
        var col = this.refs.column;

        var self = this,
            footerpos = $("footer#footer").position(),
            minheight = 0;

        this.state.widgets.map(function(item, i) {
            var btn = self.refs["button_" + item.widget_id],
                widget = btn.refs["widget_" + item.widget_id],
                p = $(widget.getDOMNode()).position();


            var ph = p.top - $("#column-to-expand").position().top;

            minheight = ph + $(widget.getDOMNode()).height() > minheight ? ph + $(widget.getDOMNode()).height() : minheight;


        });
        $("#column-to-expand").css("min-height", minheight - 30);

    },

    render: function() {

        var ExpandWidgetButton = this.ExpandWidgetButton,
            Widget = MMCafeReact.WS.Widget;
        return (
                React.createElement("div", {id: "column-to-expand", ref: "column"}, 
                this.state.widgets.map(function(item, i) {
                    return React.createElement(ExpandWidgetButton, {
                            key:  "button_" + item.widget_id, 
                            ref:  "button_" + item.widget_id, 
                            widget_id:  item.widget_id, 
                            type:  item.type, 
                            expanded:  true, 
                            title:  item.title, 
                            data:  item.data, 
                            html:  item.html, 
                            handleBtnClick:  this.handleBtnClick
                            }
                            )
                }, this)
                )

     );
   
    }
});

module.exports = ExpandableColumn;
},{}],3:[function(require,module,exports){
var Widget = React.createClass({displayName: "Widget",

    WidgetHeader : React.createClass({displayName: "WidgetHeader",

        toggleMinimize: function() {
            this.setState({ collapsed: !this.state.collapsed });
            this.props.updateCollapse(!this.state.collapsed);
        },
        removeWidget: function() {
            PubSub.publishSync( 'widget.removed', { widget_id: this.props.widget_id } );
        },
        getInitialState: function() {
            return {label: "", buttons: {}, collapsed: this.props.collapsed, overridetooltip: null, type: this.props.type };
        },
        componentDidMount: function() {
            this.setState( {label: "" } );
        },
        render: function() {
            var buttons = [],
                div = React.createFactory('div');
            if(this.state.buttons.minimize) {
                var classname = "wigdet_header_minimize";

                this.state.collapsed ? classname += " widget_header_collapsed" : classname += " widget_header_contracted";
                
                buttons.push(React.createElement("div", {className:  classname, key: "wigdet_header_minimize", onClick: this.toggleMinimize})); 
            }
            if(this.state.buttons.tooltip)
                buttons.push(div({ className: "wigdet_header_tooltip highlight", key: "wigdet_header_tooltip", "data-overridetooltip": this.state.overridetooltip, "data-desc":  MMCafeReact.Components.Translator.getI18n('tooltip.'+ this.state.type) }));  
            
            if(this.state.buttons.remove)
                buttons.push(React.createElement("div", {className: "wigdet_header_close", key: "wigdet_header_remove", onClick: this.removeWidget})); 
            
            return (
                React.createElement("header", {className:  this.props.draggable ? "widget-drag-handler" : ""}, 
                     this.state.label || this.props.datatitle, 
                    React.createElement("div", null, 
                     buttons 
                    )
                ));
        }
    }),

    WidgetFactory: function(key, opts) {
        var keys = {
            birthdaycalendar: "WidgetContentBirthdayCalendar",
            birthdayofhiringcalendar: "WidgetContentBirthdayCalendar",
            birthdays: "WidgetContentBirthdays",
            birthdaysofhiring: "WidgetContentBirthdays",
            calendar: "WidgetContentCalendar",
            ocurrencecalendar: "WidgetContentOcurrenceCalendar",
            carousel: "WidgetContentCarousel",
            cards: "WidgetContentAbilityCards",
            contacts: "WidgetContentContacts",
            dynamiclinks:"WidgetContentDynamicLinks",
            favorites: "WidgetContentFavorites",
            feed: "WidgetContentFeeds",
            userfeed: "WidgetContentUserFeeds",
            forum: "WidgetContentForum",
            incidents: "WidgetContentIncidents",
            notepad: "WidgetContentNotepad",
            lastcomments: "WidgetContentLastComments",
            lastupdates: "WidgetContentLastUpdates",
            links: "WidgetContentLinks",
            mostviews: "WidgetContentMostViews",
            news: "WidgetContentNews",
            plain: "WidgetContentPlain",
            offerrides: "WidgetContentOfferrides",
            ourteam: "WidgetContentOurTeam",
            outdoor: "WidgetContentOutDoor",
            poll: "WidgetContentPoll",
            praises: "WidgetContentPraises",
            tags: "WidgetContentTags",
            tasks: "WidgetContentTasks",
            tasksmanager: "WidgetContentTasksmanager",
            vacations: "WidgetContentVacations",
            usersearch: "WidgetContentUserSearch",
            warnings: "WidgetContentWarnings",
            weather: "WidgetContentWeather",
            requiredread: "WidgetContentRequiredRead",
            dashboardtasks: "WidgetContentDashboardTasks"
        }
        var W = MMCafeReact.Widgets[keys[key]];
        return React.createElement(W, {ref: "content", widget_id:  opts.widget_id, key:  opts.key, html:  opts.html, isExpanded:  opts.isExpanded, isVisible:  opts.isVisible, data:  opts.data});
    },

    toggleVisible: function() {
        this.setState({ expanded: !this.state.expanded });
    },

    handleToggleClick: function(event) {
        this.setState({ expanded: !this.state.expanded });
    },
    handleUpdateCollapse: function(collapsed) {
        this.setState({ collapsed: collapsed });
    },
    isExpanded: function() {
        return this.state.expanded;
    },
    isVisible: function() {
        return this.state.expanded && this.state.collapsed;
    },
	getInitialState: function() {

        var expanded = (this.props.expanded == undefined) ? true : this.props.expanded;

        if(this.props.remote) {
            return { html: '<span className="mm-widget-notready"></span>',
                     type: null,
                     expanded: expanded,
                     collapsed: true,
                     disabled: false,
                     customicon: this.props.data ? this.props.data.customicon : this.props.customicon,
                     buttons: { minimize : false, tooltip: false } };
        } else {
            return { html: this.props.html || '<span className="mm-widget-notready"></span>',
                     title: this.props.title,
                     type: this.props.type ? this.props.type : "plain",
                     expanded: expanded,
                     collapsed: true,
                     disabled: false,
                     customicon: this.props.data ? this.props.data.customicon : this.props.customicon,
                     buttons: this.props.buttons || { minimize : false, tooltip: false }
                };
        }
        
    	
    },
	componentDidMount: function() {
    	var buttons = {}, title = "", type = "";
        if(this.props.remote && this.state.expanded) {
            $.ajax({
                url: this.props.source, dataType: "json"
            })
            .then(function(response) {
                if(!response)
                    return false;
                if(response.disabled) {
                    this.setState({ disabled: true })
                } else {
                    this.setState({ html: '<span className="mm-widget-notready"></span>',
                         title: response.title,
                         html: response.html,
                         type: response.type ? response.type : "plain",
                         expanded: response.expanded || true,
                         customicon: response.customicon,
                         buttons: response.buttons || { minimize : false, tooltip: false }
                    });

                    if(response.data.html)
                        this.refs.content.setState({ html: response.data.html });
                    if(response.data.customicon)
                        this.setState({ customicon: response.data.customicon });
                    if(this.refs.content.remoteData)
                        this.refs.content.remoteData(response.data);
                    if(this.refs.header)
                        this.refs.header.setState({ label: this.state.title, buttons: this.state.buttons, overridetooltip: response.overridetooltip });
                }
            }.bind(this))
            .fail(function(error) {
                console.error("Error retrieving portlet", this.props.source);
                PubSub.publishSync("widget.async_rendered");
                 this.setState({ failed: true });
            }.bind(this))
            .done(function() {
                PubSub.publishSync("widget.async_rendered");
            });
            
        } else {

            if(this.refs.content.remoteData)
                this.refs.content.remoteData(this.props.data);
            if(this.refs.header)
                this.refs.header.setState({ label: this.state.title, buttons: this.state.buttons });
        }

    },
    render: function() {
        var key = "widget_header_" + this.props.widget_id,
            ckey = "widget_content_" + this.props.widget_id,
            widgetcontent = null,
            classname = "widget widget_plain";


        if(this.state.type && !this.state.failed) {
            widgetcontent = this.WidgetFactory(this.state.type, { key: ckey, html: this.state.html, isExpanded: this.isExpanded, isVisible: this.isVisible, data: this.props.data || null, widget_id: this.props.widget_id });
            classname = "widget widget_"+ this.state.type;
        } else {
            widgetcontent = this.WidgetFactory("plain", { key: ckey, html: this.state.html, isExpanded: this.isExpanded, isVisible: this.isVisible, data: this.props.data || null });
        }
        if(this.state.disabled || this.state.failed) {
            classname = classname += " widget_disabled";
        } else {
            this.state.expanded ? classname += " widget_expanded" : classname += " widget_notexpanded";
            this.state.collapsed ? classname += " widget_collapsed" : classname += " widget_contracted";     
        }

        if(this.state.customicon) {
            classname += " widget_icon_"+this.state.customicon;
        }
        var WidgetHeader = this.WidgetHeader;
        
        return (React.createElement("div", {draggable:  this.props.draggable, className:  classname, "data-id":  this.props.widget_id, "data-type":  this.state.type}, 
            this.state.type && this.state.type!="outdoor" && 
            React.createElement(WidgetHeader, {
                ref: "header", 
                key:  key, 
                type: this.state.type, 
                widget_id:  this.props.widget_id, 
                collapsed:  this.state.collapsed, 
                updateCollapse:  this.handleUpdateCollapse, 
                removeWidget:  this.props.handleRemoveWidget, 
                draggable:  this.props.draggable, 
                datatitle: this.props.title
            }), 
            
             widgetcontent 
            
        ));

    }
});

module.exports = Widget;
},{}],4:[function(require,module,exports){

var WidgetsManager = React.createClass({displayName: "WidgetsManager",

    _addWidget: function(widget) {
        PubSub.publish("widget.added", { widget_id: widget })
    },

	componentWillMount: function() {

        PubSub.subscribe("workspace.not_rendered_widgets", function(msg, data) {
            this.setState({ to_show_widgets: data.widgets}) 
        }.bind(this));

    },

    toggleView: function() {
        this.setState({ expanded: !this.state.expanded })
    },
    
    getInitialState: function() {

        var all_widgets_json = {};
        for(var i=0; i<this.props.allwidgets.length; i++) {
            var widget = this.props.allwidgets[i];
            all_widgets_json[widget.id] = { name: widget.name };
        }
        return {to_show_widgets: null, all_widgets_json: all_widgets_json, expanded: true };

    },
    
 	render: function() {

        var nodes = [], all_widgets_json = this.state.all_widgets_json, addWidget = this._addWidget, className = "";

        if(this.state.to_show_widgets) {
            nodes = this.state.to_show_widgets.map(function(widget) {
                return React.createElement("li", {key:  "widget_" + widget, onClick:  addWidget.bind(null, widget) }, 
                         all_widgets_json[widget].name
                );
            });

            if(this.state.to_show_widgets.length > 3) {
                nodes.push(React.createElement("li", {key:  "widget_toggle", className: "togglelink", onClick:  this.toggleView}, 
                    React.createElement("i", {className: "fa fa-ellipsis-h"})
                ));

            } 
        }

    	return  (
        	React.createElement("div", {className:  this.state.expanded ? "expanded" : ""}, 
             this.state.to_show_widgets == null && "nenhum portlet a adicionar", 
             nodes 
            )
        );

    }
});

module.exports = WidgetsManager;
},{}],5:[function(require,module,exports){
var Workspace = React.createClass({displayName: "Workspace",

    widgets_count: null,
    dragSrcEl: null,
    isMovingOn: null,

    _build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },

    draghandlers: function(node) {
        var self = this;

        function start(e) {
            self.dragSrcEl = this;
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData('text/html', this.innerHTML);
        }

        function enter(e) {
            console.log("oioi")

            var droppable = $(e.target).parents('.widget');
            if(!$(droppable).hasClass("moving")) {

                if (self.isMovingOn && self.isMovingOn.length > 0 && self.isMovingOn[0]!=self.dragSrcEl) {
                    self.isMovingOn[0].style.marginTop = 0;
                    self.isMovingOn[0].style.marginBottom = "20px";
                    self.isMovingOn.removeClass("moving");
                }

                $(droppable).addClass("moving");

                self.isMovingOn = $(droppable);
                
                if(droppable.length > 0 && droppable[0]!=self.dragSrcEl) {
                    
                    var height = droppable[0].offsetHeight, top = $(droppable).offset().top;

                    if(e.pageY > top - height / 2)
                        droppable[0].style.marginTop = self.dragSrcEl.offsetHeight + "px";
                    else
                        droppable[0].style.marginBottom = (self.dragSrcEl.offsetHeight + 20) + "px";
                }

            } 

           return false;

        }

        function over(e) {
            if (e.preventDefault) {
                e.preventDefault(); // Necessary. Allows us to drop.
            }
            e.dataTransfer.dropEffect = 'move';

            return false;
        }

        function drop(e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            }
     
            return false;
        }

        function end(e) {

        
            //checa se fez drop em cima dele mesmo
           var coordinates= {
                start : {
                    y: $(self.dragSrcEl).offset().top,
                    x: $(self.dragSrcEl).offset().left
                },
                end: {
                    y: $(self.dragSrcEl).offset().top + $(self.dragSrcEl).outerHeight(),
                    x: $(self.dragSrcEl).offset().left + $(self.dragSrcEl).outerWidth()
                }
           }
          
           var isinside = (e.clientY >= coordinates.start.y && e.clientY <= coordinates.end.y) &&  (e.clientX >= coordinates.start.x && e.clientX <= coordinates.end.x);
          
           if(isinside) {
                if (self.isMovingOn && self.isMovingOn.length > 0 && self.isMovingOn[0]!=self.dragSrcEl) {
                    self.isMovingOn[0].style.marginTop = 0;
                    self.isMovingOn[0].style.marginBottom = "20px";
                    self.isMovingOn.removeClass("moving");
                }
                return false;
           }

            //checa se está fazendo drop num local válido     
            if (self.isMovingOn && self.isMovingOn.length && (e.srcElement != self.isMovingOn[0]) && (self.dragSrcEl != self.isMovingOn[0])) {
          
            
                var allcolwidgets = self.state.widgets;
                
                //self.dragSrcEl.style.opacity = "0";


                //remove from previous location
                for(key in self.state.widgets) {
                    var arr = [], colwidgets = self.state.widgets[key];
                    for(var i=0; i<colwidgets.length; i++) {
                       if(colwidgets[i]!=$(self.dragSrcEl).data("id"))
                        arr.push(colwidgets[i]);
                    }
                    allcolwidgets[key] = arr;
                }


                if($(self.isMovingOn).hasClass("widget_ghost")) {
                    allcolwidgets[$(self.isMovingOn).parent().data("id")] = [$(self.dragSrcEl).data("id")];
                } else {
                
                    for(key in self.state.widgets) {
                        var arr = [], colwidgets = self.state.widgets[key];
                        for(var i=0; i<colwidgets.length; i++) {
                   
                            if(colwidgets[i]==$(self.isMovingOn).data("id"))
                                arr.push($(self.dragSrcEl).data("id"));
                               arr.push(colwidgets[i]);
                        }
                        allcolwidgets[key] = arr;
                    }

                }

               self.setState({ widgets: allcolwidgets });

               if (self.isMovingOn && self.isMovingOn.length > 0 && self.isMovingOn[0]!=self.dragSrcEl) {
                    self.isMovingOn[0].style.marginTop = 0;
                    self.isMovingOn[0].style.marginBottom = "20px";
                    self.isMovingOn.removeClass("moving");
                }


            } else {
                if (self.isMovingOn && self.isMovingOn.length > 0 && self.isMovingOn[0]!=self.dragSrcEl) {
                    self.isMovingOn[0].style.marginTop = 0;
                    self.isMovingOn[0].style.marginBottom = "20px";
                    self.isMovingOn.removeClass("moving");
                }
            }
            $(e.srcElement).removeClass("moving")
            self.isMovingOn = null;
        }

        node.removeEventListener('dragstart', start);
        node.removeEventListener('dragenter', enter);
        node.removeEventListener('dragover', over);
        node.removeEventListener('drop', drop);
        node.removeEventListener('dragend', end);


        node.addEventListener('dragstart', start, false);
        node.addEventListener('dragenter', enter, false);
        node.addEventListener('dragover', over, false);
        node.addEventListener('drop', drop, false);
        node.addEventListener('dragend', end, false);

        //node.addEventListener('dragleave', function(e) {
        //}, false);

        
    },
    
    drag: function() {

        var widgets = $('.workspace .widget');
        var self = this;
        [].forEach.call(widgets, function(widget) {
            self.draghandlers(widget);
        });
    },

    reset: function() {
        var initwidgets = this.props.defaultworkspace;
        this.setState({columns: this.props.columns || [], widgets: initwidgets });
    },

    updatePosition: function() {

        var positions = {};
        this.state.columns.forEach(function(column) {
            var node = this.refs["column_" + column].getDOMNode(),
                widgets = node.querySelectorAll(".widget"),
                array = Array.prototype.map.call(widgets, function(item) {
                    if(item.getAttribute("data-id"))
                        return (item.getAttribute("data-id"));
                });
            positions[column] = array;

        }.bind(this));

        var ret = this._build_data(this.props.source.updateposition)

        $.ajax({
            url: ret.url, data: {method: ret.data.method, authenticityToken: ret.data.authenticityToken, key: "positions_" + this.props.homepageId, "valor": JSON.stringify(positions) }, type: "post"
        })
        .done(function(response) {
           //console.log("Workspace updated");
        })
        .fail(function(jqXHR) {
            console.error("Error whyle trying to update workspace positions". jqXHR);
        });
    },

    /*
    doDraggable: function() {

        this.drag();

    },
    */

    componentDidMount: function() {
        var self = this;

        PubSub.subscribe( 'workspace.reset', function(msg, data) {
            this.reset();
        }.bind(this));

        PubSub.subscribe( 'widget.removed', function(msg, data) {
            
            var allcolwidgets = self.state.widgets;
            for(key in self.state.widgets) {
 
                var arr = [], colwidgets = self.state.widgets[key];
                for(var i=0; i<colwidgets.length; i++) {
                   if(colwidgets[i]!=data.widget_id)
                    arr.push(colwidgets[i]);
                }
                allcolwidgets[key] = arr;
             
            }
    
            self.setState({ widgets: allcolwidgets });
        });

        PubSub.subscribe( 'widget.added', function(msg, data) {
            for(key in self.props.defaultworkspace) {

                if(self.props.defaultworkspace[key].indexOf(data.widget_id)>=0) {
                    var allcolwidgets = self.state.widgets;
                    if(!allcolwidgets[key])
                        allcolwidgets[key] = []
                    allcolwidgets[key].push(data.widget_id);
                    self.setState({ widgets: allcolwidgets })
                }
            }

        })

        this.drag();

        var counter = 0;
        PubSub.subscribe("widget.async_rendered", function(msg, data) {
            counter++;
            if(counter == self.widgets_count) {
                PubSub.publish("workspace.async_ready");
            }
        });

        var count = 0,
            widgets = this.state.widgets;
        this.state.columns.forEach(function(column) {
            if(widgets[column] && widgets[column][0]!=null)
                count+= widgets[column].length;
        })
        this.widgets_count = count;


        PubSub.subscribe("widget.hideme", function(msg, data) {
            $(self.getDOMNode()).find("div[data-id='" + data.widget_id + "']").addClass("hidden");
        });
        this.updatePosition();
    },


    componentDidUpdate: function() {


        //this.doDraggable();
        this.drag();
        this.updatePosition();

    },

    getInitialState: function() {
        var userworkCollums = Object.keys(this.props.userworkspace),
            defaultCollums = Object.keys(this.props.defaultworkspace),
            arr = new Array;
        
        defaultCollums.forEach(function(columnid) {

            userworkCollums.filter(function(n){
                if(n == columnid) {arr.push(n)}
            });
            
        });

        var initwidgets = userworkCollums.length > 0 ? this.props.userworkspace : this.props.defaultworkspace;

        return {columns: this.props.columns || [], widgets: initwidgets };
    },

    componentWillUnmount: function() {
        PubSub.unsubscribe( 'widget.removed' );
        PubSub.unsubscribe( 'widget.added' );
    },

    render: function() {


        var cols = [];
        var classname = this.props.column_classname, 
            widgetsource = this.props.source.widgetsource;


        var not_rendered_widgets = [], rendered_widgets = [], all_widgets = [];

        var widgets = []; 
        this.state.columns.map(function(column) {


            widgets = this.state.widgets[column];
        
            var c = React.createElement(MMCafeReact.WS.Column, {
                            key: column, 
                            ref: "column_" + column, 
                            col_id: column, 
                            col_classname:  classname + " mm-ws-col", 
                            widgetRemoved:  this.widgetRemoved, 
                            widgetSource:  widgetsource, 
                            widgets:  widgets 
                            }
                      )
            cols.push(c);

            rendered_widgets.push(widgets);
            all_widgets.push(this.props.defaultworkspace[column]);

           

        }, this);

        rendered_widgets = (Array.prototype.concat.apply([], rendered_widgets));
        all_widgets = (Array.prototype.concat.apply([], all_widgets));

        rendered_widgets = rendered_widgets.map(function(s) {
            return parseInt(s);
        });

        all_widgets.filter(function(item, index) {
            if(rendered_widgets.indexOf(item)<0)
                not_rendered_widgets.push(item);
        })

        PubSub.publish("workspace.not_rendered_widgets", { widgets: not_rendered_widgets });
        
        return  (
            React.createElement("div", {className:  "row" }, 
             cols 
            )
        );

    }
});

module.exports = Workspace;
},{}],6:[function(require,module,exports){
var WS = {};

WS.Column = require("../Column");
WS.ExpandableColumn = require("../ExpandableColumn");
WS.Widget = require("../Widget");
WS.WidgetsManager = require("../WidgetsManager");
WS.Workspace = require("../Workspace");

module.exports = WS;
},{"../Column":1,"../ExpandableColumn":2,"../Widget":3,"../WidgetsManager":4,"../Workspace":5}],7:[function(require,module,exports){
var WS = require("../WS");
window.MMCafeReact = window.MMCafeReact || {};
MMCafeReact.WS = WS;
},{"../WS":6}]},{},[7]);
