
var Column = React.createClass({
    displayName: "Column",


    render: function () {
        var widgets = [];
        if (this.props.widgets && this.props.widgets.length > 0) {
            this.props.widgets.forEach(function (widget_id, index) {
                var url = this.props.widgetSource.replace("#{id}", widget_id);
                if (widget_id != null) widgets.push(React.createElement(MMCafeReact.WS.Widget, {
                    key: "widget_" + widget_id,
                    ref: "widget_" + widget_id,
                    widget_id: widget_id,
                    type: "plain",
                    remote: true,
                    source: url,
                    expanded: true,
                    draggable: true,
                    handleRemoveWidget: this.props.widgetRemoved
                }));
            }.bind(this));
        }

        var classname = this.props.col_classname;
        if (widgets.length == 0) {
            classname += " column_ghost";
            widgets.push(React.createElement(
                "div",
                { className: "widget widget_ghost", id: "ghost_" + this.props.col_id, key: "widget_ghost" },
                React.createElement("div", null)
            ));
        }

        return React.createElement(
            "div",
            { className: classname, "data-id": this.props.col_id },
            widgets
        );
    }
});

module.exports = Column;
