

var ExpandableColumn = React.createClass({
    displayName: "ExpandableColumn",


    ExpandWidgetButton: React.createClass({
        displayName: "ExpandWidgetButton",


        handleClick: function () {
            this.setState({ force_expand: !this.state.force_expand });
        },

        getInitialState: function () {
            return { expanded: this.props.expanded, force_expand: false };
        },

        componentDidMount: function () {},

        render: function () {
            var Widget = MMCafeReact.WS.Widget,
                className = "btn-toggle-widget btn-toggle-widget-" + this.props.type;

            this.state.expanded ? className += " btn-toggle-expanded" : className += " btn-toggle-notexpanded";
            var divclass = this.state.force_expand ? "active" : "";

            return React.createElement(
                "div",
                { key: "expandablewidgetbutton_" + this.props.widget_id },
                React.createElement("button", { className: className, "data-target": this.props.type, onClick: this.handleClick }),
                React.createElement(
                    "div",
                    { className: divclass },
                    React.createElement(Widget, {
                        key: "widget_" + this.props.widget_id,
                        ref: "widget_" + this.props.widget_id,
                        widget_id: this.props.widget_id,
                        type: this.props.type,
                        expanded: true,
                        title: this.props.title,
                        data: this.props.data,
                        html: this.props.html
                    })
                )
            );
        }

    }),

    handleBtnClick: function (opts) {},

    getInitialState: function () {
        return { widgets: this.props.widgets };
    },

    componentDidMount: function () {
        var col = this.refs.column;

        var self = this,
            footerpos = $("footer#footer").position(),
            minheight = 0;

        this.state.widgets.map(function (item, i) {
            var btn = self.refs["button_" + item.widget_id],
                widget = btn.refs["widget_" + item.widget_id],
                p = $(widget.getDOMNode()).position();

            var ph = p.top - $("#column-to-expand").position().top;

            minheight = ph + $(widget.getDOMNode()).height() > minheight ? ph + $(widget.getDOMNode()).height() : minheight;
        });
        $("#column-to-expand").css("min-height", minheight - 30);
    },

    render: function () {

        var ExpandWidgetButton = this.ExpandWidgetButton,
            Widget = MMCafeReact.WS.Widget;
        return React.createElement(
            "div",
            { id: "column-to-expand", ref: "column" },
            this.state.widgets.map(function (item, i) {
                return React.createElement(ExpandWidgetButton, {
                    key: "button_" + item.widget_id,
                    ref: "button_" + item.widget_id,
                    widget_id: item.widget_id,
                    type: item.type,
                    expanded: true,
                    title: item.title,
                    data: item.data,
                    html: item.html,
                    handleBtnClick: this.handleBtnClick
                });
            }, this)
        );
    }
});

module.exports = ExpandableColumn;
