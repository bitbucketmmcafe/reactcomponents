var Widgets = {};

Widgets.WidgetContentPlain = require("./../widgets/WidgetContentPlain");
Widgets.WidgetContentBirthdayCalendar = require("./../widgets/WidgetContentBirthdayCalendar");
Widgets.WidgetContentBirthdays = require("./../widgets/WidgetContentBirthdays");
Widgets.WidgetContentCalendar = require("./../widgets/WidgetContentCalendar");
Widgets.WidgetContentContacts = require("./../widgets/WidgetContentContacts");
Widgets.WidgetContentFavorites = require("./../widgets/WidgetContentFavorites");
Widgets.WidgetContentFeeds = require("./../widgets/WidgetContentFeeds");
Widgets.WidgetContentForum = require("./../widgets/WidgetContentForum");
Widgets.WidgetContentLastComments = require("./../widgets/WidgetContentLastComments");
Widgets.WidgetContentLastUpdates = require("./../widgets/WidgetContentLastUpdates");
Widgets.WidgetContentLinks = require("./../widgets/WidgetContentLinks");
Widgets.WidgetContentMostViews = require("./../widgets/WidgetContentMostViews");
Widgets.WidgetContentNews = require("./../widgets/WidgetContentNews");
Widgets.WidgetContentNotepad = require("./../widgets/WidgetContentNotepad");
Widgets.WidgetContentOfferrides = require("./../widgets/WidgetContentOfferrides");
Widgets.WidgetContentOurTeam = require("./../widgets/WidgetContentOurTeam");
Widgets.WidgetContentPoll = require("./../widgets/WidgetContentPoll");
Widgets.WidgetContentRequiredRead = require("./../widgets/WidgetContentRequiredRead");
Widgets.WidgetContentTags = require("./../widgets/WidgetContentTags");
Widgets.WidgetContentTasks = require("./../widgets/WidgetContentTasks");
Widgets.WidgetContentUserSearch = require("./../widgets/WidgetContentUserSearch");
Widgets.WidgetContentWarnings = require("./../widgets/WidgetContentWarnings");
Widgets.WidgetContentWeather = require("./../widgets/WidgetContentWeather");

module.exports = Widgets;
