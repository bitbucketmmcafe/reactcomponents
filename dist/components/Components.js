var Components = {};

Components.AutocompleteUser = require("../components/AutocompleteUser");
Components.BranchLabel = require("../components/BranchLabel");
Components.Calendar = require("../components/Calendar");
Components.CBD = require("../components/CBD");
Components.CompanyLabel = require("../components/CompanyLabel");
Components.ContactUser = require("../components/ContactUser");
Components.DateLabel = require("../components/DateLabel");
Components.DepartmentLabel = require("../components/DepartmentLabel");
Components.Favorite = require("../components/Favorite");
Components.Link = require("./../components/Link");
Components.List = require("../components/List");
Components.Loader = require("../components/Loader");
Components.Rating = require("../components/Rating");
Components.Translator = require("../components/Translator");
Components.UserLink = require("../components/UserLink");
Components.GRGGL = require("../components/GRGGL");

module.exports = Components;
