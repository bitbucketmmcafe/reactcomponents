(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Component para link tag
 */
var Link = React.createClass({displayName: "Link",

	propTypes: {
		/**
		 * Dados do link: data = { name: "text for link", url: "http://mmcafe.com.br" }
		 */
		data: React.PropTypes.object,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string,
		/**
		 * target do link: target = "_blank"
		 */
		target: React.PropTypes.string
	},

    render: function() {
    	var regex = /(&nbsp;|<([^>]+)>)/ig;
        return React.createElement("a", {href: this.props.data.url, 
        		  title:  this.props.data.name, 
        		  className:  this.props.classname, 
        		  target:  this.props.data.target, 
        		  "data-remote":  this.props.data.remote, 
        		  dangerouslySetInnerHTML: { __html: this.props.data.name}});
    }

});

module.exports = Link;

},{}],2:[function(require,module,exports){
/**
 * Component para link de usuário
 */
var UserLink = React.createClass({displayName: "UserLink",

    propTypes: {
        /**
         * Usuário, é um json
         */
        user: React.PropTypes.object,
        /**
         * classe css: classname = "col-md-3" 
         */
        classname: React.PropTypes.string
    },

    render: function() {
        var month = (new Date()).getMonth()+1, day = (new Date()).getDate(), classname = this.props.classname || "mm-link-user";

        if(this.props.user.birthdate) {
            var nodes = this.props.user.birthdate.split("/").map(function(item) {
                return parseInt(item);
            });

            if(nodes[0] == day && nodes[1] == month) {
                classname += " mm-user-birthday";
            }

        }

        if(this.props.user.onVacation) {
            classname += " mm-user-vacation";   
        }

        var data = {name: this.props.user.preferedname, url: "/m/usuario/" + this.props.user.id, remote: true};
        return React.createElement(MMCafeReact.Components.Link, {data:  data, classname:  classname });
    }

});

module.exports = UserLink;

},{}],3:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentNews.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentNews = widget;

},{"../../../widgets/WidgetContentNews.js":4}],4:[function(require,module,exports){
var Link = require("../components/Link");
var UserLink = require("../components/UserLink");


var WidgetContentNews = React.createClass({displayName: "WidgetContentNews",

    
    _formatDate: function(date) {

    },

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        var SPACE = ' ';
        if(this.state.data && this.state.data.news) {
            var po_publishinfo_type = this.state.data.po_publishinfo_type;
			var nodes = this.state.data.news.map(function(n) {

                var status = null;
                
                if(this.state.data.check_statusaccess) {

                    var hash = {
                            1: {
                                "status": "accessed",
                                "label": MMCafeReact.Components.Translator.getI18n("statusaccess.accessed"),
                                "icon": "check-circle"
                            },
                            2: {
                                "status": "notaccessed",
                                "label": MMCafeReact.Components.Translator.getI18n("statusaccess.notaccessed"),
                                "icon": "info-circle"
                            },
                            3: {
                                "status": "urgent",
                                "label": MMCafeReact.Components.Translator.getI18n("statusaccess.urgent"),
                                "icon": "exclamation-triangle"
                            }
                    }

                    var statusaccess_item = hash[n.statusaccess];
                    var classname = "mm-statusaccess-label mm-statusaccess-" + statusaccess_item.status;
                    var classname_icon = "fa fa-" + statusaccess_item.icon; 
                    status = React.createElement("span", {className:  classname },  SPACE, 
                        React.createElement("i", {className:  classname_icon, "aria-hidden": "true"}), 
                         statusaccess_item.label
                    )
                }
                n.remote = true;

                return React.createElement("li", {key:  "listitem_" + n.id}, 

                             n.thumb && 
                            React.createElement("img", {src:  n.thumb, title:  n.name}), 
                            
                            React.createElement("h3", null, 
                                React.createElement(Link, {data:  n }), 
                                 status 
                            ), 
                             po_publishinfo_type!="nothing" && po_publishinfo_type=="all" &&
                            React.createElement("span", null, 
                                React.createElement(MMCafeReact.Components.Translator, {key:  "translatefor_" + n.id, value:  "content.updated.from" }), 
                                 SPACE, 
                                React.createElement(UserLink, {user:  n.lasteditedby}), 
                                 SPACE, 
                                React.createElement(MMCafeReact.Components.Translator, {key:  "translateat_" + n.id, value:  "content.updated.at" }),  SPACE, 
                                React.createElement(MMCafeReact.Components.DateLabel, {iso8061date: moment.utc().format(n.lasteditiondate), pattern:  n.lasteditiondate.date_pattern || "DD/MM/YY - HH:mm"})
                            ), 
                            
                             po_publishinfo_type!="nothing" && po_publishinfo_type!="all" &&
                                React.createElement("span", null, 
                                    React.createElement(MMCafeReact.Components.Translator, {key:  "translatefor_" + n.id, value:  "po.published" }), 
                                     SPACE, 
                                    React.createElement(MMCafeReact.Components.Translator, {key:  "translateat_" + n.id, value:  "content.updated.at" }),  SPACE, 
                                    React.createElement(MMCafeReact.Components.DateLabel, {iso8061date: moment.utc().format(n.lasteditiondate), pattern:  n.lasteditiondate.date_pattern || "DD/MM/YY - HH:mm"})
                                ), 
                            
                            React.createElement("p", null, 
                                 n.description
                            )
                        );
			}.bind(this));
			
        }

        return (
            React.createElement("div", {className: "widget_content"}, 
                React.createElement("ul", null, 
                 nodes 
                ), 
                 this.state.data && this.state.data.url_folder &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_folder},  this.state.data.url_label))
            	
            )
        );
    }
});

module.exports = WidgetContentNews;
},{"../components/Link":1,"../components/UserLink":2}]},{},[3]);
