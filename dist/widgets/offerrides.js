(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentOfferrides.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentOfferrides = widget;
},{"../../../widgets/WidgetContentOfferrides.js":2}],2:[function(require,module,exports){

/**
 * WidgetContent para Caronas. Faz parte do pacote MMCafeReact.Widgets
 */
var WidgetContentOfferrides = React.createClass({displayName: "WidgetContentOfferrides",

    remoteData: function(data) {
        this.setState({ data: data });
        this.getRides();
    
    },
    getRides: function() {

        if(!this.state.data || !this.state.data.source)
            return false;


        var self = this;
        $.ajax({
            url: self.state.data.source.rides
        })
        .then(function(response) {
            self.setState({ rides: response, error: null });
        })
        .fail(function(err) {
            self.setState({ error: err });
        })
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        if(!this.props.isVisible())
            return false;

        if(this.state.error) {
            return (React.createElement("div", {className: "widget_content"}, 
                 React.createElement("div", {className: "widget-content-error"},  this.state.error.statusText), 
                 this.state.data && this.state.data.url_forum &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_forum},  this.state.data.url_label))
                
            ))
        }


        var self = this, list = [];
        if(this.state.rides) {
			var nodes = this.state.rides.map(function(r) {
			    return React.createElement("li", null, 
                    React.createElement("img", {src:  r.absolutethumb}), 
                    React.createElement("a", {href:  r.absoluteurl, target: "_blank"}, 
                    "De ",  r.start_address, " a ",  r.end_address
                    )
                )
            });
			list = nodes;
        }

        return (
            React.createElement("div", {className: "widget_content"}, 
                React.createElement("ul", null, 
                 list 
                ), 
                 this.state.data && this.state.data.url_modulo &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_modulo},  this.state.data.url_label))
            	
            )
        );
    }
});

module.exports = WidgetContentOfferrides;

},{}]},{},[1]);
