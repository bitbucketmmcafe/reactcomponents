(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentPoll.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentPoll = widget;
},{"../../../widgets/WidgetContentPoll.js":2}],2:[function(require,module,exports){
var WidgetContentPoll = React.createClass({displayName: "WidgetContentPoll",

    _build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },

	handleSubmit: function(e) {
		var form = e.target, 
            val = form.elements.answers.value;

		PubSub.publish("components.loader.add", { key: "poll.submit" } );

        var url = this.state.data.source.vote + form.querySelector('input[name="answers"]:checked').value;
        var builded_data = this._build_data(url);
        $
        .ajax({url: builded_data.url, data: builded_data.data, method: 'post' })
		.then(function(response) {
			PubSub.publish("components.loader.remove", { key: "poll.submit", type: "success" } ); 
			this.setState({ result: response, voted: true });
            window.localStorage.setItem("is_poll_voted_" + this.state.data.poll.id, true);
		}.bind(this))
        .fail(function(jqXHR) {
            PubSub.publish("components.loader.remove", { key: "poll.submit", type: "error", error: jqXHR } ); 
        })

        e.preventDefault();
	},

    updateResults: function(e) {

        if(e)
            PubSub.publish("components.loader.add", { key: "poll.loadingresults" } ); 
        $
        .ajax({ url: this.state.data.source.view })
        .then(function(response) {
            if(e)
                PubSub.publish("components.loader.remove", { key: "poll.loadingresults", type: "success" } ); 
            this.setState({ result: response });
        }.bind(this))
        .fail(function(jqXHR) {
            if(e)
                PubSub.publish("components.loader.remove", { key: "poll.loadingresults", type: "error", error: jqXHR } ); 
        })

        if(e)
            e.preventDefault();
    },

    backToPoll: function(e) {
        this.setState({ result: null });
        e.preventDefault();
    },

    remoteData: function(data) {
        this.setState({ data: data });
        if(window.localStorage.getItem("is_poll_voted_" + data.poll.id)) {
            this.setState({ voted: true });
            this.updateResults();
        }
            
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data, voted: false };
    },

    render: function() {
        
        var self = this;

        if(this.state.result) {

        	var nodes = this.state.result.options.map(function(n) {
                var perc = 0
                if(n.votes > 0){
                    perc = Math.round(100 * n.votes / self.state.result.total )
                }

        		divStyle = { width: perc + "%"}

                return React.createElement("li", {key:  "listitem_" + n.id}, 
                           n.name, 
                          React.createElement("div", {className: "progress"}, 
								React.createElement("div", {className: "progress-bar", role: "progressbar", "aria-valuenow":  perc, "aria-valuemin": "0", "aria-valuemax": "100", style: divStyle}, 
								 perc, "%"
								)
							)
                        );
			});

        	return (
	            React.createElement("div", {className: "widget_content"}, 
	          		React.createElement("h4", null,  this.state.data.poll.name), 

                     this.state.html && 
                    React.createElement("div", {className: "htmlPoll", dangerouslySetInnerHTML: {__html:this.state.html}}), 
                    

	          		React.createElement("ul", null, 
                		 nodes 
                	), 
                     !this.state.voted &&
                    React.createElement("button", {className: "btn btn-primary", onClick:  this.backToPoll}, React.createElement(MMCafeReact.Components.Translator, {value:  "poll.back" }))
                    
	            )
	        );
        }

        /* não está vendo os resultados */
        if(this.state.data && this.state.data.poll ) {
			var nodes = this.state.data.poll.options.map(function(n) {
                return React.createElement("li", {key:  "listitem_" + n.id}, 
                          React.createElement("input", {type: "radio", name: "answers", required: true, value:  n.id}), React.createElement("span", null,  n.name)
                        );
			});
			
        }

        return (
            React.createElement("div", {className: "widget_content"}, 
             this.state.data && 
                React.createElement("form", {onSubmit: this.handleSubmit}, 
                	React.createElement("h4", null,  this.state.data.poll.name), 

                     this.state.html && 
                    React.createElement("div", {className: "htmlPoll", dangerouslySetInnerHTML: {__html:this.state.html}}), 
                    

                	React.createElement("ul", null, 
                		 nodes 
                	), 
                	React.createElement("button", {className: "btn btn-primary btn-mm-defaultform-submit"}, React.createElement(MMCafeReact.Components.Translator, {value:  "poll.submit" })), 
                    React.createElement("button", {className: "btn btn-primary btn-mm-defaultform-submit", onClick:  this.updateResults}, React.createElement(MMCafeReact.Components.Translator, {value:  "poll.viewresults" }))
                )
            
            )
        );
    }
});

module.exports = WidgetContentPoll;
},{}]},{},[1]);
