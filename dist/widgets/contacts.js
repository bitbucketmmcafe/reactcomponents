(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentContacts.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentContacts = widget;
},{"../../../widgets/WidgetContentContacts.js":2}],2:[function(require,module,exports){
var WidgetContentContacts = React.createClass({displayName: "WidgetContentContacts",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        var nodes = null;

        if(this.state.data && this.state.data.contacts) {
            nodes = this.state.data.contacts.map(function(user) {
                var ul = null;
                if(user.phone && (user.areaPrefix || user.phone.extension || user.phone.phone)) {
                    ul = React.createElement("ul", null, 
                         (user.phone.areaPrefix || user.phone.phone) &&
                        React.createElement("li", null, 
                            React.createElement(MMCafeReact.Components.Translator, {key:  "t_phonenumber_" + user.id, value:  "phone.number" }), ":",  
                                 user.phone.areaPrefix && React.createElement("span", null, " (",  user.phone.areaPrefix, ")"), " ",  user.phone.phone
                        ), 
                        
                         user.phone.extension &&
                        React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "t_phoneextension_" + user.id, value:  "phone.extension" }), ": ",  user.phone.extension)
                        
                    )
                }
                return React.createElement("li", {key:  "userlifor_" + user.id}, 
                    React.createElement("img", {src:  user.avatar}), 
                    React.createElement(MMCafeReact.Components.UserLink, {key:  "userlinkfor_" + user.id, user:  user }), 
                     ul 
                )
            });
            
        }
        return (
            React.createElement("div", {className: "widget_content"}, 
                React.createElement("ul", null, 
                 nodes 
                ), 
                 this.state.data && this.state.data.url_contacts &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_contacts},  this.state.data.url_label))
                
            )
        );
    }
});

module.exports = WidgetContentContacts;

},{}]},{},[1]);
