(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentIncidents.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentIncidents = widget;
},{"../../../widgets/WidgetContentIncidents.js":2}],2:[function(require,module,exports){
var WidgetContentIncidents = React.createClass({displayName: "WidgetContentIncidents",

    _formatDate: function(date) {

    },

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        var data={}, sameday = [];

        if(this.state.data && this.state.data.incidents) {


            this.state.data.incidents.forEach(function(incidents) {
                data[incidents.publishdate] ?
                    data[incidents.publishdate].push(incidents) :
                    data[incidents.publishdate] = [incidents]; 
            });
        for(var key in data) {
			var nodes = data[key].map(function(n) {
                return (React.createElement("div", {key:  "listitem_" + n.id, className: "incident"}, 
                        
                          React.createElement(MMCafeReact.Components.Link, {data:  n }), 

                          React.createElement("div", {className: n.byprop.class}, n.byprop.status)
                        
                        )
                    );
			});
            sameday.push(React.createElement("div", {key:  "samedate_" + key}, React.createElement("h4", null, " ", React.createElement(MMCafeReact.Components.DateLabel, {iso8061date: moment.utc().format(key), pattern:  key.date_pattern || "DD/MM/YY"})),  nodes ));
        }
			
        }
        return (
            React.createElement("div", {className: "widget_content"}, 
                sameday, 

                 this.state.data &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_folder},  this.state.data.url_label))
                
            )
        );
    }
});

module.exports = WidgetContentIncidents;
},{}]},{},[1]);
