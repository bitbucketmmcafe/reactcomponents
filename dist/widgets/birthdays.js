(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentBirthdays.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentBirthdays = widget;
},{"../../../widgets/WidgetContentBirthdays.js":2}],2:[function(require,module,exports){
var WidgetContentBirthdays = React.createClass({displayName: "WidgetContentBirthdays",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        var data = {}, 
            today = null, 
            nodestoday = [], 
            others = [], 
            hide_company = false,
            hide_company_for_birthday_widget = false,
            show_branch = false, 
            todaymarkup = null, 
            byadmissiondate="false";
        
        if(this.state.data && this.state.data.users) {
            hide_company = this.state.data.hide_company;
            show_branch = this.state.data.show_branch;
            hide_company_for_birthday_widget = this.state.data.hide_company_for_birthday_widget;
            if(this.state.data.byadmissiondate && this.state.data.byadmissiondate==true){
                this.state.data.users.forEach(function(user) {
                    data[user.admissionDate_summary] ?
                        data[user.admissionDate_summary].push(user) :
                        data[user.admissionDate_summary] = [user];
                });

                byadmissiondate= "true";

            }else{
                show_branch = this.state.data.show_branch;
                hide_company_for_birthday_widget = this.state.data.hide_company_for_birthday_widget;
                this.state.data.users.forEach(function(user) {
                    data[user.birthdate] ?
                        data[user.birthdate].push(user) :
                        data[user.birthdate] = [user]; 
                });
                byadmissiondate= "false";
            }
            
            
            var today = moment().format('DD/MM'),
                tomorrow = moment().add(1, 'day');
            
            if(data[today]){

                todaymarkup = React.createElement("h3", null,  today, " - ", React.createElement(MMCafeReact.Components.Translator, {value:  "birthdays.today" }));
            }
                
            for(var key in data) {

                if(key == today) {
                    
                   nodestoday = data[key].map(function(user) {
                    
                        return(React.createElement("div", {key:  "userhighlight_" + user.id}, React.createElement("img", {src:  user.avatar}), 
                                React.createElement("div", {className: "birthday-info"}, 
                                React.createElement(MMCafeReact.Components.UserLink, {key:  "userlinkfor_" + user.id, user:  user }), 
                                 (!hide_company && !hide_company_for_birthday_widget) && user.company &&
                                React.createElement("span", null, 
                                    React.createElement(MMCafeReact.Components.CompanyLabel, {key:  "companyfor_" + user.id, data:  user.company}), 
                                    React.createElement("br", null)
                                ), 
                                
                                 show_branch && user.branch && 
                                    React.createElement("span", null, 
                                        React.createElement(MMCafeReact.Components.BranchLabel, {key:  "branchfor_" + user.id, data:  user.branch})
                                    ), 
                                
                                byadmissiondate=="true" && user.yearsincompany > 0 &&
                                    React.createElement("span", {className: "yearsincompany"}, user.yearsincompany, 
                                        user.yearsincompany == 1 && 
                                            React.createElement("span", null, " ", MMCafeReact.Components.Translator.getI18n('birthdays.year')), 
                                        
                                        user.yearsincompany > 1 && 
                                            React.createElement("span", null, " ", MMCafeReact.Components.Translator.getI18n('birthdays.years'))
                                        
                                    )
                                
                                )
                                ));
                       
                   });
                } else {

                    var nodes = data[key].map(function(user) {
                        return(React.createElement("div", {key:  "userhighlight_" + user.id, className: "widget-content no-avatar"}, 
                                React.createElement(MMCafeReact.Components.UserLink, {key:  "userlinkfor_" + user.id, user:  user }), 
                                 (!hide_company && !hide_company_for_birthday_widget) && user.company &&
                                React.createElement("span", null, 
                                    React.createElement(MMCafeReact.Components.CompanyLabel, {key:  "companyfor_" + user.id, data:  user.company}), 
                                    React.createElement("br", null)
                                ), 
                                
                                 show_branch && user.branch && 
                                React.createElement("div", null, 
                                    React.createElement(MMCafeReact.Components.BranchLabel, {key:  "branchfor_" + user.id, data:  user.branch})
                                ), 
                                
                                byadmissiondate=="true" && user.yearsincompany > 0 &&
                                    React.createElement("span", {className: "yearsincompany"}, user.yearsincompany, 
                                        user.yearsincompany == 1 && 
                                            React.createElement("span", null, " ", MMCafeReact.Components.Translator.getI18n('birthdays.year')), 
                                        
                                        user.yearsincompany > 1 && 
                                            React.createElement("span", null, " ", MMCafeReact.Components.Translator.getI18n('birthdays.years'))
                                        
                                    )
                                
                                ));
                    });
                    others.push(React.createElement("div", {key:  "dateuserhighlight_" + key}, React.createElement("h4", null, 
                                     key != "null" ? key : "" &&
                                        key, 
                                    
                                     tomorrow.format('DD/MM') == key && 
                                    React.createElement("span", null, " - ",  MMCafeReact.Components.Translator.getI18n('birthdays.tomorrow') )
                                    ), 
                                     nodes 
                                ));
                }
            }
        }
        
        return (
            React.createElement("div", {className: "widget_content"}, 
                 todaymarkup, 
                 nodestoday, 
                 nodestoday.length > 0 &&
                React.createElement("hr", null), 
                
               
                 others, 

                 (!nodestoday || nodestoday.length == 0) && others.length == 0 && 
                    React.createElement("div", {className: "alert mm-alert-error"}, 
                        React.createElement(MMCafeReact.Components.Translator, {value:  "birthdaycalendar.noresults" })
                    ), 
                

                 this.state.data &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_birthdays},  this.state.data.url_label))
                
            )
        );
    }
});

module.exports = WidgetContentBirthdays;

},{}]},{},[1]);
