(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentTasks.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentTasks = widget;

},{"../../../widgets/WidgetContentTasks.js":2}],2:[function(require,module,exports){
var WidgetContentTasks = React.createClass({displayName: "WidgetContentTasks",

	_focus: function(event) {
        $(event.target).closest('.widget').attr("draggable", false);
    },

    _blur: function(event) {
        $(event.target).closest('.widget').attr("draggable", true);
    },


	/**
	 * Transforms query string into json object, to be used for post
	 */
	_generate_post_data: function(data) {
		var obj = {};
		this.state.data.source.save.split('?')[1].split('&').forEach(function(item) {
			var x = item.split('=');
			obj[x[0]] = x[1];
		})

		obj.valor = JSON.stringify(data);

		return obj;
	},

	_submit: function(data, key) {
		if(key)
			PubSub.publish("components.loader.add", { key: "task." + key } );

		return $.ajax({
			url: this.state.data.source.save.split('?')[0],
			type: 'post',
			data: this._generate_post_data(data)
		})
		.then(function(response) {
			if(key)
				PubSub.publish("components.loader.remove", { key: "task." + key, type: "success" } );
		}.bind(this))
		.then( this._getTasks )
		.fail(function(jqXHR) {
			console.log('has failed')
			PubSub.publish("components.loader.remove", { key: "task." + key, type: "error", error: jqXHR } );
		});
	},

	handleSubmit: function(e) {

		e.preventDefault();

		var data = this.state.tasks;
		data.push({
			disabled: false,
			text: this.refs.textarea.getDOMNode().value
		});
	
		this._submit(data, 'submit')
		.then( function() {
			this.refs.textarea.getDOMNode().value = "";
		}.bind(this))
	},

	_deleteTask: function(index) {

		var data = this.state.tasks;
		data.splice(index, 1);
		this._submit(data, 'delete');
	
	},

	_toggleDisabled: function(index) {

		var data = this.state.tasks,
			task = data[index];

		data[index].disabled = !data[index].disabled;
		this._submit(data, 'change');

	},

	_getTasks: function() {
		var url = this.state.data.source.list;

		$
		.ajax({ url: url })
		.then(function(response) {
			this.setState({ tasks: response, ready: true });
		}.bind(this))
		.fail(function(jqXHR) {
			console.error('Portlet Tasks - error while get tasks', jqXHR);
			this.setState({ ready: true });
		}.bind(this))
	},

	componentDidMount: function() {
		var el = this.getDOMNode();

		$(el).on("blur", "li span", function() {

			var data = this.state.tasks;
			data[$(event.target).data("index")].text = $(event.target).html();
			this._submit(data, 'change');

		}.bind(this));
	},

	dragStart: function(e) {
        this.dragged = e.currentTarget;
        this.ul = $(e.target).closest("ul")[0];
        this.ul.style.height = this.ul.clientHeight + "px";
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData("text/html", e.currentTarget);
    },

    dragEnd: function(e) {

    	this.ul.style.height = "auto";

        this.dragged.style.display = "block";

        $(".tasklist_placeholder").remove();

        var from = Number(this.dragged.dataset.index),
        	to = Number(this.over.dataset.index);

        var tasks = this.state.tasks;
        tasks.splice(to, 0, tasks.splice(from, 1)[0]);
        this._submit(tasks, null);

    },

    dragOver: function(e) {
        e.preventDefault();
        if(this.dragged)
	        this.dragged.style.display = "none";
        if(e.target.className == "tasklist_placeholder") {
            
        }
        $(".tasklist_placeholder").remove();
        this.over = e.target;
        this.placeholder = document.createElement("li");
        this.placeholder.className = "tasklist_placeholder";
        var t = $(e.target).closest("li");
        if(e.target.parentNode)
	        this.ul.insertBefore(this.placeholder, t[0]);
    },

    remoteData: function(data) {
		this.setState({ data: data });
		this._getTasks();
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data, ready: false, tasks: [] };
    },

    render: function() {
        var self = this;

        var nodes = [];

        if(this.state.tasks) {
        	nodes = this.state.tasks.map(function(n, index) {
        		var className = n.disabled ? "disabled" : "";
                return React.createElement("li", {
                		draggable: "true", 
                		onDragEnd: self.dragEnd, 
                        onDragStart: self.dragStart, 
                		"data-index": index, 
                		key:  "tasklistlistitem_" + index}, 
                        	React.createElement("input", {type: "checkbox", name: "option", value:  index, checked:  n.disabled, onChange: self._toggleDisabled.bind(null,index)}), 
                        	React.createElement("span", {className:  className, contentEditable:  true, "data-index":  index, dangerouslySetInnerHTML: { __html: n.text.replace(/\r?\n/g, '<br />')}}), 
                        	React.createElement("button", {onClick: self._deleteTask.bind(null,index)})
                        );
			});

        }

        return (
            React.createElement("div", {className: "widget_content", onDragOver: this.dragOver}, 

             this.state.tasks && 
            React.createElement("ul", null, 
            	 nodes 
            ), 
            
             !this.state.ready &&
            	React.createElement("span", {className: "mm-widget-notready"}), 
            
            React.createElement("form", {onSubmit: this.handleSubmit, ref: "form"}, 
            	React.createElement("textarea", {required: true, onFocus:  this._focus, onBlur:  this._blur, ref: "textarea"}), 
            	React.createElement("button", {className: "btn btn-primary"}, 
					React.createElement(MMCafeReact.Components.Translator, {value:  "tasks.submit" })
            	)
            )
            )
        );
    }
});

module.exports = WidgetContentTasks;
},{}]},{},[1]);
