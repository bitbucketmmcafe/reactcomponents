(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentTasksmanager.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentTasksmanager = widget;
},{"../../../widgets/WidgetContentTasksmanager.js":2}],2:[function(require,module,exports){
var WidgetContentsTasksmanager = React.createClass({displayName: "WidgetContentsTasksmanager",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        var nextTasksArea = null, overdueTasksArea = null;

        var hasNextTasks = false, hasOverdueTasks = false;

        if(this.state.data && this.state.data.overdueTasks && this.state.data.overdueTasks.length > 0) {
            hasOverdueTasks = true;
            var nodes = this.state.data.overdueTasks.map(function(task) {

                const spanStyle = {
                    color: task.category.color ? task.category.color : 'black'
                };
            
                return React.createElement("li", {key: task.id, className:  'status_' + task.color}, 
                    React.createElement("span", {className: "status_highlight"}, React.createElement(MMCafeReact.Components.Translator, {key:  "last", value:  "tasksmanager.last" }), " ",  task.days, 

                    task.days == 1 && 
                        React.createElement("span", null, " ", React.createElement(MMCafeReact.Components.Translator, {key:  "days", value:  "tasksmanager.day" })), 
                    
                    task.days > 1 && 
                        React.createElement("span", null, " ", React.createElement(MMCafeReact.Components.Translator, {key:  "days", value:  "tasksmanager.days" }))
                    

                    ), 
                    React.createElement("a", {href:  this.state.data.url_talktrack + task.url}, " ", React.createElement("span", {style: spanStyle}, " ",  task.category.name), " ",  task.title), 
                    React.createElement("div", null, React.createElement("em", null, " ", React.createElement(MMCafeReact.Components.Translator, {key:  "last", value:  "tasksmanager.deadline" }), " ",  task.deadline))
                )
            }.bind(this));
            overdueTasksArea = React.createElement("div", null, 
                React.createElement("h3", {className: "overdue"}, React.createElement(MMCafeReact.Components.Translator, {key:  "nexts", value:  "tasksmanager.overdue" })), 
                React.createElement("ul", null,  nodes )
                )
        }


        if(this.state.data && this.state.data.nextTasks && this.state.data.nextTasks.length > 0) {
            hasNextTasks = true;
            var nodes = this.state.data.nextTasks.map(function(task) {

                const spanStyle = {
                    color: task.category.color ? task.category.color : 'black'
                };

                return React.createElement("li", {key: task.id, className:  'status_' + task.color}, 

                    React.createElement("span", {className: "status_highlight"}, 

                    task.days == 1 && 
                    React.createElement("span", null, " ", React.createElement(MMCafeReact.Components.Translator, {key:  "missing", value:  "tasksmanager.miss" }), " ", task.days, " ", React.createElement(MMCafeReact.Components.Translator, {key:  "days", value:  "tasksmanager.day" })), 
                    
                    task.days > 1 && 
                    React.createElement("span", null, " ", React.createElement(MMCafeReact.Components.Translator, {key:  "missing", value:  "tasksmanager.missing" }), " ", task.days, " ", React.createElement(MMCafeReact.Components.Translator, {key:  "days", value:  "tasksmanager.days" }))
                    
                    ), 
                    React.createElement("a", {href:  this.state.data.url_talktrack + task.url}, React.createElement("span", {style: spanStyle}, " ",  task.category.name), " ",  task.title), 
                    React.createElement("div", {className: "taskDates"}, React.createElement("em", null, " Início: ", moment(task.updatedAt).format('DD/MM/YYYY'), " - Fim: ",  task.deadline))
                )
            }.bind(this));
            nextTasksArea = React.createElement("div", null, 
                React.createElement("h3", {className: "next"}, React.createElement(MMCafeReact.Components.Translator, {key:  "nexts", value:  "tasksmanager.next" })), 
                React.createElement("ul", null,  nodes )
                )
        }
        return (
            React.createElement("div", {className: "widget_content"}, 
                 overdueTasksArea, 
                 nextTasksArea, 
                 (this.state.data && (!hasNextTasks && !hasOverdueTasks)) &&
                React.createElement("div", null, 
                    React.createElement("h4", null, React.createElement(MMCafeReact.Components.Translator, {key:  "congrats", value:  "tasksmanager.congrats" })), 
                    React.createElement("p", null, React.createElement(MMCafeReact.Components.Translator, {key:  "congrats", value:  "tasksmanager.congrats.desc" }))
                ), 
                
                 this.state.data && this.state.data.url_talktrack &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_talktrack},  this.state.data.url_label))
                
            )
        );
    }
});

module.exports = WidgetContentsTasksmanager;

},{}]},{},[1]);
