(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentWarnings.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentWarnings = widget;

},{"../../../widgets/WidgetContentWarnings.js":2}],2:[function(require,module,exports){
var WidgetContentWarnings = React.createClass({displayName: "WidgetContentWarnings",

	remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    componentDidMount: function() {
    	var self = this;
    	PubSub.subscribe("widget.warnings.read", function(msg, data) {
    		if(self.state.data && self.state.data.warnings) {
    			var filtered = self.state.data.warnings.filter(function(item) {
    				return item.id != data.warning_id;
    			});
    			var selfdata = self.state.data;
    			selfdata.warnings = filtered;
    			self.setState({ data: data });
    		}
    	});
    },

    render: function() {
        
        if(this.state.data && this.state.data.warnings) {
			var nodes = this.state.data.warnings.map(function(n) {
                return React.createElement("li", {key:  "listitem_" + n.id}, 
                             n.thumb && 
                            React.createElement("img", {src:  n.thumb, title:  n.name, className: "img-responsive"}), 
                            
                            React.createElement("h3", null, 
                                React.createElement(MMCafeReact.Components.Link, {data:  n })
                            ), 
                            React.createElement(MMCafeReact.Components.Translator, {key:  "translateat_" + n.id, value:  "content.updated.at" }), " ", 
                             moment(n.lasteditiondate).format("DD/MM/YY"), 
                             n.description &&
                            React.createElement("p", null, 
                                 n.description
                            )
                            
                        );
			});

			if(this.state.data.warnings.length == 0)
				PubSub.publish("widget.hideme", { widget_id: this.props.widget_id })
			
        }
        
        return (
            React.createElement("div", {className: "widget_content"}, 
                React.createElement("ul", null, 
                 nodes 
                ), 
                 this.state.data &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_folder},  this.state.data.url_label))
            	
            )
        );
    }
});

module.exports = WidgetContentWarnings;
},{}]},{},[1]);
