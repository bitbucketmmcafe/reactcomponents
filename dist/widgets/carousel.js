(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentCarousel.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentCarousel = widget;
},{"../../../widgets/WidgetContentCarousel.js":2}],2:[function(require,module,exports){
var WidgetContentCarousel = React.createClass({displayName: "WidgetContentCarousel",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    carrosselMount: function(dado){
 		dado.owlCarousel({
			autoPlay: 5500,
			stopOnHover : true,
			navigation : false,
			pagination: false,
			goToFirstSpeed : 2000,
			slideSpeed : 800,
			responsive : true,
			singleItem : true,
			autoHeight : false

		});

		var arrow = dado.siblings(".latest-interviews-carousel-arrows");
		$(arrow).find("a").on('click', function(event){
			dado.trigger('owl.' + event.currentTarget.dataset.direction);
		})
    },

    componentDidUpdate: function(){
    	  	
    	var owl = $(this.getDOMNode()).find(".widget-slider-container");
    	this.carrosselMount(owl)
    	
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
    	if(this.state.data && this.state.data.resources) {
	    	var images = this.state.data.resources.map(function(image, index){

				return 	React.createElement("div", {key:  "carrosselimage_" + image.id, className: "slide"}, 
							React.createElement("ul", {className: "list-unstyled"}, 
								React.createElement("li", {className: "interview-slide-wrap"}, 
								
									image.link.url && image.link.url.length > 0 &&
										React.createElement("a", {href: image.link.url, target: image.link.target}, 
											React.createElement("div", {className: "interviews-carousel-slide-title text-center"}, 
												React.createElement("span", {className: "interviews-slide-title"}, image.name), 
												!image.description &&
													React.createElement("h4", {className: "interviews-slide-sub-title"}, image.description)
												
											), 
											React.createElement("img", {src: image.url, className: "interviews-carousel-slide-image", alt: image.name})
										), 
									

									!image.link.url && 
										React.createElement("div", null, 
											React.createElement("div", {className: "interviews-carousel-slide-title text-center"}, 
												React.createElement("span", {className: "interviews-slide-title"}, image.name), 
												!image.description &&
													React.createElement("h4", {className: "interviews-slide-sub-title"}, image.description)
												
											), 
											React.createElement("img", {src: image.url, className: "interviews-carousel-slide-image", alt: image.name})
										)
										
								)
							)
						)

	    	});
		}
        return (
			React.createElement("div", {className: "widget-content widget-react-slider"}, 
				React.createElement("div", {className: "latest-interviews-carousel"}, 
					React.createElement("div", {className: "latest-interviews-carousel-arrows"}, 
						React.createElement("a", {className: "latest-interviews-carousel-prev", "data-direction": "prev"}, React.createElement("i", {className: "fa fa-angle-left"})), 
						React.createElement("a", {className: "latest-interviews-carousel-next", "data-direction": "next"}, React.createElement("i", {className: "fa fa-angle-right"}))
					), 

					React.createElement("div", {ref: "slideContainer", className: "widget-latest-interviews-carousel widget-slider-container"}, 

								images

					)
				)
			)
        );
    }

})

module.exports = WidgetContentCarousel;
},{}]},{},[1]);
