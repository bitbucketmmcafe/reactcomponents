(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentOurTeam.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentOurTeam = widget;
},{"../../../widgets/WidgetContentOurTeam.js":2}],2:[function(require,module,exports){
var WidgetContentOurTeam = React.createClass({displayName: "WidgetContentOurTeam",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        var data = {}, contratados = [], promovidos = [], desligados = [], hide_company = false;
        
        if(this.state.data && this.state.data.users) {

            hide_company = this.state.data.hide_company;

            contratados = this.state.data.users.contratados.map(function(user) {
                return(React.createElement("li", {key:  "userhighlight_" + user.id}, 
                                React.createElement("img", {src:  user.avatar}), 
                                React.createElement("ul", null, 
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.UserLink, {key:  "userlinkfor_" + user.id, user:  user })), 
                                 !hide_company && user.company  &&
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "translatecompanyfor_" + user.id, value:  "ourteam.company" }), ": ", React.createElement(MMCafeReact.Components.CompanyLabel, {key:  "companyfor_" + user.id, data:  user.company})), 
                                
                                 user.branch &&
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "translatebranchfor_" + user.id, value:  "ourteam.branch" }), ": ", React.createElement(MMCafeReact.Components.BranchLabel, {key:  "branchfor_" + user.id, data:  user.branch})), 
                                
                                 user.department &&
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "translatedepfor_" + user.id, value:  "ourteam.department" }), ": ", React.createElement(MMCafeReact.Components.DepartmentLabel, {key:  "departmentfor_" + user.id, data:  user.department}))
                                
                                )));
            });

            promovidos = this.state.data.users.promovidos.map(function(user) {
                return(React.createElement("li", {key:  "userhighlight_" + user.id}, 
                                React.createElement("img", {src:  user.avatar}), 
                                React.createElement("ul", null, 
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.UserLink, {key:  "userlinkfor_" + user.id, user:  user })), 
                                 !hide_company && user.company &&
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "translatecompanyfor_" + user.id, value:  "ourteam.company" }), ": ", React.createElement(MMCafeReact.Components.CompanyLabel, {key:  "companyfor_" + user.id, data:  user.company})), 
                                
                                 user.branch &&
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "translatebranchfor_" + user.id, value:  "ourteam.branch" }), ": ", React.createElement(MMCafeReact.Components.BranchLabel, {key:  "branchfor_" + user.id, data:  user.branch})), 
                                
                                 user.department &&
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "translatedepfor_" + user.id, value:  "ourteam.department" }), ": ", React.createElement(MMCafeReact.Components.DepartmentLabel, {key:  "departmentfor_" + user.id, data:  user.department}))
                                
                                )));
            });

            desligados = this.state.data.users.desligados.map(function(user) {
                return(React.createElement("li", {key:  "userhighlight_" + user.id}, 
                                React.createElement("ul", null, 
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.UserLink, {key:  "userlinkfor_" + user.id, user:  user })), 
                                 !hide_company && user.company &&
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "translatecompanyfor_" + user.id, value:  "ourteam.company" }), ": ", React.createElement(MMCafeReact.Components.CompanyLabel, {key:  "companyfor_" + user.id, data:  user.company})), 
                                
                                 user.branch &&
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "translatebranchfor_" + user.id, value:  "ourteam.branch" }), ": ", React.createElement(MMCafeReact.Components.BranchLabel, {key:  "branchfor_" + user.id, data:  user.branch})), 
                                
                                 user.department &&
                                React.createElement("li", null, React.createElement(MMCafeReact.Components.Translator, {key:  "translatedepfor_" + user.id, value:  "ourteam.department" }), ": ", React.createElement(MMCafeReact.Components.DepartmentLabel, {key:  "departmentfor_" + user.id, data:  user.department}))
                                
                                )));
            });

            
        }
        
        return (
            React.createElement("div", {className: "widget_content"}, 
            
                 contratados.length > 0 &&
                React.createElement("h3", null, React.createElement(MMCafeReact.Components.Translator, {value:  "ourteam.hired" })), 
                
                 contratados.length > 0 &&
                React.createElement("ul", null, 
                 contratados 
                ), 
                

                 promovidos.length > 0 &&
                React.createElement("h3", null, React.createElement(MMCafeReact.Components.Translator, {value:  "ourteam.promoted" })), 
                
                 promovidos.length > 0 &&
                React.createElement("ul", null, 
                 promovidos 
                ), 
                

                 desligados.length > 0 &&
                React.createElement("h3", null, React.createElement(MMCafeReact.Components.Translator, {value:  "ourteam.fired" })), 
                
                 desligados.length > 0 &&
                React.createElement("ul", null, 
                 desligados 
                )
                


            )
        );
    }
});

module.exports = WidgetContentOurTeam;

},{}]},{},[1]);
