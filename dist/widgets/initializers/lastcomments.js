var comments = [{ "id": 1, "content": "texto 1 sadsa saddsad", "parentcontent": { "id": 2, "url": "http://www.google.com", "name": "nome 1" }, "user": { "id": 3, "fullname": "Christian", "nickname": "Chris", "birthdate": "21/09", "avatar": "sadsa" } }, { "id": 2, "content": "texto 1 sadsa saddsad", "parentcontent": { "id": 2, "url": "http://www.google.com", "name": "nome 1" }, "user": { "id": 3, "fullname": "Christian", "nickname": "Chris", "birthdate": "10/09", "avatar": "sadsa" } }];

window.onload = function () {

    var expanded = true;
    var widget = React.render(React.createElement(MMCafeReact.WS.Widget, {
        type: "lastcomments",
        key: "100000",
        title: "Portlet últimos comentários - static",
        data: { comments: comments },
        widget_id: "100000" }), document.getElementById('container-widget-lastcomments'));

    var widgetdyn = React.render(React.createElement(MMCafeReact.WS.Widget, {
        type: "plain",
        key: "9",
        title: "Portlet últimos comentários - dinâmico",
        remote: true,
        source: "../../examples/fixtures/9.json",
        widget_id: "9" }), document.getElementById('container-widget-lastcomments-remote'));
};
