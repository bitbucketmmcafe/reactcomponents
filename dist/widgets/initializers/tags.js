var data = {
    "tags": [{ "id": 1, "name": "label 1", "url": "http://www.google.com", "weight": 10 }, { "id": 2, "name": "label 2", "url": "http://www.google.com", "weight": 6 }, { "id": 3, "name": "label 3", "url": "http://www.google.com", "weight": 5 }, { "id": 4, "name": "label 4", "url": "http://www.google.com", "weight": 8 }]
};

window.onload = function () {

    var expanded = true;
    var widget = React.render(React.createElement(MMCafeReact.WS.Widget, {
        type: "tags",
        key: "100000",
        title: "Portlet tags - static",
        data: data,
        widget_id: "100000" }), document.getElementById('container-widget-tags'));

    var widgetdyn = React.render(React.createElement(MMCafeReact.WS.Widget, {
        type: "tags",
        key: "7",
        title: "Portlet tags - dinâmico",
        remote: true,
        source: "../../examples/fixtures/7.json",
        widget_id: "5" }), document.getElementById('container-widget-tags-remote'));
};
