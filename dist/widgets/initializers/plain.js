window.onload = function () {
    var widget = React.render(React.createElement(MMCafeReact.WS.Widget, {
        key: "1",
        widget_id: "1" }), document.getElementById('container-widget-none'));

    var widgetremote = React.render(React.createElement(MMCafeReact.WS.Widget, {
        key: "4",
        widget_id: "4",
        remote: true,
        source: "../../examples/fixtures/4.json" }), document.getElementById('container-widget-none-remote'));
};
