window.onload = function () {
    var buttons = { minimize: true, tooltip: true };
    var widget = React.render(React.createElement(MMCafeReact.WS.Widget, {
        type: "weather",
        key: "3",
        widget_id: "3",
        buttons: buttons,
        title: "Custom Previsão Tempo" }), document.getElementById('container-widget-weather'));
};
