window.onload = function () {

    var workspace = React.render(React.createElement(MMCafeReact.WS.Workspace, {
        columns: workspacedata.columns,
        column_classname: workspacedata.column_classname,
        defaultworkspace: workspacedata.defaultworkspace,
        userworkspace: workspacedata.userworkspace,
        source: workspacedata.source }), document.getElementById('container'));
};
