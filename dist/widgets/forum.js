(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentForum.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentForum = widget;
},{"../../../widgets/WidgetContentForum.js":2}],2:[function(require,module,exports){

/**
 * WidgetContent para Fórum. Faz parte do pacote MMCafeReact.Widgets
 */
var WidgetContentForum = React.createClass({displayName: "WidgetContentForum",

    remoteData: function(data) {
        this.setState({ data: data });
        this.getFeed();
    
    },
    getFeed: function() {

        if(!this.state.data || !this.state.data.source)
            return false;

        
        var url = this.state.data.source.feeds,
        self = this;

        $.ajax({
            url: url
        })
        .then( function(response) {
            self.setState({ feeds: response, error: null });
        })
        .fail( function(err) {
            self.setState({ error: err });
        })
        
    },

    rendertext: function(entry) {
        if(entry.feed.feedable_type.toLowerCase() == "debate") {
            return moment(entry.feed.updated_at).format("DD/MM/YY") + " - Novo fórum <a href=\"" + entry.feeddata.url + "\" data-forum=\"true\">" + entry.feeddata.debate.name + "</a>";
        } else if(entry.feed.feedable_type.toLowerCase() == "topic") {
            return moment(entry.feed.updated_at).format("DD/MM/YY") + " - Novo tópico <a href=\"" + entry.feeddata.url + "\" data-forum-debate=\"" + entry.feeddata.on.url + "\" data-forum=\"true\">" + entry.feeddata.topic.subject + "</a>" + " no fórum <a href=\"" + entry.feeddata.on.url + "\" data-forum=\"true\">" + entry.feeddata.on.debate.name + "</a>";
        } else if(entry.feed.feedable_type.toLowerCase() == "comment") {
            var hash = entry.feeddata.url.split("/")[2];
            return moment(entry.feed.updated_at).format("DD/MM/YY") + " - <a href=\"" + entry.feeddata.url + "\" data-forum=\"true\" data-forum-debate=\"" + entry.feeddata.on.on.url + "\">Novo comentário" + "</a> no tópico <a href=\"" + entry.feeddata.on.url + "\" data-forum-debate=\"" + entry.feeddata.on.on.url + "\" data-forum=\"true\">" + entry.feeddata.on.topic.subject + "</a>";
        }
    },


    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        if(!this.props.isVisible())
            return false;

        if(this.state.error) {
            return (React.createElement("div", {className: "widget_content"}, 
                 React.createElement("div", {className: "widget-content-error"},  this.state.error.statusText), 
                 this.state.data && this.state.data.url_forum &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_forum},  this.state.data.url_label))
                
            ))
        }


        var self = this, list = [];
        if(this.state.feeds) {
			var nodes = this.state.feeds.map(function(n) {
			    return { text: self.rendertext(n) };
            });
			list = React.createElement(MMCafeReact.Components.List, {items:  nodes });
        }



        return (
            React.createElement("div", {className: "widget_content"}, 
                 list, 
                 this.state.data && this.state.data.url_forum &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_forum},  this.state.data.url_label))
            	
            )
        );
    }
});

module.exports = WidgetContentForum;

},{}]},{},[1]);
