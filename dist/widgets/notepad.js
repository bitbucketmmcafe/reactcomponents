(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentNotepad.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentNotepad = widget;

},{"../../../widgets/WidgetContentNotepad.js":2}],2:[function(require,module,exports){

/**
 * WidgetContent para Bloco de Notas. Faz parte do pacote MMCafeReact.Widgets
 */
var WidgetContentNotepad = React.createClass({displayName: "WidgetContentNotepad",

	timeout: null,

	handleKeyUp: function(event) {
        var textareaValue = React.findDOMNode(this.refs.textarea).value;

		this.setState({ counter : this.state.maxlength - event.target.value.length, content: textareaValue });

		var txt = event.target.value;
		this.timeout = clearTimeout(this.timeout);

		this.timeout = setTimeout((function() {
            var url = this.state.data.source.update + txt,
                query = url.split("user.edit.view?"),
                arr=query[1].split("&");
            result={};
            for(i=0;i<arr.length;i++) {
                k = arr[i].split('=');
                result[k[0]] = (k[1] || '');
            };
            $
			.ajax({ url: this.state.data.source.update.split("?")[0], data: result, method: "post",cache:false })
            .fail(function(jqXHR) {
                console.error("Error while trying to save notepad's content", jqXHR)
            });
		}).bind(this), 1000);

	},

    _focus: function(event) {
        $(event.target).closest('.widget').attr("draggable", false);
    },

    _blur: function(event) {
        $(event.target).closest('.widget').attr("draggable", true);
    },

    remoteData: function(data) {
        var l = data.content ? data.content.length : 0;

        this.setState({ data: data, counter : data.maxlength - l, maxlength: data.maxlength });
        this.forceUpdate();
    },
    
    getInitialState: function() {
        return {html: this.props.html, data: this.props.data, maxlength: 250 };
    },

    render: function() {

        return (
            React.createElement("div", {className: "widget_content"}, 
             this.state.data && 
                React.createElement("textarea", {ref: "textarea", onChange: this.handleKeyUp, maxLength:  this.state.maxlength, onFocus:  this._focus, onBlur:  this._blur, defaultValue:  this.state.data.content}), 
            
            React.createElement("span", null,  this.state.counter)
            )

        );

    }
});

module.exports = WidgetContentNotepad;

},{}]},{},[1]);
