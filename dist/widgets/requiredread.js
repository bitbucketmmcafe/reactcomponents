(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentRequiredRead.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentRequiredRead = widget;

},{"../../../widgets/WidgetContentRequiredRead.js":2}],2:[function(require,module,exports){
var WidgetContentRequiredRead = React.createClass({displayName: "WidgetContentRequiredRead",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
    	var urgentnodes = [], readnodes = [];
		if(this.state.data && this.state.data.urgent) {
			urgentnodes = this.state.data.urgent.map(function(n) {
				return React.createElement("li", {key:  "listitem_" + n.id}, 
					 moment(n.lasteditiondate).format("DD/MM/YY"), " - ", React.createElement(MMCafeReact.Components.Link, {data:  n })
				);
			});
		}

		if(this.state.data && this.state.data.read) {
			readnodes = this.state.data.read.map(function(n) {
				return React.createElement("li", {key:  "listitem_" + n.id}, 
					 moment(n.lasteditiondate).format("DD/MM/YY"), " - ", React.createElement(MMCafeReact.Components.Link, {data:  n })
				);
			});
		}

		return (
            React.createElement("div", {className: "widget_content"}, 
            	 urgentnodes.length > 0 &&
            	React.createElement("div", null, 
            	React.createElement("h3", {className: "urgent"}, React.createElement(MMCafeReact.Components.Translator, {key:  "urgent", value:  "requiredread.urgent" })), 
                React.createElement("ul", null, 
                 urgentnodes 
                )
                ), 
            	
            	 readnodes.length > 0 && 
            	React.createElement("div", null, 
            	React.createElement("h3", {className: "read"}, React.createElement(MMCafeReact.Components.Translator, {key:  "urgent", value:  "requiredread.read" })), 
                React.createElement("ul", null, 
                 readnodes 
                )
                ), 
            	
            	 (urgentnodes.length == 0 && readnodes.length == 0) &&
            	React.createElement("div", null, 
            		React.createElement("h4", null, React.createElement(MMCafeReact.Components.Translator, {key:  "congrats", value:  "requiredread.congrats" })), 
            		React.createElement("p", null, React.createElement(MMCafeReact.Components.Translator, {key:  "congrats", value:  "requiredread.congrats.desc" }))
            	), 
            	
				 this.state.data && this.state.data.url_folder && (urgentnodes.length > 0 || readnodes.length > 0) &&
				React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_folder},  this.state.data.url_label))
				
               
            )
        );
    }
});

module.exports = WidgetContentRequiredRead;
},{}]},{},[1]);
