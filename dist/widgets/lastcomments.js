(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentLastComments.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentLastComments = widget;




},{"../../../widgets/WidgetContentLastComments.js":2}],2:[function(require,module,exports){
var WidgetContentLastComments = React.createClass({displayName: "WidgetContentLastComments",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        
        if(this.state.data && this.state.data.comments) {
			var nodes = this.state.data.comments.map(function(n) {
                n.parentcontent.remote = true;
                return (React.createElement("article", {key:  "comment_" + n.id}, 
                            React.createElement("div", {dangerouslySetInnerHTML: { __html: n.content}}), 

                            React.createElement("footer", null, 
                                React.createElement("img", {src:  n.user.avatar}), 
                                React.createElement(MMCafeReact.Components.Link, {data:  n.parentcontent}), 
                                React.createElement(MMCafeReact.Components.UserLink, {user:  n.user}), 
                                React.createElement(MMCafeReact.Components.Likeable, {target: {id: n.id, likeId:null, likeable:false}, count: n.likeCount, type: "comment", viewOnly: true})
                            )
                        ));

			});
        }
        return (
            React.createElement("div", {className: "widget_content"}, 
                 nodes 
            )
        );
    }
});

module.exports = WidgetContentLastComments;

},{}]},{},[1]);
