(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentOutDoor.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentBirthdays = widget;
},{"../../../widgets/WidgetContentOutDoor.js":2}],2:[function(require,module,exports){
var WidgetContentOutDoor = React.createClass({displayName: "WidgetContentOutDoor",

    _formatDate: function(date) {

    },

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {data: this.props.data };
    },

    render: function() {
    	
        if(this.state.data && this.state.data.outdoor) {
			var nodes = this.state.data.outdoor.map(function(n) {
                return (React.createElement("div", {key:  "listitem_" + n.id, className: "w-container"}, 
  						React.createElement("a", {href: n.url}), 
            			n.firstresource && n.firstresource.type=="resource" &&
    						React.createElement("img", {src: n.thumb.length > 0 ? n.thumb : n.firstresource.url, className: "img-responsive"}), 
						
						React.createElement("div", {className: "selectorShadow"}), 
    					React.createElement("div", {className: "w-container-text"}, 
      						React.createElement("div", {className: "w-innerText"}, 
        						React.createElement("div", {className: "text"}, 
								n.semtitulo!=true &&
          						React.createElement("h3", null, 
        							n.name
      							), 
								
      							n.description && n.description!="" && n.semdescricao!=true &&
		          					React.createElement("p", null, n.description), 
								
								n.sembotao!=true &&
									React.createElement("p", {className: "w-link btn btn-primary"}, React.createElement(MMCafeReact.Components.Translator, {value:  "outdoor.learnmore.link" }))
								
        					)
      					)
    					)
    					));
                        
			});
			
        }
        return (
            React.createElement("div", {className: "widget_content"}, 
                
                 nodes 
                
            )
        );
    }
});

module.exports = WidgetContentOutDoor;

},{}]},{},[1]);
