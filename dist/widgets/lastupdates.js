(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentLastUpdates.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentLastUpdates = widget;
},{"../../../widgets/WidgetContentLastUpdates.js":2}],2:[function(require,module,exports){
var WidgetContentLastUpdates = React.createClass({displayName: "WidgetContentLastUpdates",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        
        if(this.state.data && this.state.data.lastupdates) {

            var hash = {
                1: {
                    "status": "accessed",
                    "label": MMCafeReact.Components.Translator.getI18n("statusaccess.accessed"),
                    "icon": "check-circle"
                },
                2: {
                    "status": "notaccessed",
                    "label": MMCafeReact.Components.Translator.getI18n("statusaccess.notaccessed"),
                    "icon": "info-circle"
                },
                3: {
                    "status": "urgent",
                    "label": MMCafeReact.Components.Translator.getI18n("statusaccess.urgent"),
                    "icon": "exclamation-triangle"
                }
            }

			var nodes = this.state.data.lastupdates.map(function(n) {
                
                var status = null;
                
                if(this.state.data.check_statusaccess) {
                    var statusaccess_item = hash[n.statusaccess];
                    var classname = "mm-statusaccess-label mm-statusaccess-" + statusaccess_item.status;
                    var classname_icon = "fa fa-" + statusaccess_item.icon; 
                    status = React.createElement("span", {className:  classname }, 
                        React.createElement("i", {className:  classname_icon, "aria-hidden": "true"}), 
                         statusaccess_item.label
                    )
                }

                n.remote = true;
				return React.createElement("li", {key:  "listitem_" + n.id}, 
                    React.createElement("div", null, 
                    React.createElement("a", {href:  n.url, dangerouslySetInnerHTML: { __html: n.name}}),  status 
                    )
                )
			}.bind(this));
        }


        return (
            React.createElement("div", {className: "widget_content"}, 
            	React.createElement("ol", null, 
            	 nodes 
            	), 
                 this.state.data && this.state.data.url_folder &&
                React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_folder},  this.state.data.url_label))
                
            )
        );
    }
});

module.exports = WidgetContentLastUpdates;
},{}]},{},[1]);
