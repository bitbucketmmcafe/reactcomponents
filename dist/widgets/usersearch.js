(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentUserSearch.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentUserSearch = widget;
},{"../../../widgets/WidgetContentUserSearch.js":2}],2:[function(require,module,exports){
var WidgetContentUserSearch = React.createClass({displayName: "WidgetContentUserSearch",
    
    remoteData: function(data) {
        this.setState({ data: data });
    },
    
    sourceParams: function() {
        if(this.state.data && this.state.data.userCompanySearch == true){
            var pars = {branchId : null, companyId: this.state.data.company, departmentId: null, size: 3}
            return pars;
        }
        if(this.refs.cbd.sourceParams() && this.state.data && this.state.data.userCompanySearch == false){

            var pars = this.refs.cbd.sourceParams();
            pars["size"] = 3;
            return pars;
        }
    },

    
    getInitialState: function() {
        return {html: this.props.html, data: this.props.data, fieldsSearch: "fullName,nickname,name,extension", typesearch: "name", required: true, isChecked: false};
    },
    
    componentDidMount: function() {
        var self = this;
        PubSub.subscribe( 'component.autocompleteuser.selected', function(msg, data) {
            var href = self.state.data.source.userprofile + data.user.id;
            document.location = href;
        });
        
        $(this.getDOMNode()).on("change", "select[name='companyId']", function() {
            if($(this).val()!="")
            self.refs.autocomplete.setState({ required: false })
            else
            self.refs.autocomplete.setState({ required: "required" })
        })
    },
    
    _typeSearchName: function() {
        this.setState({ typesearch: "name", fieldsSearch: "fullName,nickname,name,extension", isChecked: false });
    },
    
    _typeSearchPerfil: function() {
        this.setState({ typesearch: "profile", fieldsSearch: "fullName,nickname,cvContent,homePhone,mobilePhone,nationality,observation,myInterests", isChecked: true});
    },
 
    handleCheck: function() {
        this.setState({
            isChecked: !this.state.isChecked // flip boolean value
        }, function() {
            if(this.state.isChecked)
                this._typeSearchPerfil(); 

        }.bind(this));
        
    },        
 
    
    render: function() {
        return (
            React.createElement("div", {className: "widget_content"}, 
            React.createElement("form", {action:  this.state.data && !this.state.isChecked? this.state.data.action: "/user.search.view?method=searchUserByPrefix", className: "mm-defaultform-submit", ref: "form", method: "post", autoComplete: "off"}, 
            React.createElement("input", {type: "hidden", name: "search", value: "true"}), 
             this.state.data && this.state.data.userCompanySearch==true && this.state.data.company &&
            React.createElement("input", {type: "hidden", name: "companyId", value: this.state.data.company}), 
            
            
             this.state.data && this.state.data.contextualSearch==false &&
                React.createElement("input", {type: "hidden", name: "searchFieldsUser", value:  this.state.fieldsSearch}), 
            
             this.state.data && this.state.data.contextualSearch==true && this.state.isChecked && this.state.data.checkProfile==true &&
                React.createElement("input", {type: "hidden", name: "searchFieldsUser", value:  this.state.fieldsSearch}), 
            
            
             this.state.data && this.state.data.source && !this.state.data.disable_cbd && this.state.data.source.business_fields==true && this.state.data.userCompanySearch==false &&
                React.createElement(MMCafeReact.Components.CBD, {ref:  "cbd", data:  this.state.data}), 
            
            
             this.state.data && this.state.data.canSeeCV && this.state.data.contextualSearch==false &&
                
                React.createElement("fieldset", {className: "select-type"}, 
                React.createElement("input", {type: "radio", name: "searchTalents", checked:  this.state.typesearch == "name", onChange:  this._typeSearchName}), React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value:  "usersearch.name" })), 
                React.createElement("input", {type: "radio", name: "searchTalents", checked:  this.state.typesearch == "profile", onChange:  this._typeSearchPerfil}), React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value:  "usersearch.profile" }))
                ), 
            
            
             this.state.data && this.state.data.contextualSearch==false && this.state.data.userCompanySearch==false &&
                React.createElement("fieldset", null, 
                React.createElement("label", {className: "label-name"}, React.createElement(MMCafeReact.Components.Translator, {value:  "usersearch.name" })), 
                React.createElement(MMCafeReact.Components.AutocompleteUser, {ref: "autocomplete", required: "required", maxlength:  this.state.data.maxlength, disableautocomplete:  this.state.data.disableautocomplete, source:  this.state.data && !this.state.isChecked ? this.state.data.source.usersearch: "/ajax/users/user.view?method=getUsersStartedWithSuggest&", sourceParams:  this.sourceParams, 
                sourceParams2: this.state.fieldsSearch})
                ), 
            

             this.state.data && this.state.data.contextualSearch==false && this.state.data.userCompanySearch==true &&
                React.createElement("fieldset", null, 
                React.createElement("label", {className: "label-name"}, React.createElement(MMCafeReact.Components.Translator, {value:  "usersearch.name" })), 
                React.createElement(MMCafeReact.Components.AutocompleteUser, {ref: "autocomplete", required: "required", maxlength:  this.state.data.maxlength, disableautocomplete:  this.state.data.disableautocomplete, source:  this.state.data && !this.state.isChecked ? this.state.data.source.usersearch: "/ajax/users/user.view?method=getUsersStartedWithSuggest&", sourceParams: this.sourceParams, 
                sourceParams2: this.state.fieldsSearch})
                ), 
            
            
             this.state.data && this.state.data.contextualSearch==true && 
                React.createElement("fieldset", null, 
                React.createElement("label", {className: "label-name"}, React.createElement(MMCafeReact.Components.Translator, {value:  this.state.data.contextualLabel})), 
                React.createElement(MMCafeReact.Components.AutocompleteUser, {ref: "autocomplete", required: "required", disableautocomplete:  this.state.data.disableautocomplete, source:  this.state.data &&  !this.state.isChecked ? this.state.data.source.usersearch: "/ajax/users/user.view?method=getUsersStartedWithSuggest&", sourceParams:  this.state.data.source.business_fields==true? this.sourceParams:"", 
                sourceParams2:  this.state.isChecked ? this.state.fieldsSearch:"", business_fields: this.state.data.source.business_fields, contextualSearch: true})
                ), 
            
            
             this.state.data && this.state.data.canSeeCV && this.state.data.contextualSearch==true && this.state.data.checkProfile==true &&
                React.createElement("fieldset", {className: "select-type"}, 
                React.createElement("input", {type: "checkbox", name: "searchTalents", checked: this.state.isChecked, onChange: this.handleCheck}), React.createElement("label", null, React.createElement(MMCafeReact.Components.Translator, {value:  "usersearch.profile" }))
                ), 
            
            
            React.createElement("button", {type: "submit", className:  "btn btn-primary btn-mm-defaultform-submit" }, 
            React.createElement(MMCafeReact.Components.Translator, {value:  "usersearch.submit" })
            )
            )
            
            )
        );
    }
});

module.exports = WidgetContentUserSearch;

},{}]},{},[1]);
