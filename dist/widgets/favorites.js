(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentFavorites.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentFavorites = widget;
},{"../../../widgets/WidgetContentFavorites.js":2}],2:[function(require,module,exports){
var WidgetContentFavorites = React.createClass({displayName: "WidgetContentFavorites",

    _build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },

    _deleteFav: function(id, e) {

        var confirm = new window.ExpressoModules.ExpressoConfirmModal({
            messages: {
                title: MMCafeReact.Components.Translator.getI18n("favorites.delete.prompt.title"),
                desc: "",
                proceed: MMCafeReact.Components.Translator.getI18n("favorites.delete.prompt.confirm"),
                cancel: MMCafeReact.Components.Translator.getI18n("favorites.delete.prompt.cancel")
            },
            onProceed: function(event) {
                var url = this.state.data.source.removefav + id;
                PubSub.publish("components.loader.add", { key: "po.favorite.remove" } );
                var builded_data = this._build_data(url)
                $
                .ajax({url: builded_data.url, data: builded_data.data, method: 'post' })
                .then(function() {
                    var array = this.state.data.favorites.filter(function(fav) {
                        if(fav.id != id)
                            return fav;
                    });
                    var data = this.state.data;
                    data.favorites = array;
                    this.setState({ data: data });

                }.bind(this))
                .done(function() {
                    PubSub.publish("components.loader.remove", { key: "po.favorite.remove", type: "success" } );
                }).
                fail(function(jqXHR) {
                    PubSub.publish("components.loader.remove", { key: "po.favorite.remove", type: "error", error: jqXHR } );
                })
            }.bind(this)
        });
        confirm.open();

    },

    dragStart: function(e) {
        this.dragged = e.currentTarget;
        e.dataTransfer.effectAllowed = 'move';
        this.ul = $(e.target).closest("ul")[0];
        this.ul.style.height = this.ul.clientHeight + "px";
        e.dataTransfer.setData("text/html", e.currentTarget);
    },

    dragEnd: function(e) {

        this.ul.style.height = "auto";

        this.dragged.style.display = "block";
        $(".favorite_placeholder").remove();

        var from = Number(this.dragged.dataset.index),
            to = Number(this.over.dataset.index);

        var newOrder = this.state.data.favorites.map( function(f, counter) {
            return counter;
        })
        newOrder.splice(to, 0, newOrder.splice(from, 1)[0]);

        var builded_data = this._build_data("/ajax/favorite/order/user.view?method=changeOrderFavoritePO&neworderId=" + newOrder)
       
        $
        .ajax({url: builded_data.url, data: builded_data.data, method: 'post' })
        .then( function(response) {
            
            var data = this.state.data;
            data.favorites = response.data.favorites;

            this.setState({ data: data});
        }.bind(this));
        
    },
    dragOver: function(e) {
        e.preventDefault();
        this.dragged.style.display = "none";
        if(e.target.className == "favorite_placeholder") {
            //return;
        }
        $(".favorite_placeholder").remove();
        this.over = e.target;
        this.placeholder = document.createElement("li");
        this.placeholder.className = "favorite_placeholder";
        var t = $(e.target).closest("li");
        if(e.target && e.target.parentNode && e.target.tagName == "UL") {
            try {
                this.ul.insertBefore(this.placeholder, t[0]);
            } catch(e) {
                console.error(e)
            }
        }
    },

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        if(this.state.data && this.state.data.favorites) {

            var list = this.state.data.favorites.map(function(n, index) {
                n.remote = true;
                return React.createElement("li", {
                        draggable: "true", 
                        onDragEnd: this.dragEnd, 
                        onDragStart: this.dragStart, 
                        "data-index": index, 
                        key:  "listitem_" + n.id}, 
                             n.thumb && 
                            React.createElement("img", {src:  n.thumb, title:  n.name}), 
                            
                            React.createElement("h3", null, 
                                React.createElement(MMCafeReact.Components.Link, {data:  n })
                            ), 
                            React.createElement("p", null, 
                                 n.description
                            ), 
                            React.createElement("button", {onClick: this._deleteFav.bind(null,n.id)})
                        );
            }.bind(this));
        }

        return (
            React.createElement("div", {className: "widget_content"}, 

                this.state.data && this.state.data.favorites && this.state.data.favorites.length > 0 && 
                    React.createElement("ul", {className:  this.props.classname, onDragOver: this.dragOver}, 
                         list 
                    ), 
                

                this.state.data && this.state.data.favorites && this.state.data.favorites.length <= 0 &&
                    React.createElement("div", {className: "no_favs"}, 
                        React.createElement("div", {className: "iconexpresso-favorito-vazio"}), 
                        React.createElement("p", null, 

                            React.createElement(MMCafeReact.Components.Translator, {value:  "favorites.nofavs.firsttext" }), 
                            React.createElement("span", {className: "star"}), " ", React.createElement(MMCafeReact.Components.Translator, {value:  "favorites.nofavs.finaltext" })
                        )
                    )
                
            )
        );
    }
});

module.exports = WidgetContentFavorites;

},{}]},{},[1]);
