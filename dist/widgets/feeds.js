(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentFeeds.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentFeeds = widget;
},{"../../../widgets/WidgetContentFeeds.js":2}],2:[function(require,module,exports){
var FeedsList = React.createClass({displayName: "FeedsList",
  render: function() {
    
    if(this.props.feeds) {
        var feednodes = this.props.feeds.map(function(feed, index) {
            return React.createElement("li", {key:  "feed_" + index}, 
                        React.createElement("a", {href: feed.url, title: feed.title, target: "_blank"},  feed.title)
                    );
            
        }.bind(this));
    }
    return (
		React.createElement("ul", {className: "mm-feedlist"}, 
			 feednodes 
		)
    );
  }
});

var WidgetContentFeeds = React.createClass({displayName: "WidgetContentFeeds",

    first_load: true,

    remoteData: function(data) {
        this.setState({ data: data });
        this.consumeFeed(this.state.data.source.get_feeds+data.feeds[0].url, 0);
    },

    handleClick: function(e){
    	e.preventDefault();
        PubSub.publish("components.loader.add", { key: "feed.consume" } );
		this.consumeFeed(this.state.data.source.get_feeds+e.currentTarget.href, $(e.currentTarget.parentNode).index());
    },

    consumeFeed: function(url, index) {
        $.ajax({
          url: url,
          method:"POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            this.setState({ feedItems: data, selectedIndex: index });
            if(!this.first_load) {
                PubSub.publish("components.loader.remove", { key: "feed.consume", type: "success" } );
            } else {
                this.first_load = false;
            }
          }.bind(this),
          error: function(xhr, status, err) {
            PubSub.publish("components.loader.remove", { key: "feed.consume", type: "error", error: jqXHR } );
          }.bind(this)
        });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        if(this.state.data && this.state.data.feeds) {
			var nodes = this.state.data.feeds.map(function(n, index) {
                return React.createElement("li", {key:  "listitem_" + index}, 
                			React.createElement("a", {href: n.url, trigger: "click", onClick: this.handleClick, className:  this.state.selectedIndex == index ? "selected" : ""}, n.name)
                        );
			}.bind(this));
        }

        return (
            React.createElement("div", {className: "widget_content"}, 
                React.createElement("ul", {className: "listFeeds"}, 
                 nodes 
                ), 
                 this.state.feedItems &&
                React.createElement(FeedsList, {feeds:  this.state.feedItems})
                
            )
        );
        
    }

});

module.exports = WidgetContentFeeds;

},{}]},{},[1]);
