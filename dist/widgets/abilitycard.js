(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentAbilityCards.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentAbilityCards = widget;

},{"../../../widgets/WidgetContentAbilityCards.js":2}],2:[function(require,module,exports){
var WidgetContentAbilityCards = React.createClass({displayName: "WidgetContentAbilityCards",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        var nodes = null;
        if(this.state.data && this.state.data.cards) {
            nodes = this.state.data.cards.map( function(card) {
                return React.createElement("div", {key:  "carditem_" + card.id}, 
                    React.createElement("img", {src:  card.image}), 
                    React.createElement("h3", null,  card.title), 

                    React.createElement("ul", null, 
                        React.createElement("li", null, 
                             React.createElement(MMCafeReact.Components.Translator, {value:  "cards.to" }), ": ", React.createElement(MMCafeReact.Components.UserLink, {key:  "userlinkfor_" + card.to.id, user:  card.to})
                        ), 
                        React.createElement("li", null, 
                           React.createElement(MMCafeReact.Components.Translator, {value:  "cards.from" }), ": ", React.createElement(MMCafeReact.Components.UserLink, {key:  "userlinkfor_" + card.from.id, user:  card.from})
                        ), 
                        React.createElement("li", null, 
                            React.createElement(MMCafeReact.Components.Translator, {value:  "cards.sent_at" }), " ",  card.sent_at
                        )
                    ), 

                    React.createElement("p", null,  card.message)
                )
            })
        }
        return (
            React.createElement("div", {className: "widget_content"}, 
             nodes, 
             this.state.data && this.state.data.url_folder &&
            React.createElement("footer", null, React.createElement("a", {href:  this.state.data.url_folder},  this.state.data.url_label))
            
            )
        );
    }
});

module.exports = WidgetContentAbilityCards;

},{}]},{},[1]);
