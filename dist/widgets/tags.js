(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var widget = require("../../../widgets/WidgetContentTags.js");
window.MMCafeReact.Widgets = window.MMCafeReact.Widgets || {};
MMCafeReact.Widgets.WidgetContentTags = widget;

},{"../../../widgets/WidgetContentTags.js":2}],2:[function(require,module,exports){
var WidgetContentTags = React.createClass({displayName: "WidgetContentTags",

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        
        if(this.state.data && this.state.data.tags) {
			var nodes = this.state.data.tags.map(function(tag) {
				return React.createElement("a", {href:  tag.url, key:  "tag_"+ tag.id, title:  tag.name, "data-remote": "true", className:  "size_" + tag.weight}, "#",  tag.name)
			});
        }

        return (
            React.createElement("div", {className: "widget_content"}, 
                 nodes 
            )
        );
    }
});

module.exports = WidgetContentTags;
},{}]},{},[1]);
