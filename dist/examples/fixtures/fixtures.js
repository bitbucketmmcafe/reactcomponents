var user_widgets_id = {
	11: [1, 5, 6],
    12: [2, 4],
    13: [3]
};

var dragSrcEl = null;

var workspace_cols = [11, 12, 13];

var workspace_cols_new = [13, 12, 11];

var workspacedata = {
	columns: [24, 25, 26, 27],
	column_classname: "col-xs-3",
	defaultworkspace: {
						24:[1,2,3],
						25:[4,5],
						26:[6],
						27:[],
						28:[]
					  },
	source: {
		widgetsource: "fixtures/#{id}.json",
    	updateposition: "fixtures/updateworkspace.json"	
	}
    
}



var fixtures = {
	plain: {
		title: "Widget padrão",
		html: "algum texto?"
	},
	weather : {
		title: "Previsão do tempo",
		html: "algum texto pro widget weather"
	},
	calendar : {
		title: "Calendário",
		html: "algum texto pro widget de calendar"
	},
	forum : {
		title: "Fórum",
		html: "algum texto pro widget de fórum"
	},
	tags : {
		title: "Tags",
		html: "algum texto pro widget de tags"
	}

};

var expandable_columns_data = [
	{ widget_id: 1, type: "plain", title: "Título 1", html: "algum html!" },
	{ widget_id: 8, type: "notepad", title: "Notepad", html: "algum html!" },
	{	widget_id: 2, 
		type: "calendar",
		title: "Calendário12!",
		html: "algum html pro calendário?!!",
		data: {
			source: {
				dayswithevents: "../../examples/fixtures/dayswithevents.json?query=",
				eventsbyday:"../../examples/fixtures/eventsbyday.json?query="
			}
		}
	},
	{ widget_id: 3, type: "weather", title: "Previsão do tempo!"},
	{	widget_id: 6, 
		type: "forum",
		title: "Fórum",
		html: "algum html pro fórum",
		data: {
			source: {
				feeds: "http://localhost:3009/feeds.json?size=5"
			}
		}
	}

];

var workspace_widgets_details = {
	1: { widget_id: 1, type: "plain"},
	2: { widget_id: 2, type: "calendar"},
	3: { widget_id: 3, type: "weather"},
	4: { widget_id: 4, type: "plain"},
	5: { widget_id: 5, type: "news"},
	6: { widget_id: 6, type: "forum"},
	7: { widget_id: 7, type: "tags"}
}
