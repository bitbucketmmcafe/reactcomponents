
/**
 * WidgetContent para Caronas. Faz parte do pacote MMCafeReact.Widgets
 */
var WidgetContentOfferrides = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
        this.getRides();
    
    },
    getRides: function() {

        if(!this.state.data || !this.state.data.source)
            return false;


        var self = this;
        $.ajax({
            url: self.state.data.source.rides
        })
        .then(function(response) {
            self.setState({ rides: response, error: null });
        })
        .fail(function(err) {
            self.setState({ error: err });
        })
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        if(!this.props.isVisible())
            return false;

        if(this.state.error) {
            return (<div className="widget_content">
                 <div className="widget-content-error">{ this.state.error.statusText }</div>
                { this.state.data && this.state.data.url_forum &&
                <footer><a href={ this.state.data.url_forum}>{ this.state.data.url_label }</a></footer>
                }
            </div>)
        }


        var self = this, list = [];
        if(this.state.rides) {
			var nodes = this.state.rides.map(function(r) {
			    return <li>
                    <img src={ r.absolutethumb } />
                    <a href={ r.absoluteurl } target="_blank">
                    De { r.start_address } a { r.end_address }
                    </a>
                </li>
            });
			list = nodes;
        }

        return (
            <div className="widget_content">
                <ul>
                { list }
                </ul>
                { this.state.data && this.state.data.url_modulo &&
                <footer><a href={ this.state.data.url_modulo}>{ this.state.data.url_label }</a></footer>
            	}
            </div>
        );
    }
});

module.exports = WidgetContentOfferrides;
