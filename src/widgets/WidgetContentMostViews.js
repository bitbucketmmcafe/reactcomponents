var WidgetContentMostViews = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        
        if(this.state.data && this.state.data.entries) {

			var nodes = this.state.data.entries.map(function(n) {
                
               var resource_image =  n.firstresource && n.firstresource.content_type.indexOf("image")>=0? n.firstresource : "";
                
				return { text: n.name, link: n.url, id: n.id, external: n.external, description: n.description, thumb: n.thumb, image: resource_image };
			});
			var list = <MMCafeReact.Components.List items={ nodes } />;
        }

        return (
            <div className="widget_content">
                { list }
            </div>
        );
    }
});

module.exports = WidgetContentMostViews;