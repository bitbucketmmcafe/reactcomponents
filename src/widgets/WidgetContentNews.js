var Link = require("../components/Link");
var UserLink = require("../components/UserLink");


var WidgetContentNews = React.createClass({

    
    _formatDate: function(date) {

    },

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        var SPACE = ' ';
        if(this.state.data && this.state.data.news) {
            var po_publishinfo_type = this.state.data.po_publishinfo_type;
			var nodes = this.state.data.news.map(function(n) {

                var status = null;
                
                if(this.state.data.check_statusaccess) {

                    var hash = {
                            1: {
                                "status": "accessed",
                                "label": MMCafeReact.Components.Translator.getI18n("statusaccess.accessed"),
                                "icon": "check-circle"
                            },
                            2: {
                                "status": "notaccessed",
                                "label": MMCafeReact.Components.Translator.getI18n("statusaccess.notaccessed"),
                                "icon": "info-circle"
                            },
                            3: {
                                "status": "urgent",
                                "label": MMCafeReact.Components.Translator.getI18n("statusaccess.urgent"),
                                "icon": "exclamation-triangle"
                            }
                    }

                    var statusaccess_item = hash[n.statusaccess];
                    var classname = "mm-statusaccess-label mm-statusaccess-" + statusaccess_item.status;
                    var classname_icon = "fa fa-" + statusaccess_item.icon; 
                    status = <span className={ classname }>{ SPACE } 
                        <i className={ classname_icon } aria-hidden="true"></i>
                        { statusaccess_item.label }
                    </span>
                }
                n.remote = true;

                return <li key={ "listitem_" + n.id }>

                            { n.thumb && 
                            <img src={ n.thumb } title={ n.name } />
                            }
                            <h3>
                                <Link data={ n } />
                                { status }
                            </h3>
                            { po_publishinfo_type!="nothing" && po_publishinfo_type=="all" &&
                            <span>
                                <MMCafeReact.Components.Translator key={ "translatefor_" + n.id } value={ "content.updated.from" } />
                                { SPACE }
                                <UserLink user={ n.lasteditedby } /> 
                                { SPACE }
                                <MMCafeReact.Components.Translator key={ "translateat_" + n.id } value={ "content.updated.at" } />{ SPACE }
                                <MMCafeReact.Components.DateLabel iso8061date={moment.utc().format(n.lasteditiondate)} pattern={ n.lasteditiondate.date_pattern || "DD/MM/YY - HH:mm" } />
                            </span>
                            }
                            { po_publishinfo_type!="nothing" && po_publishinfo_type!="all" &&
                                <span>
                                    <MMCafeReact.Components.Translator key={ "translatefor_" + n.id } value={ "po.published" } />
                                    { SPACE }
                                    <MMCafeReact.Components.Translator key={ "translateat_" + n.id } value={ "content.updated.at" } />{ SPACE }
                                    <MMCafeReact.Components.DateLabel iso8061date={moment.utc().format(n.lasteditiondate)} pattern={ n.lasteditiondate.date_pattern || "DD/MM/YY - HH:mm" } />
                                </span>
                            }
                            <p>
                                { n.description }
                            </p>
                        </li>;
			}.bind(this));
			
        }

        return (
            <div className="widget_content">
                <ul>
                { nodes }
                </ul>
                { this.state.data && this.state.data.url_folder &&
                <footer><a href={ this.state.data.url_folder}>{ this.state.data.url_label }</a></footer>
            	}
            </div>
        );
    }
});

module.exports = WidgetContentNews;