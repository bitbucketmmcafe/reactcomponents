var WidgetContentLastComments = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        
        if(this.state.data && this.state.data.comments) {
			var nodes = this.state.data.comments.map(function(n) {
                n.parentcontent.remote = true;
                return (<article key= { "comment_" + n.id }>
                            <div dangerouslySetInnerHTML={{ __html: n.content  }}></div>

                            <footer>
                                <img src={ n.user.avatar } />
                                <MMCafeReact.Components.Link data={ n.parentcontent } />
                                <MMCafeReact.Components.UserLink user={ n.user } />
                                <MMCafeReact.Components.Likeable target={{id: n.id, likeId:null, likeable:false}} count={n.likeCount} type={"comment"} viewOnly={true} />
                            </footer>
                        </article>);

			});
        }
        return (
            <div className="widget_content">
                { nodes }
            </div>
        );
    }
});

module.exports = WidgetContentLastComments;
