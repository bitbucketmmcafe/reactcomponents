
/**
 * WidgetContent para Fórum. Faz parte do pacote MMCafeReact.Widgets
 */
var WidgetContentForum = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
        this.getFeed();
    
    },
    getFeed: function() {

        if(!this.state.data || !this.state.data.source)
            return false;

        
        var url = this.state.data.source.feeds,
        self = this;

        $.ajax({
            url: url
        })
        .then( function(response) {
            self.setState({ feeds: response, error: null });
        })
        .fail( function(err) {
            self.setState({ error: err });
        })
        
    },

    rendertext: function(entry) {
        if(entry.feed.feedable_type.toLowerCase() == "debate") {
            return moment(entry.feed.updated_at).format("DD/MM/YY") + " - Novo fórum <a href=\"" + entry.feeddata.url + "\" data-forum=\"true\">" + entry.feeddata.debate.name + "</a>";
        } else if(entry.feed.feedable_type.toLowerCase() == "topic") {
            return moment(entry.feed.updated_at).format("DD/MM/YY") + " - Novo tópico <a href=\"" + entry.feeddata.url + "\" data-forum-debate=\"" + entry.feeddata.on.url + "\" data-forum=\"true\">" + entry.feeddata.topic.subject + "</a>" + " no fórum <a href=\"" + entry.feeddata.on.url + "\" data-forum=\"true\">" + entry.feeddata.on.debate.name + "</a>";
        } else if(entry.feed.feedable_type.toLowerCase() == "comment") {
            var hash = entry.feeddata.url.split("/")[2];
            return moment(entry.feed.updated_at).format("DD/MM/YY") + " - <a href=\"" + entry.feeddata.url + "\" data-forum=\"true\" data-forum-debate=\"" + entry.feeddata.on.on.url + "\">Novo comentário" + "</a> no tópico <a href=\"" + entry.feeddata.on.url + "\" data-forum-debate=\"" + entry.feeddata.on.on.url + "\" data-forum=\"true\">" + entry.feeddata.on.topic.subject + "</a>";
        }
    },


    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        if(!this.props.isVisible())
            return false;

        if(this.state.error) {
            return (<div className="widget_content">
                 <div className="widget-content-error">{ this.state.error.statusText }</div>
                { this.state.data && this.state.data.url_forum &&
                <footer><a href={ this.state.data.url_forum}>{ this.state.data.url_label }</a></footer>
                }
            </div>)
        }


        var self = this, list = [];
        if(this.state.feeds) {
			var nodes = this.state.feeds.map(function(n) {
			    return { text: self.rendertext(n) };
            });
			list = <MMCafeReact.Components.List items={ nodes } />;
        }



        return (
            <div className="widget_content">
                { list }
                { this.state.data && this.state.data.url_forum &&
                <footer><a href={ this.state.data.url_forum}>{ this.state.data.url_label }</a></footer>
            	}
            </div>
        );
    }
});

module.exports = WidgetContentForum;
