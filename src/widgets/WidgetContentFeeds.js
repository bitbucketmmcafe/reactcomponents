var FeedsList = React.createClass({
  render: function() {
    
    if(this.props.feeds) {
        var feednodes = this.props.feeds.map(function(feed, index) {
            return <li key={ "feed_" + index }>
                        <a href={feed.url} title={feed.title} target={"_blank"}>{ feed.title }</a>
                    </li>;
            
        }.bind(this));
    }
    return (
		<ul className="mm-feedlist">
			{ feednodes }
		</ul>
    );
  }
});

var WidgetContentFeeds = React.createClass({

    first_load: true,

    remoteData: function(data) {
        this.setState({ data: data });
        this.consumeFeed(this.state.data.source.get_feeds+data.feeds[0].url, 0);
    },

    handleClick: function(e){
    	e.preventDefault();
        PubSub.publish("components.loader.add", { key: "feed.consume" } );
		this.consumeFeed(this.state.data.source.get_feeds+e.currentTarget.href, $(e.currentTarget.parentNode).index());
    },

    consumeFeed: function(url, index) {
        $.ajax({
          url: url,
          method:"POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            this.setState({ feedItems: data, selectedIndex: index });
            if(!this.first_load) {
                PubSub.publish("components.loader.remove", { key: "feed.consume", type: "success" } );
            } else {
                this.first_load = false;
            }
          }.bind(this),
          error: function(xhr, status, err) {
            PubSub.publish("components.loader.remove", { key: "feed.consume", type: "error", error: jqXHR } );
          }.bind(this)
        });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        if(this.state.data && this.state.data.feeds) {
			var nodes = this.state.data.feeds.map(function(n, index) {
                return <li key={ "listitem_" + index } >
                			<a href={n.url} trigger={"click"} onClick={this.handleClick} className={ this.state.selectedIndex == index ? "selected" : "" }>{n.name}</a>
                        </li>;
			}.bind(this));
        }

        return (
            <div className="widget_content">
                <ul className="listFeeds">
                { nodes }
                </ul>
                { this.state.feedItems &&
                <FeedsList feeds={ this.state.feedItems } />
                }
            </div>
        );
        
    }

});

module.exports = WidgetContentFeeds;
