var WidgetContentRequiredRead = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
    	var urgentnodes = [], readnodes = [];
		if(this.state.data && this.state.data.urgent) {
			urgentnodes = this.state.data.urgent.map(function(n) {
				return <li key={ "listitem_" + n.id }>
					{ moment(n.lasteditiondate).format("DD/MM/YY") } - <MMCafeReact.Components.Link data={ n } />
				</li>;
			});
		}

		if(this.state.data && this.state.data.read) {
			readnodes = this.state.data.read.map(function(n) {
				return <li key={ "listitem_" + n.id }>
					{ moment(n.lasteditiondate).format("DD/MM/YY") } - <MMCafeReact.Components.Link data={ n } />
				</li>;
			});
		}

		return (
            <div className="widget_content">
            	{ urgentnodes.length > 0 &&
            	<div>
            	<h3 className="urgent"><MMCafeReact.Components.Translator key={ "urgent" } value={ "requiredread.urgent" } /></h3>
                <ul>
                { urgentnodes }
                </ul>
                </div>
            	}
            	{ readnodes.length > 0 && 
            	<div>
            	<h3 className="read"><MMCafeReact.Components.Translator key={ "urgent" } value={ "requiredread.read" } /></h3>
                <ul>
                { readnodes }
                </ul>
                </div>
            	}
            	{ (urgentnodes.length == 0 && readnodes.length == 0) &&
            	<div>
            		<h4><MMCafeReact.Components.Translator key={ "congrats" } value={ "requiredread.congrats" } /></h4>
            		<p><MMCafeReact.Components.Translator key={ "congrats" } value={ "requiredread.congrats.desc" } /></p>
            	</div>
            	}
				{ this.state.data && this.state.data.url_folder && (urgentnodes.length > 0 || readnodes.length > 0) &&
				<footer><a href={ this.state.data.url_folder}>{ this.state.data.url_label }</a></footer>
				}
               
            </div>
        );
    }
});

module.exports = WidgetContentRequiredRead;