var WidgetContentOutDoor = React.createClass({

    _formatDate: function(date) {

    },

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {data: this.props.data };
    },

    render: function() {
    	
        if(this.state.data && this.state.data.outdoor) {
			var nodes = this.state.data.outdoor.map(function(n) {
                return (<div key={ "listitem_" + n.id } className="w-container">
  						<a href={n.url}></a>
            			{n.firstresource && n.firstresource.type=="resource" &&
    						<img src={n.thumb.length > 0 ? n.thumb : n.firstresource.url} className="img-responsive" />
						}
						<div className="selectorShadow"></div>
    					<div className="w-container-text">
      						<div className="w-innerText">
        						<div className="text">
								{n.semtitulo!=true &&
          						<h3>
        							{n.name}
      							</h3>
								}
      							{n.description && n.description!="" && n.semdescricao!=true &&
		          					<p>{n.description}</p>
								}
								{n.sembotao!=true &&
									<p className="w-link btn btn-primary"><MMCafeReact.Components.Translator value={ "outdoor.learnmore.link" } /></p>
								}
        					</div>
      					</div>
    					</div>
    					</div>);
                        
			});
			
        }
        return (
            <div className="widget_content">
                
                { nodes }
                
            </div>
        );
    }
});

module.exports = WidgetContentOutDoor;
