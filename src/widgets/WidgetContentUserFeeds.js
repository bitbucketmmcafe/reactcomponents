var FeedsList = React.createClass({
  render: function() {
    
    if(this.props.feeds) {
        var feednodes = this.props.feeds.map(function(feed, index) {
            return <li key={ "userfeed_" + index }>
                        <a href={feed.url} title={feed.title} target={"_blank"}>{ feed.title }</a>
                    </li>;
            
        }.bind(this));
    }
    return (
		<ul className="mm-feedlist">
			{ feednodes }
		</ul>
    );
  }
});


var WidgetContentUserFeeds = React.createClass({

	first_load: true,

	/**
	 * Transforms query string into json object, to be used for post
	 */
	_generate_post_data: function(data) {
		var obj = {};
		this.state.data.source.save.split('?')[1].split('&').forEach(function(item) {
			var x = item.split('=');
			obj[x[0]] = x[1];
		})

		obj.valor = JSON.stringify(data);

		return obj;
	},

	_submit: function(data, key) {
		if(key)
			PubSub.publish("components.loader.add", { key: "userfeed." + key } );

		return $.ajax({
			url: this.state.data.source.save.split('?')[0],
			type: 'post',
			data: this._generate_post_data(data)
		})
		.then(function(response) {
			if(key)
				PubSub.publish("components.loader.remove", { key: "userfeed." + key, type: "success" } );
		}.bind(this))
		.then( this._getUserFeeds )
		.fail(function(jqXHR) {
			console.log('has failed')
			PubSub.publish("components.loader.remove", { key: "userfeed." + key, type: "error", error: jqXHR } );
		});
	},

	handleSubmit: function(e) {

		e.preventDefault();

		var data = this.state.userfeeds;
		data.push({
			disabled: true,
			feedurl: this.refs.feedurl.getDOMNode().value,
			feedname: this.refs.feedname.getDOMNode().value
		});
	
		this._submit(data, 'submit')
		.then( function() {
			this.refs.feedurl.getDOMNode().value = "";
			this.refs.feedname.getDOMNode().value = "";
		}.bind(this))
	},

	_deleteTask: function(index) {

		var data = this.state.userfeeds;
		data.splice(index, 1);
		this._submit(data, 'delete');
	
	},

	_toggleDisabled: function(index) {

		var data = this.state.userfeeds,
			userfeeds = data[index];

		data[data[index].disabled = !data[index].disabled];

		this._submit(data, 'change');

	},

	_getUserFeeds: function() {
		var url = this.state.data.source.list;
		$
		.ajax({ url: url })
		.then(function(response) {
			this.setState({ userfeeds: response, ready: true });
			var getFeeds = this.state.data.source.get_feeds
			var myFeeds = []
			this.state.userfeeds.map(function(n, index) {
				if(n.disabled == true){
					myFeeds.push(n)
				}
			});

			if(myFeeds.length > 0){
				this.consumeFeed(this.state.data.source.get_feeds+myFeeds[0].feedurl, 0 );

				

			}else{
				this.setState({feedItems:""})
			}
		}.bind(this))
		.fail(function(jqXHR) {
			console.error('Portlet Tasks - error while get userfeeds', jqXHR);
			this.setState({ ready: true });
		}.bind(this))
	},

    handleFeedClick: function(e){
    	e.preventDefault();
        PubSub.publish("components.loader.add", { key: "feed.consume" } );
		this.consumeFeed(this.state.data.source.get_feeds+e.currentTarget.href, $(e.currentTarget.parentNode).index());
    },

	handleContainerClick: function(e){
		e.preventDefault();
		if(e.currentTarget.lastElementChild.classList.contains("fa-angle-down")){
			e.currentTarget.lastElementChild.classList.add("fa-angle-up");
			e.currentTarget.lastElementChild.classList.remove("fa-angle-down")
		}else{
			e.currentTarget.lastElementChild.classList.add("fa-angle-down");
			e.currentTarget.lastElementChild.classList.remove("fa-angle-up")			
		};

		e.currentTarget.nextSibling.classList.toggle("visible");
	},


    consumeFeed: function(url, index) {
        $.ajax({
          url: url,
          method:"POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            this.setState({ feedItems: data, selectedIndex: index });

            if(!this.first_load && key.indexOf("userfeed")<= -1) {
                PubSub.publish("components.loader.remove", { key: "feed.consume", type: "success" } );
            } else {
                this.first_load = false;
            }
          }.bind(this),
          error: function(xhr, status, err) {
            PubSub.publish("components.loader.remove", { key: "feed.consume", type: "error", error: jqXHR } );
          }.bind(this)
        });

    },

    remoteData: function(data) {
		this.setState({ data: data });
		this._getUserFeeds();
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data, ready: false, userfeeds: [] };
    },

    render: function() {
        var self = this;

        var nodes = [];

        if(this.state.userfeeds) {
        	nodes = this.state.userfeeds.map(function(n, index) {
        		var className = n.disabled ? "disabled" : "";
                return <li 
                		data-index={index}
                		key={ "userfeedlistlistitem_" + index }>
                        	<input type="checkbox" name="option" value={ index } checked={ n.disabled } onChange={self._toggleDisabled.bind(null,index)} />
                        	<span className={ className } data-index={ index } dangerouslySetInnerHTML={{ __html: n.feedname.replace(/\r?\n/g, '<br />') }}></span>
                        	<button onClick={self._deleteTask.bind(null,index)}></button>
                        </li>;
                
			});


			var listfeedsnodes = this.state.userfeeds.map(function(n, index) {
				if(n.disabled == true){
                return <li key={ "userfeeds_listitem_" + index } >
                			<a href={n.feedurl} trigger={"click"} onClick={this.handleFeedClick} className={ this.state.selectedIndex == index ? "selected" : "" }>{n.feedname}</a>
                        </li>;
            	}
			}.bind(this));


        }

        return (
            <div className="widget_content" onDragOver={this.dragOver}>

			<a onClick={this.handleContainerClick} className="show-hide-container" href="#addUserRss" title="adicionar rss">
			ADICIONAR RSS
				<i className="fa fa-angle-down"></i>
			</a>

			<div className="userfeedsContainer">

	            <form className="userfeedform" onSubmit={this.handleSubmit} ref="form">

					<div>
						<label>Url: </label>
						<input type="text" name="widgetrssurl" ref="feedurl" />
					</div>
					<div>
						<label>Nome:</label>
						<input type="text" name="widgetrssname" ref="feedname"/>
					</div>

	            	<button className="btn btn-primary">
						<MMCafeReact.Components.Translator value={ "userfeeds.submit" } />
	            	</button>
	            </form>           

	            { this.state.userfeeds && 
	            <ul >
	            	{ nodes }    
	            </ul>
	            }
            </div>
            { !this.state.ready &&
            	<span className="mm-widget-notready"></span>
            }

            {listfeedsnodes &&
				
        	<div>
	            <ul className="listUserFeeds">
	                { listfeedsnodes}
	            </ul>

				<FeedsList feeds={ this.state.feedItems } />
			</div>

			}

            </div>
        );
    }
});

module.exports = WidgetContentUserFeeds;