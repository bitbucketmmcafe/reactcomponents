var WidgetContentUserSearch = React.createClass({
    
    remoteData: function(data) {
        this.setState({ data: data });
    },
    
    sourceParams: function() {
        if(this.state.data && this.state.data.userCompanySearch == true){
            var pars = {branchId : null, companyId: this.state.data.company, departmentId: null, size: 3}
            return pars;
        }
        if(this.refs.cbd.sourceParams() && this.state.data && this.state.data.userCompanySearch == false){

            var pars = this.refs.cbd.sourceParams();
            pars["size"] = 3;
            return pars;
        }
    },

    
    getInitialState: function() {
        return {html: this.props.html, data: this.props.data, fieldsSearch: "fullName,nickname,name,extension", typesearch: "name", required: true, isChecked: false};
    },
    
    componentDidMount: function() {
        var self = this;
        PubSub.subscribe( 'component.autocompleteuser.selected', function(msg, data) {
            var href = self.state.data.source.userprofile + data.user.id;
            document.location = href;
        });
        
        $(this.getDOMNode()).on("change", "select[name='companyId']", function() {
            if($(this).val()!="")
            self.refs.autocomplete.setState({ required: false })
            else
            self.refs.autocomplete.setState({ required: "required" })
        })
    },
    
    _typeSearchName: function() {
        this.setState({ typesearch: "name", fieldsSearch: "fullName,nickname,name,extension", isChecked: false });
    },
    
    _typeSearchPerfil: function() {
        this.setState({ typesearch: "profile", fieldsSearch: "fullName,nickname,cvContent,homePhone,mobilePhone,nationality,observation,myInterests", isChecked: true});
    },
 
    handleCheck: function() {
        this.setState({
            isChecked: !this.state.isChecked // flip boolean value
        }, function() {
            if(this.state.isChecked)
                this._typeSearchPerfil(); 

        }.bind(this));
        
    },        
 
    
    render: function() {
        return (
            <div className="widget_content">
            <form action={ this.state.data && !this.state.isChecked? this.state.data.action: "/user.search.view?method=searchUserByPrefix" } className={"mm-defaultform-submit"} ref={"form"} method="post" autoComplete="off">
            <input type="hidden" name="search" value="true" />
            { this.state.data && this.state.data.userCompanySearch==true && this.state.data.company &&
            <input type="hidden" name="companyId" value={this.state.data.company} />
            }
            
            { this.state.data && this.state.data.contextualSearch==false &&
                <input type="hidden" name="searchFieldsUser" value={ this.state.fieldsSearch } />
            }
            { this.state.data && this.state.data.contextualSearch==true && this.state.isChecked && this.state.data.checkProfile==true &&
                <input type="hidden" name="searchFieldsUser" value={ this.state.fieldsSearch } />
            }
            
            { this.state.data && this.state.data.source && !this.state.data.disable_cbd && this.state.data.source.business_fields==true && this.state.data.userCompanySearch==false &&
                <MMCafeReact.Components.CBD ref={ "cbd" } data={ this.state.data } />
            }
            
            { this.state.data && this.state.data.canSeeCV && this.state.data.contextualSearch==false &&
                
                <fieldset className="select-type">
                <input type="radio" name="searchTalents" checked={ this.state.typesearch == "name" }  onChange={ this._typeSearchName } /><label><MMCafeReact.Components.Translator value={ "usersearch.name" } /></label>
                <input type="radio" name="searchTalents" checked={ this.state.typesearch == "profile" } onChange={ this._typeSearchPerfil } /><label><MMCafeReact.Components.Translator value={ "usersearch.profile" } /></label>
                </fieldset>
            }
            
            { this.state.data && this.state.data.contextualSearch==false && this.state.data.userCompanySearch==false &&
                <fieldset>     
                <label className="label-name"><MMCafeReact.Components.Translator value={ "usersearch.name" } /></label>
                <MMCafeReact.Components.AutocompleteUser ref="autocomplete" required="required" maxlength={ this.state.data.maxlength } disableautocomplete={ this.state.data.disableautocomplete } source={ this.state.data && !this.state.isChecked ? this.state.data.source.usersearch: "/ajax/users/user.view?method=getUsersStartedWithSuggest&" } sourceParams={ this.sourceParams }
                sourceParams2={this.state.fieldsSearch}></MMCafeReact.Components.AutocompleteUser>
                </fieldset>
            }

            { this.state.data && this.state.data.contextualSearch==false && this.state.data.userCompanySearch==true &&
                <fieldset>     
                <label className="label-name"><MMCafeReact.Components.Translator value={ "usersearch.name" } /></label>
                <MMCafeReact.Components.AutocompleteUser ref="autocomplete" required="required" maxlength={ this.state.data.maxlength } disableautocomplete={ this.state.data.disableautocomplete } source={ this.state.data && !this.state.isChecked ? this.state.data.source.usersearch: "/ajax/users/user.view?method=getUsersStartedWithSuggest&" } sourceParams={this.sourceParams}
                sourceParams2={this.state.fieldsSearch}></MMCafeReact.Components.AutocompleteUser>
                </fieldset>
            }
            
            { this.state.data && this.state.data.contextualSearch==true && 
                <fieldset>                
                <label className="label-name"><MMCafeReact.Components.Translator value={ this.state.data.contextualLabel } /></label>
                <MMCafeReact.Components.AutocompleteUser ref="autocomplete" required="required" disableautocomplete={ this.state.data.disableautocomplete } source={ this.state.data &&  !this.state.isChecked ? this.state.data.source.usersearch: "/ajax/users/user.view?method=getUsersStartedWithSuggest&" } sourceParams={ this.state.data.source.business_fields==true? this.sourceParams:""} 
                sourceParams2={ this.state.isChecked ? this.state.fieldsSearch:""} business_fields={this.state.data.source.business_fields} contextualSearch={true}></MMCafeReact.Components.AutocompleteUser>                             
                </fieldset>                
            }
            
            { this.state.data && this.state.data.canSeeCV && this.state.data.contextualSearch==true && this.state.data.checkProfile==true &&
                <fieldset className="select-type">                  
                <input type="checkbox" name="searchTalents" checked={this.state.isChecked}  onChange={this.handleCheck}/><label><MMCafeReact.Components.Translator value={ "usersearch.profile" } /></label>
                </fieldset>  
            }
            
            <button type="submit" className={ "btn btn-primary btn-mm-defaultform-submit" }>
            <MMCafeReact.Components.Translator value={ "usersearch.submit" } />
            </button>
            </form>
            
            </div>
        );
    }
});

module.exports = WidgetContentUserSearch;
