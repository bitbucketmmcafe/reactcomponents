var WidgetContentAbilityCards = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        var nodes = null;
        if(this.state.data && this.state.data.cards) {
            nodes = this.state.data.cards.map( function(card) {
                return <div key={ "carditem_" + card.id }>
                    <img src={ card.image } />
                    <h3>{ card.title }</h3>

                    <ul>
                        <li>
                             <MMCafeReact.Components.Translator value={ "cards.to" } />: <MMCafeReact.Components.UserLink key={ "userlinkfor_" + card.to.id } user={ card.to } />
                        </li>
                        <li>
                           <MMCafeReact.Components.Translator value={ "cards.from" } />: <MMCafeReact.Components.UserLink key={ "userlinkfor_" + card.from.id } user={ card.from } />
                        </li>
                        <li>
                            <MMCafeReact.Components.Translator value={ "cards.sent_at" } /> { card.sent_at } 
                        </li>
                    </ul>

                    <p>{ card.message }</p>
                </div>
            })
        }
        return (
            <div className="widget_content">
            { nodes }
            { this.state.data && this.state.data.url_folder &&
            <footer><a href={ this.state.data.url_folder}>{ this.state.data.url_label }</a></footer>
            }
            </div>
        );
    }
});

module.exports = WidgetContentAbilityCards;
