var WidgetContentsTasksmanager = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        var nextTasksArea = null, overdueTasksArea = null;

        var hasNextTasks = false, hasOverdueTasks = false;

        if(this.state.data && this.state.data.overdueTasks && this.state.data.overdueTasks.length > 0) {
            hasOverdueTasks = true;
            var nodes = this.state.data.overdueTasks.map(function(task) {

                const spanStyle = {
                    color: task.category.color ? task.category.color : 'black'
                };
            
                return <li key={task.id} className={ 'status_' + task.color }>
                    <span className="status_highlight"><MMCafeReact.Components.Translator key={ "last" } value={ "tasksmanager.last" } /> { task.days } 

                    {task.days == 1 && 
                        <span> <MMCafeReact.Components.Translator key={ "days" } value={ "tasksmanager.day" } /></span>
                    }
                    {task.days > 1 && 
                        <span> <MMCafeReact.Components.Translator key={ "days" } value={ "tasksmanager.days" } /></span>
                    } 

                    </span>
                    <a href={ this.state.data.url_talktrack + task.url } > <span style={spanStyle}> { task.category.name }</span> { task.title }</a>
                    <div><em>&nbsp;<MMCafeReact.Components.Translator key={ "last" } value={ "tasksmanager.deadline" } /> { task.deadline }</em></div>
                </li>
            }.bind(this));
            overdueTasksArea = <div>
                <h3 className='overdue'><MMCafeReact.Components.Translator key={ "nexts" } value={ "tasksmanager.overdue" } /></h3>
                <ul>{ nodes }</ul>
                </div>
        }


        if(this.state.data && this.state.data.nextTasks && this.state.data.nextTasks.length > 0) {
            hasNextTasks = true;
            var nodes = this.state.data.nextTasks.map(function(task) {

                const spanStyle = {
                    color: task.category.color ? task.category.color : 'black'
                };

                return <li key={task.id} className={ 'status_' + task.color }>

                    <span className="status_highlight">

                    {task.days == 1 && 
                    <span> <MMCafeReact.Components.Translator key={ "missing" } value={ "tasksmanager.miss" } /> {task.days} <MMCafeReact.Components.Translator key={ "days" } value={ "tasksmanager.day" } /></span>
                    }
                    {task.days > 1 && 
                    <span> <MMCafeReact.Components.Translator key={ "missing" } value={ "tasksmanager.missing" } /> {task.days} <MMCafeReact.Components.Translator key={ "days" } value={ "tasksmanager.days" } /></span>
                    }
                    </span>
                    <a href={ this.state.data.url_talktrack + task.url } ><span style={spanStyle}> { task.category.name }</span> { task.title }</a>
                    <div className="taskDates"><em>&nbsp;Início: {moment(task.updatedAt).format('DD/MM/YYYY')} - Fim: { task.deadline }</em></div>
                </li>
            }.bind(this));
            nextTasksArea = <div>
                <h3 className='next'><MMCafeReact.Components.Translator key={ "nexts" } value={ "tasksmanager.next" } /></h3>
                <ul>{ nodes }</ul>
                </div>
        }
        return (
            <div className="widget_content">
                { overdueTasksArea }
                { nextTasksArea }
                { (this.state.data && (!hasNextTasks && !hasOverdueTasks)) &&
                <div>
                    <h4><MMCafeReact.Components.Translator key={ "congrats" } value={ "tasksmanager.congrats" } /></h4>
                    <p><MMCafeReact.Components.Translator key={ "congrats" } value={ "tasksmanager.congrats.desc" } /></p>
                </div>
                }
                { this.state.data && this.state.data.url_talktrack &&
                <footer><a href={ this.state.data.url_talktrack}>{ this.state.data.url_label }</a></footer>
                }
            </div>
        );
    }
});

module.exports = WidgetContentsTasksmanager;
