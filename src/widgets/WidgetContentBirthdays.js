var WidgetContentBirthdays = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        var data = {}, 
            today = null, 
            nodestoday = [], 
            others = [], 
            hide_company = false,
            hide_company_for_birthday_widget = false,
            show_branch = false, 
            todaymarkup = null, 
            byadmissiondate="false";
        
        if(this.state.data && this.state.data.users) {
            hide_company = this.state.data.hide_company;
            show_branch = this.state.data.show_branch;
            hide_company_for_birthday_widget = this.state.data.hide_company_for_birthday_widget;
            if(this.state.data.byadmissiondate && this.state.data.byadmissiondate==true){
                this.state.data.users.forEach(function(user) {
                    data[user.admissionDate_summary] ?
                        data[user.admissionDate_summary].push(user) :
                        data[user.admissionDate_summary] = [user];
                });

                byadmissiondate= "true";

            }else{
                show_branch = this.state.data.show_branch;
                hide_company_for_birthday_widget = this.state.data.hide_company_for_birthday_widget;
                this.state.data.users.forEach(function(user) {
                    data[user.birthdate] ?
                        data[user.birthdate].push(user) :
                        data[user.birthdate] = [user]; 
                });
                byadmissiondate= "false";
            }
            
            
            var today = moment().format('DD/MM'),
                tomorrow = moment().add(1, 'day');
            
            if(data[today]){

                todaymarkup = <h3>{ today } - <MMCafeReact.Components.Translator value={ "birthdays.today" } /></h3>;
            }
                
            for(var key in data) {

                if(key == today) {
                    
                   nodestoday = data[key].map(function(user) {
                    
                        return(<div key={ "userhighlight_" + user.id }><img src={ user.avatar } />
                                <div className="birthday-info">
                                <MMCafeReact.Components.UserLink key={ "userlinkfor_" + user.id } user={ user } />
                                { (!hide_company && !hide_company_for_birthday_widget) && user.company &&
                                <span>
                                    <MMCafeReact.Components.CompanyLabel key={ "companyfor_" + user.id } data={ user.company } />
                                    <br/>
                                </span>
                                }
                                { show_branch && user.branch && 
                                    <span>
                                        <MMCafeReact.Components.BranchLabel key={ "branchfor_" + user.id } data={ user.branch } />
                                    </span>
                                }
                                {byadmissiondate=="true" && user.yearsincompany > 0 &&
                                    <span className="yearsincompany">{user.yearsincompany}
                                        {user.yearsincompany == 1 && 
                                            <span> {MMCafeReact.Components.Translator.getI18n('birthdays.year')}</span>
                                        }
                                        {user.yearsincompany > 1 && 
                                            <span> {MMCafeReact.Components.Translator.getI18n('birthdays.years')}</span>
                                        }
                                    </span>
                                }
                                </div>
                                </div>);
                       
                   });
                } else {

                    var nodes = data[key].map(function(user) {
                        return(<div key={ "userhighlight_" + user.id } className="widget-content no-avatar">
                                <MMCafeReact.Components.UserLink key={ "userlinkfor_" + user.id } user={ user } />
                                { (!hide_company && !hide_company_for_birthday_widget) && user.company &&
                                <span>
                                    <MMCafeReact.Components.CompanyLabel key={ "companyfor_" + user.id } data={ user.company } />
                                    <br/>
                                </span>
                                }
                                { show_branch && user.branch && 
                                <div>
                                    <MMCafeReact.Components.BranchLabel key={ "branchfor_" + user.id } data={ user.branch } />
                                </div>
                                }
                                {byadmissiondate=="true" && user.yearsincompany > 0 &&
                                    <span className="yearsincompany">{user.yearsincompany}
                                        {user.yearsincompany == 1 && 
                                            <span> {MMCafeReact.Components.Translator.getI18n('birthdays.year')}</span>
                                        }
                                        {user.yearsincompany > 1 && 
                                            <span> {MMCafeReact.Components.Translator.getI18n('birthdays.years')}</span>
                                        }
                                    </span>
                                }
                                </div>);
                    });
                    others.push(<div key={ "dateuserhighlight_" + key }><h4>
                                    { key != "null" ? key : "" &&
                                        key
                                    }
                                    { tomorrow.format('DD/MM') == key && 
                                    <span> - { MMCafeReact.Components.Translator.getI18n('birthdays.tomorrow') }</span>
                                    }</h4>
                                    { nodes }
                                </div>);
                }
            }
        }
        
        return (
            <div className="widget_content">
                { todaymarkup }
                { nodestoday }
                { nodestoday.length > 0 &&
                <hr />
                }
               
                { others }

                { (!nodestoday || nodestoday.length == 0) && others.length == 0 && 
                    <div className="alert mm-alert-error">
                        <MMCafeReact.Components.Translator value={ "birthdaycalendar.noresults" } />
                    </div>
                }

                { this.state.data &&
                <footer><a href={ this.state.data.url_birthdays}>{ this.state.data.url_label }</a></footer>
                }
            </div>
        );
    }
});

module.exports = WidgetContentBirthdays;
