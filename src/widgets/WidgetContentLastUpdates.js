var WidgetContentLastUpdates = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        
        if(this.state.data && this.state.data.lastupdates) {

            var hash = {
                1: {
                    "status": "accessed",
                    "label": MMCafeReact.Components.Translator.getI18n("statusaccess.accessed"),
                    "icon": "check-circle"
                },
                2: {
                    "status": "notaccessed",
                    "label": MMCafeReact.Components.Translator.getI18n("statusaccess.notaccessed"),
                    "icon": "info-circle"
                },
                3: {
                    "status": "urgent",
                    "label": MMCafeReact.Components.Translator.getI18n("statusaccess.urgent"),
                    "icon": "exclamation-triangle"
                }
            }

			var nodes = this.state.data.lastupdates.map(function(n) {
                
                var status = null;
                
                if(this.state.data.check_statusaccess) {
                    var statusaccess_item = hash[n.statusaccess];
                    var classname = "mm-statusaccess-label mm-statusaccess-" + statusaccess_item.status;
                    var classname_icon = "fa fa-" + statusaccess_item.icon; 
                    status = <span className={ classname }>  
                        <i className={ classname_icon } aria-hidden="true"></i>
                        { statusaccess_item.label }
                    </span>
                }

                n.remote = true;
				return <li key={ "listitem_" + n.id }>
                    <div>
                    <a href={ n.url } dangerouslySetInnerHTML={{ __html: n.name  }}></a>{ status }
                    </div>
                </li>
			}.bind(this));
        }


        return (
            <div className="widget_content">
            	<ol>
            	{ nodes }
            	</ol>
                { this.state.data && this.state.data.url_folder &&
                <footer><a href={ this.state.data.url_folder}>{ this.state.data.url_label }</a></footer>
                }
            </div>
        );
    }
});

module.exports = WidgetContentLastUpdates;