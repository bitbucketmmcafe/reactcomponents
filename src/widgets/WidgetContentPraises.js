var WidgetContentPraises = React.createClass({

    _formatDate: function(date) {

    },

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        if(this.state.data && this.state.data.praises) {
			var nodes = this.state.data.praises.map(function(n) {
                return <li key={ "listitem_" + n.id }>
                          <MMCafeReact.Components.Link data={ n } />
                        </li>;
			});
			
        }
        return (
            <div className="widget_content">
                
                <ul>
                { nodes }
                </ul>
                { this.state.data && this.state.data.url_folder &&
                    <footer><a href={ this.state.data.url_folder}>{ this.state.data.url_label }</a></footer>
                }
            </div>
        );
    }
});

module.exports = WidgetContentPraises;