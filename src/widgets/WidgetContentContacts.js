var WidgetContentContacts = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        var nodes = null;

        if(this.state.data && this.state.data.contacts) {
            nodes = this.state.data.contacts.map(function(user) {
                var ul = null;
                if(user.phone && (user.areaPrefix || user.phone.extension || user.phone.phone)) {
                    ul = <ul>
                        { (user.phone.areaPrefix || user.phone.phone) &&
                        <li>
                            <MMCafeReact.Components.Translator key={ "t_phonenumber_" + user.id } value={ "phone.number" } />: 
                                { user.phone.areaPrefix && <span> ({ user.phone.areaPrefix })</span> } { user.phone.phone }
                        </li>
                        }
                        { user.phone.extension &&
                        <li><MMCafeReact.Components.Translator key={ "t_phoneextension_" + user.id } value={ "phone.extension" } />: { user.phone.extension }</li>
                        }
                    </ul>
                }
                return <li key={ "userlifor_" + user.id }>
                    <img src={ user.avatar } />
                    <MMCafeReact.Components.UserLink key={ "userlinkfor_" + user.id } user={ user } />
                    { ul }
                </li>
            });
            
        }
        return (
            <div className="widget_content">
                <ul>
                { nodes }
                </ul>
                { this.state.data && this.state.data.url_contacts &&
                <footer><a href={ this.state.data.url_contacts}>{ this.state.data.url_label }</a></footer>
                }
            </div>
        );
    }
});

module.exports = WidgetContentContacts;
