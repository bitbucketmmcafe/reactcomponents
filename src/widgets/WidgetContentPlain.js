var WidgetContentPlain = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        return (
            
            <div className="widget_content">
                {this.state.data && this.state.data.html &&
                    <div dangerouslySetInnerHTML={{ __html: this.state.data.html  }}></div>
                }
            </div>
        );
    }
});

module.exports = WidgetContentPlain;