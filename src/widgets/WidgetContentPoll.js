var WidgetContentPoll = React.createClass({

    _build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },

	handleSubmit: function(e) {
		var form = e.target, 
            val = form.elements.answers.value;

		PubSub.publish("components.loader.add", { key: "poll.submit" } );

        var url = this.state.data.source.vote + form.querySelector('input[name="answers"]:checked').value;
        var builded_data = this._build_data(url);
        $
        .ajax({url: builded_data.url, data: builded_data.data, method: 'post' })
		.then(function(response) {
			PubSub.publish("components.loader.remove", { key: "poll.submit", type: "success" } ); 
			this.setState({ result: response, voted: true });
            window.localStorage.setItem("is_poll_voted_" + this.state.data.poll.id, true);
		}.bind(this))
        .fail(function(jqXHR) {
            PubSub.publish("components.loader.remove", { key: "poll.submit", type: "error", error: jqXHR } ); 
        })

        e.preventDefault();
	},

    updateResults: function(e) {

        if(e)
            PubSub.publish("components.loader.add", { key: "poll.loadingresults" } ); 
        $
        .ajax({ url: this.state.data.source.view })
        .then(function(response) {
            if(e)
                PubSub.publish("components.loader.remove", { key: "poll.loadingresults", type: "success" } ); 
            this.setState({ result: response });
        }.bind(this))
        .fail(function(jqXHR) {
            if(e)
                PubSub.publish("components.loader.remove", { key: "poll.loadingresults", type: "error", error: jqXHR } ); 
        })

        if(e)
            e.preventDefault();
    },

    backToPoll: function(e) {
        this.setState({ result: null });
        e.preventDefault();
    },

    remoteData: function(data) {
        this.setState({ data: data });
        if(window.localStorage.getItem("is_poll_voted_" + data.poll.id)) {
            this.setState({ voted: true });
            this.updateResults();
        }
            
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data, voted: false };
    },

    render: function() {
        
        var self = this;

        if(this.state.result) {

        	var nodes = this.state.result.options.map(function(n) {
                var perc = 0
                if(n.votes > 0){
                    perc = Math.round(100 * n.votes / self.state.result.total )
                }

        		divStyle = { width: perc + "%"}

                return <li key={ "listitem_" + n.id }>
                          { n.name }
                          <div className="progress">
								<div className="progress-bar" role="progressbar" aria-valuenow={ perc } aria-valuemin="0" aria-valuemax="100" style={divStyle}>
								{ perc }%
								</div>
							</div>
                        </li>;
			});

        	return (
	            <div className="widget_content">
	          		<h4>{ this.state.data.poll.name }</h4>

                    { this.state.html && 
                    <div className="htmlPoll" dangerouslySetInnerHTML={{__html:this.state.html}}/>
                    }

	          		<ul>
                		{ nodes }
                	</ul>
                    { !this.state.voted &&
                    <button className="btn btn-primary" onClick={ this.backToPoll }><MMCafeReact.Components.Translator value={ "poll.back" } /></button> 
                    }
	            </div>
	        );
        }

        /* não está vendo os resultados */
        if(this.state.data && this.state.data.poll ) {
			var nodes = this.state.data.poll.options.map(function(n) {
                return <li key={ "listitem_" + n.id }>
                          <input type="radio" name="answers" required value={ n.id } /><span>{ n.name }</span>
                        </li>;
			});
			
        }

        return (
            <div className="widget_content">
            { this.state.data && 
                <form onSubmit={this.handleSubmit}>
                	<h4>{ this.state.data.poll.name }</h4>

                    { this.state.html && 
                    <div className="htmlPoll" dangerouslySetInnerHTML={{__html:this.state.html}}/>
                    }

                	<ul>
                		{ nodes }
                	</ul>
                	<button className="btn btn-primary btn-mm-defaultform-submit"><MMCafeReact.Components.Translator value={ "poll.submit" } /></button>
                    <button className="btn btn-primary btn-mm-defaultform-submit" onClick={ this.updateResults }><MMCafeReact.Components.Translator value={ "poll.viewresults" } /></button>
                </form>
            }
            </div>
        );
    }
});

module.exports = WidgetContentPoll;