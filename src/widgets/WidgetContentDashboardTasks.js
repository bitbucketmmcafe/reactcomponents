/**
 * Widget de tasks no dashboard.
 */
var WidgetContentDashboardTasks = React.createClass({

    _fistrender: true,

    query_params: {
        grName: null,
        gglName: null
    },

    propTypes: {
        /**
         * Object with sources = { source: { listStatusStats: "url" } }
         */
        data: React.PropTypes.object
    },

    getStatus: function () {

        var self = this;

        PubSub.publish("components.loader.add", {key: "dashboard.task.status"});

        $.ajax({
            url: self.state.data.source.listStatusStats,
            data: {
                gr: this.query_params.grName,
                ggl: this.query_params.gglName
            }
        })
            .then(function (response) {
                PubSub.publish("components.loader.remove", {key: "dashboard.task.status", type: "success"});
                self.state.statusstats = [];
                self.setState({statusstats: response});
            })
            .fail(function (jqXHR) {
                console.log('has failed')
                PubSub.publish("components.loader.remove", {key: "dashboard.task.status", type: "error", error: jqXHR});
            });
    },

    handleQueryParams: function (queryParams) {
        this.query_params.grName = queryParams.grName;
        this.query_params.gglName = queryParams.gglName;
        this.getStatus();
    },

    remoteData: function (data) {
        this.setState({data: data});
    },

    getInitialState: function () {
        return {html: this.props.html, data: this.props.data};
    },

    sourceParams: function () {
        return this.query_params;
    },

    render: function () {

        var data = {}, listStatusStats = [];

        if (this.state && this.state.statusstats && this.query_params.grName && this.query_params.gglName) {
            if (this.state.statusstats.length > 0) {

                listStatusStats = this.state.statusstats.map(function (status) {

                    var styleStatus = {
                        width: status.percentage + "%"
                    };

                    var colorStatusBar = status.statusname === 'Aberto' ? "progress-bar progress-bar-success" :
                        status.statusname === 'Em andamento' ? "progress-bar progress-bar-warning" :
                            status.statusname === 'Finalizado no prazo' ? "progress-bar progress-bar-info" :
                                status.statusname === 'Finalizado em atraso' ? "progress-bar progress-bar-danger" : "";


                    return (
                        <div>
                            <p>{status.statusname}</p>
                            <div className="progress">
                                <div className="progress-bar" role="progressbar" aria-valuenow={status.percentage} aria-valuemin="0" aria-valuemax="100"
                                     data-toggle="tooltip" data-placement="top" title={status.statusname} style={styleStatus}>
                                    {status.percentage + "%"}
                                </div>
                            </div>
                        </div>);
                });
            } else {
                listStatusStats = (<div>
                    <span className="no_result_statusstats">Sem resultados.</span>
                </div>);
            }
        }

        return (<div className="widget_content">
            {this.state.data && this.state.data.source &&
            <MMCafeReact.Components.GRGGL ref={"grggl"} data={this.state.data} handleQueryParams={this.handleQueryParams}/>
            }
            {this.state.data && this.state.data.source && this.state.statusstats &&
            <div className="statusstats">
                <label><MMCafeReact.Components.Translator value={"dashboard.statusstats.label"}/></label>
                {listStatusStats}
            </div>
            }
            <div className="tasks_link_internal">
                <a href="/m/dashboard/tarefas" title="#springMessageText('dashboard.widget.tarefas','Tarefas')" class="btn btn-primary button">
                    <MMCafeReact.Components.Translator value={"dashboard.task.link.internal"}/>
                </a>
            </div>
        </div>);
    }

});

module.exports = WidgetContentDashboardTasks;
