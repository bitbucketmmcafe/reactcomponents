
var WidgetContentWeather = React.createClass({

    timeout_cache: 600000, //600000 miliseconds = 10 minutos
    first: true,

    remoteData: function(data) {
        this.setState({ data: data, cities: data.cities, images_context: data.images_context});
    },

    geolocation: function(event) {
        
        var geolocation = navigator.geolocation;
    
        if (navigator.geolocation && this.first==true) {
            
            navigator.geolocation.getCurrentPosition(this.get_data, this.show_error);
            this.first = false;
            
        } else { 
            return false;
        }
    },

    get_data : function(position, type) {

        var lastAPICall = (type  == "city" ? localStorage.getItem("mm-weather-lastcall " + position) :
                          localStorage.getItem("mm-weather-lastcall"));


        if(!lastAPICall || ((+new Date) - lastAPICall > this.timeout_cache )) {
            
            var now = new Date();
            var q = '';

            if(type == "city") {
                localStorage.setItem("mm-weather-lastcall " + position, +new Date);
                q = 'format=json&u=c&location=' + position;
            } else {
                var q ='format=json&u=c&lat='+position.coords.latitude + "&lon=" + position.coords.longitude
                localStorage.setItem("mm-weather-lastcall", +new Date)
            }

            this.request_API(q, type, position)


        } else {
            
            var data = type == "city" ? JSON.parse(localStorage.getItem("mm-weather-lastdata " + position))
                 : JSON.parse(localStorage.getItem("mm-weather-lastdata"));

            var valid_data = !!data
            
            if(valid_data){

                this.prepare_data_to_render(data);
            }else{
                var q = 'format=json&u=c&location=São Paulo, SP'
                this.request_API(q, type, position)
            }

        }
           
    },

    request_API : function(q, type, position){

        var OAuth = require('oauth');
        var header = {
            "X-Yahoo-App-Id": "RyqdgE3e"
        };
        var request = new OAuth.OAuth(
            null,
            null,
            'dj0yJmk9MzJqTXZVczlLWU5JJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTM0',
            '9b5635304f738bcf0a38ac13a8dc61380e4930bb',
            '1.0',
            null,
            'HMAC-SHA1',
            null,
            header
        );
        request.get(
            'https://weather-ydn-yql.media.yahoo.com/forecastrss?' + q ,
            null,
            null,
            function (err, data, result) {
                if (err) {
                    console.log('API request erro: ' + err);
                } else {

                    if(!!type && type == "city")
                        localStorage.setItem("mm-weather-lastdata " + position, data);
                    else
                        localStorage.setItem("mm-weather-lastdata", data);
                
                    this.prepare_data_to_render(data);
                }
            }.bind(this)
        )

    },

    prepare_data_to_render: function(data) {
       
        if(typeof data === 'string'){
            data = JSON.parse(data)
        }
        
        var valid_data = !!data;

        if(!data || data.error) {
            this.setState({ 
                weather: null,
                invalid: false,
                error: true
            });
            console.error("Weather widget error: ", data ? data.error : '')
            return false;
        }
        if(data.length == 0) {
            this.setState({ 
                weather: null,
                invalid: true,
                error: false
            });
            console.warn("Weather query no results");
            return false;
        }
        function getAltTemp(unit, temp) {
            return (unit === 'f' ? Math.round((5.0/9.0)*(temp-32.0)) : Math.round((9.0/5.0)*temp+32.0) )
        }

        var result = data,
        weather = {},
        forecast,
        compass = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW', 'N'],
        image404 = "https://s.yimg.com/os/mit/media/m/weather/images/icons/l/44d-100567.png";
        weather.title = result.current_observation.condition.text;
        weather.temp = result.current_observation.condition.temperature;
        weather.code = result.current_observation.condition.code;
        weather.todayCode = result.forecasts[0].code;
        weather.currently = result.current_observation.condition.text;
        weather.high = result.forecasts[0].high;
        weather.low = result.forecasts[0].low;
        weather.text = result.forecasts[0].text;
        weather.humidity = result.current_observation.atmosphere.humidity;
        weather.pressure = result.current_observation.atmosphere.pressure;
        weather.rising = result.current_observation.atmosphere.rising;
        weather.visibility = result.current_observation.atmosphere.visibility;
        weather.sunrise = result.current_observation.astronomy.sunrise;
        weather.sunset = result.current_observation.astronomy.sunset;
        //weather.description = result.item.description;
        weather.city = result.location.city;
        weather.country = result.location.country;
        weather.region = result.location.region;
        weather.updated = result.current_observation.pubDate;
        //weather.link = result.item.link;
        //weather.units = {temp: result.units.temperature, distance: result.units.distance, pressure: result.units.pressure, speed: result.units.speed};
        weather.wind = {chill: result.current_observation.wind.chill, direction: compass[Math.round(result.current_observation.wind.direction / 22.5)], speed: result.current_observation.wind.speed};

        if(result.current_observation.condition.temperature < 80 && result.current_observation.atmosphere.humidity < 40) {
            weather.heatindex = -42.379+2.04901523*result.current_observation.condition.temp+10.14333127*result.current_observation.humidity-0.22475541*result.current_observation.condition.temperature*result.current_observation.atmosphere.humidity-6.83783*(Math.pow(10, -3))*(Math.pow(result.current_observation.condition.temperature, 2))-5.481717*(Math.pow(10, -2))*(Math.pow(result.current_observation.atmosphere.humidity, 2))+1.22874*(Math.pow(10, -3))*(Math.pow(result.current_observation.condition.temperature, 2))*result.current_observation.atmosphere.humidity+8.5282*(Math.pow(10, -4))*result.current_observation.condition.temperature*(Math.pow(result.current_observation.atmosphere.humidity, 2))-1.99*(Math.pow(10, -6))*(Math.pow(result.current_observation.condition.temperature, 2))*(Math.pow(result.current_observation.atmosphere.humidity,2));
        } else {
            weather.heatindex = result.current_observation.condition.temperature;
        }


        weather.icon = result.current_observation.condition.code;
        weather.image = this.state.images_context +result.current_observation.condition.code+".jpg";

        weather.alt = {temp: getAltTemp("c", result.current_observation.condition.temperature), high: getAltTemp("c", result.forecasts[0].high), low: getAltTemp("c", result.forecasts[0].low)};
        weather.alt.unit = 'c';

        weather.forecast = [];
        for(var i=0;i<result.forecasts.length;i++) {
            forecast = result.forecasts[i];
            forecast.alt = {high: getAltTemp("c", result.forecasts[i].high), low: getAltTemp("c", result.forecasts[i].low)};
            forecast.iconcode = result.forecasts[i].code;
            weather.forecast.push(forecast);
        }

        this.setState({ 
            weather: weather,
            invalid: false,
            error: false
        });
    },

    override_location: function(event) {
        this.get_data(event.target.value, "city")
    },

    show_error: function(err) {
        console.log(err)
    },

    getInitialState: function() {
        return {html: this.props.html, weather: {}, cities: []};
    },

    componentDidUpdate: function(){

        var ua = window.navigator.userAgent;
        if(this.state && this.state.data.geolocation == true && ua.indexOf('Edge/') <= -1 && ua.indexOf('Trident/') <= -1){
            this.geolocation(event);
            
        }
    },

    render: function() {
        var forecast = null;

        if(this.state.weather && this.state.weather.forecast) {
            var nodes = this.state.weather.forecast.slice(0, 5).map(function(f, index) {

                var txt = "<span>" +  moment.unix(f.date).format("DD") + ":</span> " + 
                          "<span>" + f.high + "° (max)</span> " +
                          "<span>" + f.low + "°  (min)</span> " + 
                          "<span> <i class='iconweather iconweather-" + f.iconcode + "'></i></span>";
                return { text: txt };
            });

            var list = <MMCafeReact.Components.List items={ nodes } />;
            var style = { backgroundImage: 'url(' + this.state.weather.image + ')' };
        }
        var cities = null;
        if(this.state.cities) {
            cities = this.state.cities.map( function(city) {
                return <option key={"city_" + city.name} value={city.value}>{city.name}</option>;
            } )
        }
        
        return (
            <div className="widget_content">


                {this.state.invalid &&

                    <div className="alert alert-warning"><MMCafeReact.Components.Translator key={ "weathererror" } value={ "weather.error" } /></div>
                }

                {this.state.error &&
                    
                    <div className="alert alert-danger"><MMCafeReact.Components.Translator key={ "weathererror" } value={ "weather.error" } /></div>
                }

                { cities && cities.length > 0 && 
                <select onChange={this.override_location}>
                <option></option>
                { cities }
                </select>
                }
                { this.state.weather &&
                <div style={style}>
                    <p>
                        { this.state.data && !this.state.data.hide_city && <span>{this.state.weather.city}</span>}
                        { this.state.data && this.state.data.hide_city && <span></span>}
                        <span>{ this.state.weather.temp } °</span>
                    </p>
                    <i className={'iconweather iconweather-' + this.state.weather.icon }></i>
                </div>
                }
                { this.state.weather &&
                <h4><MMCafeReact.Components.Translator value={ "weather.nextdays" } /></h4>
                }
                { list }
            </div>
        );
    }
});

module.exports = WidgetContentWeather;