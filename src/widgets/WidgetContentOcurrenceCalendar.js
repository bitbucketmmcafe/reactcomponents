/**
 * WidgetContent para Calendário. Faz parte do pacote MMCafeReact.Widgets
 */

var WidgetContentOcurrenceCalendar = React.createClass({

    newdates : null,

    remoteData: function(data) {
        this.setState({ data: data });
        var date = moment();
        this.getDaysWithOcurrences(date.month(), date.year());
    },

    handleChangeCalendar: function(date) {
        this.setState({ ocurrences: [] });
        this.getDaysWithOcurrences(date.month(), date.year());
    },



    handleSelectDate: function(date) {
        var self = this, date = date.date;
        this.refs.mmcalendar.setState({ selected_date: date.date });
        PubSub.publishSync( 'widget.calendar.dayswithocurrences', { date: date } );
        var dateparam = date.date().toString() + "/" + (date.month() + 1).toString() + "/" + date.year().toString();
        var href = self.state.data.source.ocurrencesbyday + "?data=" + dateparam;
        document.location.assign(href);
        
    },

    getDaysWithOcurrences: function(month, year) {
        if(!this.state.data || !this.state.data.source)
            return false;
        $.ajax({
            url: this.state.data.source.dayswithocurrences + 
                 "month=" + (month + 1).toString() + "&" + 
                 "year=" + year.toString(),
            dataType: "json"
        })
        .then(function(response) {
            if(this.refs.mmcalendar) {
                PubSub.publishSync( 'widget.calendar.dayswithocurrences', { days: response } );
            }
            
        }.bind(this));
    },

    getInitialState: function() {
        return { html: this.props.html, dayswithocurrences: [], currentdate: new Date(), events: [], data: this.props.data, widget_id: this.props.widget_id};
    },

    componentWillUpdate: function() {
      
    },

    componentDidMount: function() {
        
        this.refs.mmcalendar.setState({ dayswithocurrences: this.newdates });
    },

    render: function() {
        if(!this.props.isVisible())
            return false;

        
        var widget_id = this.state.widget_id;

        if(this.props.data && this.props.data.source.ocurrencesbyday){
            var ocurrencesurl = this.props.data.source.ocurrencesbyday
        }
        
        return (
            <div className="widget_content">
                <MMCafeReact.Components.Calendar
                    ref={"mmcalendar"}
                    subscribedays={"widget.calendar.dayswithocurrences"}
                    handleChangeCalendar={ this.handleChangeCalendar }
                    selectDate= { this.handleSelectDate}
                    dayswithocurrences={ this.newdates }
                    selected={moment().startOf("day")}
                    url={ocurrencesurl} />
                <div dangerouslySetInnerHTML={{ __html: this.state.html  }}></div>

            </div>
        );

    }
});

module.exports = WidgetContentOcurrenceCalendar;
