
/**
 * WidgetContent para Bloco de Notas. Faz parte do pacote MMCafeReact.Widgets
 */
var WidgetContentNotepad = React.createClass({

	timeout: null,

	handleKeyUp: function(event) {
        var textareaValue = React.findDOMNode(this.refs.textarea).value;

		this.setState({ counter : this.state.maxlength - event.target.value.length, content: textareaValue });

		var txt = event.target.value;
		this.timeout = clearTimeout(this.timeout);

		this.timeout = setTimeout((function() {
            var url = this.state.data.source.update + txt,
                query = url.split("user.edit.view?"),
                arr=query[1].split("&");
            result={};
            for(i=0;i<arr.length;i++) {
                k = arr[i].split('=');
                result[k[0]] = (k[1] || '');
            };
            $
			.ajax({ url: this.state.data.source.update.split("?")[0], data: result, method: "post",cache:false })
            .fail(function(jqXHR) {
                console.error("Error while trying to save notepad's content", jqXHR)
            });
		}).bind(this), 1000);

	},

    _focus: function(event) {
        $(event.target).closest('.widget').attr("draggable", false);
    },

    _blur: function(event) {
        $(event.target).closest('.widget').attr("draggable", true);
    },

    remoteData: function(data) {
        var l = data.content ? data.content.length : 0;

        this.setState({ data: data, counter : data.maxlength - l, maxlength: data.maxlength });
        this.forceUpdate();
    },
    
    getInitialState: function() {
        return {html: this.props.html, data: this.props.data, maxlength: 250 };
    },

    render: function() {

        return (
            <div className="widget_content">
            { this.state.data && 
                <textarea ref="textarea" onChange={this.handleKeyUp} maxLength={ this.state.maxlength } onFocus={ this._focus } onBlur={ this._blur } defaultValue={ this.state.data.content }></textarea>
            }
            <span>{ this.state.counter }</span>
            </div>

        );

    }
});

module.exports = WidgetContentNotepad;
