var WidgetContentOurTeam = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {

        var data = {}, contratados = [], promovidos = [], desligados = [], hide_company = false;
        
        if(this.state.data && this.state.data.users) {

            hide_company = this.state.data.hide_company;

            contratados = this.state.data.users.contratados.map(function(user) {
                return(<li key={ "userhighlight_" + user.id }>
                                <img src={ user.avatar } />
                                <ul>
                                <li><MMCafeReact.Components.UserLink key={ "userlinkfor_" + user.id } user={ user } /></li>
                                { !hide_company && user.company  &&
                                <li><MMCafeReact.Components.Translator key={ "translatecompanyfor_" + user.id } value={ "ourteam.company" } />: <MMCafeReact.Components.CompanyLabel key={ "companyfor_" + user.id } data={ user.company } /></li>
                                }
                                { user.branch &&
                                <li><MMCafeReact.Components.Translator key={ "translatebranchfor_" + user.id } value={ "ourteam.branch" } />: <MMCafeReact.Components.BranchLabel key={ "branchfor_" + user.id } data={ user.branch } /></li>
                                }
                                { user.department &&
                                <li><MMCafeReact.Components.Translator key={ "translatedepfor_" + user.id } value={ "ourteam.department" } />: <MMCafeReact.Components.DepartmentLabel key={ "departmentfor_" + user.id } data={ user.department } /></li>
                                }
                                </ul></li>);
            });

            promovidos = this.state.data.users.promovidos.map(function(user) {
                return(<li key={ "userhighlight_" + user.id }>
                                <img src={ user.avatar } />
                                <ul>
                                <li><MMCafeReact.Components.UserLink key={ "userlinkfor_" + user.id } user={ user } /></li>
                                { !hide_company && user.company &&
                                <li><MMCafeReact.Components.Translator key={ "translatecompanyfor_" + user.id } value={ "ourteam.company" } />: <MMCafeReact.Components.CompanyLabel key={ "companyfor_" + user.id } data={ user.company } /></li>
                                }
                                { user.branch &&
                                <li><MMCafeReact.Components.Translator key={ "translatebranchfor_" + user.id } value={ "ourteam.branch" } />: <MMCafeReact.Components.BranchLabel key={ "branchfor_" + user.id } data={ user.branch } /></li>
                                }
                                { user.department &&
                                <li><MMCafeReact.Components.Translator key={ "translatedepfor_" + user.id } value={ "ourteam.department" } />: <MMCafeReact.Components.DepartmentLabel key={ "departmentfor_" + user.id } data={ user.department } /></li>
                                }
                                </ul></li>);
            });

            desligados = this.state.data.users.desligados.map(function(user) {
                return(<li key={ "userhighlight_" + user.id }>
                                <ul>
                                <li><MMCafeReact.Components.UserLink key={ "userlinkfor_" + user.id } user={ user } /></li>
                                { !hide_company && user.company &&
                                <li><MMCafeReact.Components.Translator key={ "translatecompanyfor_" + user.id } value={ "ourteam.company" } />: <MMCafeReact.Components.CompanyLabel key={ "companyfor_" + user.id } data={ user.company } /></li>
                                }
                                { user.branch &&
                                <li><MMCafeReact.Components.Translator key={ "translatebranchfor_" + user.id } value={ "ourteam.branch" } />: <MMCafeReact.Components.BranchLabel key={ "branchfor_" + user.id } data={ user.branch } /></li>
                                }
                                { user.department &&
                                <li><MMCafeReact.Components.Translator key={ "translatedepfor_" + user.id } value={ "ourteam.department" } />: <MMCafeReact.Components.DepartmentLabel key={ "departmentfor_" + user.id } data={ user.department } /></li>
                                }
                                </ul></li>);
            });

            
        }
        
        return (
            <div className="widget_content">
            
                { contratados.length > 0 &&
                <h3><MMCafeReact.Components.Translator value={ "ourteam.hired" } /></h3>
                }
                { contratados.length > 0 &&
                <ul>
                { contratados }
                </ul>
                }

                { promovidos.length > 0 &&
                <h3><MMCafeReact.Components.Translator value={ "ourteam.promoted" } /></h3>
                }
                { promovidos.length > 0 &&
                <ul>
                { promovidos }
                </ul>
                }

                { desligados.length > 0 &&
                <h3><MMCafeReact.Components.Translator value={ "ourteam.fired" } /></h3>
                }
                { desligados.length > 0 &&
                <ul>
                { desligados }
                </ul>
                }


            </div>
        );
    }
});

module.exports = WidgetContentOurTeam;
