var entries = [
    { "id": 1, "name": "label 1", "description": "10 visualizações", "url": "http://www.google.com", "thumb": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQfCZW-teOTh9AnEVqTBZ1DmnMQBgLhVxMYnOXSXSZOCbslwqg0" },
    { "id": 2, "name": "label 2", "url": "http://www.google.com", "views": 6, "thumb": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSh1o8W04PmxlyK49BZ3_8UnDnKbQTy1a67CSegbt9hXLW3JqI8bA" },
    { "id": 3, "name": "label 3", "description": "12 visualizações", "url": "http://www.google.com", "views": 5, "thumb": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0dI_m4E3HmbhgUSCzOiUSqCw56uT8dpzDfexIJDSW0Usd2c7z" },
    { "id": 4, "name": "label 4", "url": "http://www.google.com", "views": 8 }
]

window.onload = function() {

	var expanded = true;
    var widget = React.render(
        <MMCafeReact.WS.Widget
        	type="mostviews"
        	key="100000"
        	title="Portlet mais visualizados - static"
            data={ {entries: entries } }
        	widget_id="100000" />, 
        document.getElementById('container-widget-mostviews')
    );


    var widgetdyn = React.render(
        <MMCafeReact.WS.Widget
            type="mostviews"
            key="8"
            title="Portlet mais visualizados - dinâmico"
            remote={ true }
            source="../../examples/fixtures/8.json"
            widget_id="8" />, 
        document.getElementById('container-widget-mostviews-remote')
    );
};
