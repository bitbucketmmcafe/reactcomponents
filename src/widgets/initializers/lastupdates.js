var updates = [
    { "id": 1, "url": "http://www.google.com", "external": true, "name": "algum texto!" },
    { "id": 2, "url": "http://www.google.com", "name": "algum texto outro!" }
]

window.onload = function() {

	var expanded = true;
    var widget = React.render(
        <MMCafeReact.WS.Widget
        	type="lastupdates"
        	key="100000"
        	title="Portlet últimas atualizações - static"
            data={ {lastupdates: updates } }
        	widget_id="100000" />, 
        document.getElementById('container-widget-lastupdates')
    );

/*

    var widgetdyn = React.render(
        <MMCafeReact.WS.Widget
            type="news"
            key="5"
            title="Portlet notícias - dinâmico"
            remote={ true }
            source="../../examples/fixtures/5.json"
            widget_id="5" />, 
        document.getElementById('container-widget-news-remote')
    );
*/
};
