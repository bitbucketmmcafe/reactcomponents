var users = [
    { "id": 3, "fullname": "Christian", "nickname": "Chris", "birthdate": "15/09", "avatar": "sadsa", "company": { "name": "companhia 1", "id": 1} },
    { "id": 4, "fullname": "Christiadsan", "nickname": "Chris", "birthdate": "15/09", "avatar": "sadsa11", "company": { "name": "companhia 1", "id": 1} },
    { "id": 5, "fullname": "Christiansad", "nickname": "Chris", "birthdate": "21/09", "avatar": "sadsa11", "company": { "name": "companhia 1", "id": 1} },
    { "id": 6, "fullname": "Christiansad", "nickname": "Chris", "birthdate": "24/09", "avatar": "sadsa11", "company": { "name": "companhia 1", "id": 1} },
     { "id": 7, "fullname": "Christidsadansad", "nickname": "Chris", "birthdate": "24/09", "avatar": "sadsa11", "company": { "name": "companhia 1", "id": 1} }
];

window.onload = function() {

	var expanded = true;
    var widget = React.render(
        <MMCafeReact.WS.Widget
        	type="birthdays"
        	key="100000"
        	title="Portlet aniversariantes"
            data={ {users: users } }
        	widget_id="100000" />, 
        document.getElementById('container-widget-birthdays')
    );


    var widgetdyn = React.render(
        <MMCafeReact.WS.Widget
            type="birthdays"
            key="10"
            title="Portlet últimos comentários - dinâmico"
            remote={ true }
            source="../../examples/fixtures/10.json"
            widget_id="10" />, 
        document.getElementById('container-widget-birthdays-remote')
    );
};
