var WidgetContentCarousel = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    carrosselMount: function(dado){
 		dado.owlCarousel({
			autoPlay: 5500,
			stopOnHover : true,
			navigation : false,
			pagination: false,
			goToFirstSpeed : 2000,
			slideSpeed : 800,
			responsive : true,
			singleItem : true,
			autoHeight : false

		});

		var arrow = dado.siblings(".latest-interviews-carousel-arrows");
		$(arrow).find("a").on('click', function(event){
			dado.trigger('owl.' + event.currentTarget.dataset.direction);
		})
    },

    componentDidUpdate: function(){
    	  	
    	var owl = $(this.getDOMNode()).find(".widget-slider-container");
    	this.carrosselMount(owl)
    	
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
    	if(this.state.data && this.state.data.resources) {
	    	var images = this.state.data.resources.map(function(image, index){

				return 	<div  key={ "carrosselimage_" + image.id } className="slide">
							<ul className="list-unstyled">
								<li className="interview-slide-wrap">
								
									{image.link.url && image.link.url.length > 0 &&
										<a href={image.link.url} target={image.link.target}>
											<div className="interviews-carousel-slide-title text-center">
												<span className="interviews-slide-title">{image.name}</span>
												{!image.description &&
													<h4 className="interviews-slide-sub-title">{image.description}</h4>
												}
											</div>
											<img src={image.url} className="interviews-carousel-slide-image" alt={image.name} />
										</a>
									}

									{!image.link.url && 
										<div>
											<div className="interviews-carousel-slide-title text-center">
												<span className="interviews-slide-title">{image.name}</span>
												{!image.description &&
													<h4 className="interviews-slide-sub-title">{image.description}</h4>
												}
											</div>
											<img src={image.url} className="interviews-carousel-slide-image" alt={image.name} />
										</div>
									}	
								</li>
							</ul>
						</div>

	    	});
		}
        return (
			<div className="widget-content widget-react-slider">
				<div className="latest-interviews-carousel">
					<div className="latest-interviews-carousel-arrows">
						<a className="latest-interviews-carousel-prev" data-direction="prev"><i className="fa fa-angle-left"></i></a>
						<a className="latest-interviews-carousel-next" data-direction="next"><i className="fa fa-angle-right"></i></a>
					</div>

					<div ref="slideContainer" className="widget-latest-interviews-carousel widget-slider-container">

								{images}

					</div>
				</div>
			</div>
        );
    }

})

module.exports = WidgetContentCarousel;