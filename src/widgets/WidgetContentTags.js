var WidgetContentTags = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        
        if(this.state.data && this.state.data.tags) {
			var nodes = this.state.data.tags.map(function(tag) {
				return <a href={ tag.url } key={ "tag_"+ tag.id } title={ tag.name } data-remote="true" className={ "size_" + tag.weight }>#{ tag.name }</a>
			});
        }

        return (
            <div className="widget_content">
                { nodes }
            </div>
        );
    }
});

module.exports = WidgetContentTags;