/**
 * WidgetContent para Calendário. Faz parte do pacote MMCafeReact.Widgets
 */

var WidgetContentCalendar = React.createClass({

    newdates : null,

    remoteData: function(data) {
        this.setState({ data: data });
        var date = moment();
        this.getDaysWithEvents(date.month(), date.year());
    },

    handleChangeCalendar: function(date) {
        this.setState({ events: [] });
        this.getDaysWithEvents(date.month(), date.year());
    },

    handleSelectDate: function(date) {

        this.refs.mmcalendar.setState({ selected_date: date.date });

        var self = this, date = date.date;
        $.ajax({
            url: self.state.data.source.eventsbyday + 
                 "day=" + date.date().toString() + "&" + 
                 "month=" + (date.month() + 1).toString() + "&" + 
                 "year=" + date.year(), dataType: "json"
        })
        .then(function(response) {
            self.setState({ events: response });
        });

    },

    getDaysWithEvents: function(month, year) {
        if(!this.state.data || !this.state.data.source)
            return false;
        $.ajax({
            url: this.state.data.source.dayswithevents + 
                 "month=" + (month + 1).toString() + "&" + 
                 "year=" + year.toString(),
            dataType: "json"
        })
        .then(function(response) {
            if(this.refs.mmcalendar) {
                PubSub.publishSync( 'widget.calendar.dayswithevents', { days: response } );
                if(month == (new Date()).getMonth() && year == (new Date()).getFullYear() && response.indexOf( (new Date()).getDate() ) >= 0) {
                    $.ajax({
                        url: this.state.data.source.eventsbyday + 
                             "day=" + (new Date()).getDate().toString() + "&" + 
                             "month=" + (month + 1).toString() + "&" + 
                             "year=" + year, dataType: "json"
                    })
                    .then(function(response) {
                        this.setState({ events: response });
                    }.bind(this));
                }
            }
            
        }.bind(this));
    },

    getInitialState: function() {
        return { html: this.props.html, dayswithevents: [], currentdate: new Date(), events: [], data: this.props.data, widget_id: this.props.widget_id};
    },

    componentWillUpdate: function() {
      
    },

    componentDidMount: function() {
        this.refs.mmcalendar.setState({ dayswithevents: this.newdates });
    },

    render: function() {
        if(!this.props.isVisible())
            return false;

        var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
        
        var widget_id = this.state.widget_id;
        
        var eventsnodes = this.state.events.map(function(event, index) {
            var time = "";
            var nodeReturn = "";
            var arr = [];
            if(event.label == "Reserva de salas" && event.eventsGrouped.length > 0) {
                event.eventsGrouped.map(function(meeting, accordion_index) {
                    time = meeting.startTime.hours + ":" + ('0'+meeting.startTime.minutes).slice(-2)  + " - " +
                        meeting.endTime.hours + ":" + ('0'+meeting.endTime.minutes).slice(-2);
                    var t = {publishedBy:"<span class='title'>"+ MMCafeReact.Components.Translator.getI18n("organizedby") + ": </span>" + meeting.organizer, guests:"<span class='title'>"+ MMCafeReact.Components.Translator.getI18n("guests") + ": </span>" +  meeting.guests, description: meeting.description, id: meeting.id, name: meeting.name, observation: meeting.observation, link: meeting.url, date:time, external:false };
                    arr.push(t);
                })
                nodeReturn ={ text: event.description, label: event.address, info: arr, type:"accordion", key:index, parent_id: widget_id};
            }else{
                nodeReturn ={ text: "<span>" + event.label + "</span> " + event.name + time, link: event.url, type:"list", parent_id: widget_id};
            }
            return nodeReturn;
        });

        var allEvents = new Array;
        var eventsnodes_type = eventsnodes.map(function(event_type, index) {
            if(event_type.type == "list"){
                allEvents.push(<MMCafeReact.Components.List key={"list_" + index} items={ [event_type] } />);
            }if(event_type.type == "accordion"){
                allEvents.push(<MMCafeReact.Components.Accordion key={"accordion_" + index} items={ [event_type] } />);
            }
        });

        return (
            <div className="widget_content">
                <MMCafeReact.Components.Calendar
                    ref={"mmcalendar"}
                    subscribedays={"widget.calendar.dayswithevents"}
                    handleChangeCalendar={ this.handleChangeCalendar }
                    selectDate= { this.handleSelectDate}
                    dayswithevents={ this.newdates }
                    selected={moment().startOf("day")} />
                <div dangerouslySetInnerHTML={{ __html: this.state.html  }}></div>
                <ReactCSSTransitionGroup transitionName="mmloader" transitionEnterTimeout={500} transitionLeaveTimeout={300} >
                    <div className="panel-group container" id={"list_" + this.state.widget_id}>{ allEvents }</div>
                </ReactCSSTransitionGroup>
            </div>
        );

    }
});

module.exports = WidgetContentCalendar;
