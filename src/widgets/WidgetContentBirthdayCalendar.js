/**
 * WidgetContent para calendário de aniversários. Faz parte do pacote MMCafeReact.Widgets
 */

var WidgetContentBirthdayCalendar = React.createClass({

    newdates : null,

    remoteData: function(data) {
        this.setState({ data: data });
        this.getDaysWithBirthdays(moment().month());
    },

    handleChangeCalendar: function(date) {
        this.getDaysWithBirthdays(date.month());
    },

    handleSelectDate: function(date) {
        this.refs.mmcalendar.setState({ selected_date: date.date });
        PubSub.publishSync( 'widget.birthdaycalendar.selectdate', { date: date } );
    },

    getDaysWithBirthdays: function(month) {
        
        if(!this.state.data || !this.state.data.source){
            return false;
        }
        $.ajax({
            url: this.state.data.source.dayswithbirthdays + 
                 "month=" + (month + 1).toString(),
            dataType: "json"
        })
        .then(function(response) {
            if(this.refs.mmcalendar) {
                PubSub.publishSync( 'widget.birthdaycalendar.dayswithbirthdays', { days: response } );
            }
        }.bind(this));

    },

    getInitialState: function() {
        return { html: this.props.html, currentdate: new Date(), data: this.props.data };
    },

    render: function() {

        if(!this.props.isVisible())
            return false;

        return (
            <div className="widget_content">
                <MMCafeReact.Components.Calendar ref={"mmcalendar"} subscribedays={"widget.birthdaycalendar.dayswithbirthdays"} handleChangeCalendar={ this.handleChangeCalendar } selectDate= { this.handleSelectDate} selected={moment().startOf("day")} />
            </div>
        );

    }
});

module.exports = WidgetContentBirthdayCalendar;
