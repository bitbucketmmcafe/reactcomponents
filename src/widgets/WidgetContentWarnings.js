var WidgetContentWarnings = React.createClass({

	remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    componentDidMount: function() {
    	var self = this;
    	PubSub.subscribe("widget.warnings.read", function(msg, data) {
    		if(self.state.data && self.state.data.warnings) {
    			var filtered = self.state.data.warnings.filter(function(item) {
    				return item.id != data.warning_id;
    			});
    			var selfdata = self.state.data;
    			selfdata.warnings = filtered;
    			self.setState({ data: data });
    		}
    	});
    },

    render: function() {
        
        if(this.state.data && this.state.data.warnings) {
			var nodes = this.state.data.warnings.map(function(n) {
                return <li key={ "listitem_" + n.id }>
                            { n.thumb && 
                            <img src={ n.thumb } title={ n.name } className="img-responsive" />
                            }
                            <h3>
                                <MMCafeReact.Components.Link data={ n } />
                            </h3>
                            <MMCafeReact.Components.Translator key={ "translateat_" + n.id } value={ "content.updated.at" } />&nbsp;
                            { moment(n.lasteditiondate).format("DD/MM/YY") }
                            { n.description &&
                            <p>
                                { n.description }
                            </p>
                            }
                        </li>;
			});

			if(this.state.data.warnings.length == 0)
				PubSub.publish("widget.hideme", { widget_id: this.props.widget_id })
			
        }
        
        return (
            <div className="widget_content">
                <ul>
                { nodes }
                </ul>
                { this.state.data &&
                <footer><a href={ this.state.data.url_folder}>{ this.state.data.url_label }</a></footer>
            	}
            </div>
        );
    }
});

module.exports = WidgetContentWarnings;