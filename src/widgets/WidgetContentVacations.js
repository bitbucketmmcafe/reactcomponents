var WidgetContentVacations = React.createClass({

    _get_data: function(size) {
        return $.ajax({
            url: '/ajax/vacation/vacation.view?method=find&pageSize=' + size,
            type: 'get'
        })
        .then(function(response) {
            this.setState({ users: response}) 
        }.bind(this))
    },

    remoteData: function(data) {
        this._get_data(data.size);
        this.setState({ data: data });
    },

    getInitialState: function() {
       
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        var nodes = null;

        if(this.state.users) {
            nodes = this.state.users.map(function(user) {
                
                var ul = null;
                if(user.phone && (user.areaPrefix || user.phone.extension || user.phone.phone)) {
                    ul = <ul>
                        { (user.phone.areaPrefix || user.phone.phone) &&
                        <li>
                            <MMCafeReact.Components.Translator key={ "t_phonenumber_" + user.id } value={ "phone.number" } />: 
                                { user.phone.areaPrefix && <span> ({ user.phone.areaPrefix })</span> } { user.phone.phone }
                        </li>
                        }
                        { user.phone.extension &&
                        <li><MMCafeReact.Components.Translator key={ "t_phoneextension_" + user.id } value={ "phone.extension" } />: { user.phone.extension }</li>
                        }
                    </ul>
                }
                return <li key={ "userlifor_" + user.id }>
                    <img src={ user.avatar } />
                    <MMCafeReact.Components.UserLink key={ "userlinkfor_" + user.id } user={ user } />
                    <div>
                        <MMCafeReact.Components.DateLabel iso8061date={ user.vacationFrom } pattern={ "DD/MM/YYYY" } /> <MMCafeReact.Components.Translator value={ "vacations.to" } /> <MMCafeReact.Components.DateLabel iso8061date={ user.vacationTo } pattern={ "DD/MM/YYYY" } />
                        { ul }
                    </div>
                </li>
            });
            
        }
        return (
            <div className="widget_content">
                <ul>
                { nodes }
                </ul>
                { this.state.data && this.state.data.url_vacations &&
                <footer><a href={ this.state.data.url_vacations}>{ this.state.data.url_label }</a></footer>
                }
            </div>
        );
    }
});

module.exports = WidgetContentVacations;
