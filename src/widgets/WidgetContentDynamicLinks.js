var WidgetContentDynamicLinks = React.createClass({

    remoteData: function(data) {
        this.setState({ data: data});
        this.consumeFeed(this.state.data.source.showlinks,"");
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data, showremaining:"hidden", buttonicon:"fa-plus"};
    },

    _toggleFav: function(id, favoriteId){
    	var url="";
    	if(this.state.data.numberoffavs!="" && this.state.links.favoritesByuser < this.state.data.numberoffavs || this.state.data.numberoffavs==""){
	    	if(favoriteId){
	    		url = this.state.data.source.removelinks+favoriteId;
	    		this.consumeFeed(url,"remove");
	    	}
	    	else{
	    		url = this.state.data.source.addlinks+id;
	    		this.consumeFeed(url,"add");
	    	}
        }else if(this.state.data.numberoffavs!="" && this.state.links.favoritesByuser >= this.state.data.numberoffavs){
            if(favoriteId){
                    url = this.state.data.source.removelinks+favoriteId;
                    this.consumeFeed(url,"remove");
            }
    	}
    },

	showRemaining:function(){
		var hiddenstate = (this.state.showremaining === "hidden") ? "show" : "hidden";
		var iconstate = (this.state.buttonicon === "fa-plus") ? "fa-minus" : "fa-plus";
		this.setState({showremaining:hiddenstate, buttonicon:iconstate});
	},

    putScrollBar: function(obj){
        if(this.state.data){
            var portletheight = this.state.data.portletheight ? this.state.data.portletheight : "";   
            $(obj).mCustomScrollbar({
                setHeight: portletheight
            })
        }
    },

    consumeFeed: function(url,key) {
    	if(key)
    		PubSub.publish("components.loader.add", { key: "dynamiclinks.favorite." + key } )
        
        $.ajax({
          url: url,
          method:"POST",
          dataType: "json",
          cache: false,
          success: function(data) {
          	if(key)
				PubSub.publish("components.loader.remove", { key: "dynamiclinks.favorite." + key, type: "success" } );
            this.setState({ links: data});
          }.bind(this),
          error: function(xhr, status, err) {
            PubSub.publish("components.loader.remove", { key: "dynamiclinks.favorite."+ key, type: "error", error: jqXHR } );
          }.bind(this)
        });
    },


    componentWillUpdate: function(){
    	var scroll = $(this.getDOMNode()).find(".mCustomScrollbar");
    	this.putScrollBar(scroll)
    	
    },


	render: function() {
		var self = this;
        var dataDesc = MMCafeReact.Components.Translator.getI18n("dynamiclinks.maxamount");

        if(this.state.data && this.state.links) {
            var f = this.state.data.numberoffavs ? this.state.data.numberoffavs:5;
        	var dlinks = this.state.links.dynamiclinks;
        	var firstnodes = [];

        	dlinks.map(function(n, index) {
        		while(index < f){
        			return firstnodes.push(n)
        		}
        	})


			var nodes = firstnodes.map(function(n, index) {

				var classname = n.favorited ? "favorited " : "not-favorited";

                var tooltip = this.state.data.numberoffavs!="" && this.state.links.favoritesByuser >= this.state.data.numberoffavs && !n.favorited ? ' mm-tooltip':'';

                return <li key={ "dynamiclink_" + index } >
                			<img src={n.resource} alt={n.title} className="img-responsive" />
                			<a href={n.url} title={n.title} target="_blank">{n.title}</a>
                			<div trigger={"click"} onClick={self._toggleFav.bind(null,n.id,n.favoriteId)} data-desc={dataDesc} className={classname + tooltip} data-favorited={ this.state.favorited }></div>
                        </li>;
			}.bind(this));

        	var resnodes = dlinks.filter(function(n, index) {
        		return firstnodes.indexOf(n) != index
        	})


			var remainingnodes = resnodes.map(function(n, index) {

				var classname = n.favorited ? "favorited " : "not-favorited";
                var tooltip = this.state.data.numberoffavs!="" && this.state.links.favoritesByuser >= this.state.data.numberoffavs && !n.favorited ? ' mm-tooltip':'';

                return <li key={ "resdynamiclink_" + index } >
                			<img src={n.resource} alt={n.title} className="img-responsive" />
                			<a href={n.url} title={n.title} target="_blank">{n.title}</a>
                			<div trigger={"click"} onClick={self._toggleFav.bind(null,n.id,n.favoriteId)} data-desc={dataDesc} className={classname + tooltip} data-favorited={ this.state.favorited }></div>
                        </li>;
			}.bind(this));
        }

        var portletheight = this.state.data && this.state.data.portletheight ? this.state.data.portletheight+"px" : "";

        return (

            <div className="widget_content mCustomScrollbar" data-mcs-theme="dark"  >

            	<div>

	                <ul className="dynamicLinksList">
	                	{ nodes }
	                </ul>

	                {remainingnodes && 
	                	<div>
		                	<ul className={this.state.showremaining + " dynamicLinksList"}>
		                		{ remainingnodes }
		                	</ul>
	                		<button onClick={this.showRemaining} className="btn btn-primary"><i className={this.state.buttonicon + " fa"} aria-hidden="true"></i><MMCafeReact.Components.Translator value={ "dynamiclinks.links" } /></button>
	                	</div>
	                }

                </div>
            </div>
        );
	}

});

module.exports = WidgetContentDynamicLinks;