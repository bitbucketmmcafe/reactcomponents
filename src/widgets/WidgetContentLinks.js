var Link = require("../components/Link");

var WidgetContentLinks = React.createClass({

    _formatDate: function(date) {

    },

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    componentDidUpdate: function() {
        if(Clipboard) {
            var clipboard = new Clipboard(this.getDOMNode().querySelectorAll('.btn-tocopy'));
            clipboard.on("success", function(e) {
                e.trigger.setAttribute("title", MMCafeReact.Components.Translator.getI18n("links.copied"));
                $(e.trigger).tooltip({
                    animation: true
                })
                $(e.trigger).tooltip('show');
                setTimeout( function() {
                    $(e.trigger).tooltip('hide');
                }, 2000)
            })
        }
 
    },

    render: function() {
        
        if(this.state.data && this.state.data.links) {
			var nodes = this.state.data.links.map(function(n) {
                return <li key={ "listitem_" + n.id }>
                          <Link data={ n } />
                        <button className="btn-tocopy" data-clipboard-text={ n.url } data-toggle="tooltip" title="">
                          <i className="fa fa-files-o"></i>
                        </button>
                        </li>;
			});
        }
        return (
            <div className="widget_content">
                <p className="alert alert-warning">
                    <MMCafeReact.Components.Translator value={ "links.copylabel" } />
                </p>
                <ul>
                { nodes }
                </ul>
            </div>
        );
    }
});

module.exports = WidgetContentLinks;