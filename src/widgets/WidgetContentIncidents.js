var WidgetContentIncidents = React.createClass({

    _formatDate: function(date) {

    },

    remoteData: function(data) {
        this.setState({ data: data });
    },

    getInitialState: function() {
        return {html: this.props.html, data: this.props.data };
    },

    render: function() {
        var data={}, sameday = [];

        if(this.state.data && this.state.data.incidents) {


            this.state.data.incidents.forEach(function(incidents) {
                data[incidents.publishdate] ?
                    data[incidents.publishdate].push(incidents) :
                    data[incidents.publishdate] = [incidents]; 
            });
        for(var key in data) {
			var nodes = data[key].map(function(n) {
                return (<div key={ "listitem_" + n.id } className="incident">
                        
                          <MMCafeReact.Components.Link data={ n } />

                          <div className={n.byprop.class}>{n.byprop.status}</div>
                        
                        </div>
                    );
			});
            sameday.push(<div key={ "samedate_" + key }><h4> <MMCafeReact.Components.DateLabel iso8061date={moment.utc().format(key)} pattern={ key.date_pattern || "DD/MM/YY" } /></h4>{ nodes }</div>);
        }
			
        }
        return (
            <div className="widget_content">
                {sameday}

                { this.state.data &&
                <footer><a href={ this.state.data.url_folder}>{ this.state.data.url_label }</a></footer>
                }
            </div>
        );
    }
});

module.exports = WidgetContentIncidents;