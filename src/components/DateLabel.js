/**
 * Component para label de data. Exemplo em https://jsfiddle.net/chrisbenseler/69z2wepo/16016/
 */
var DateLabel = React.createClass({

	propTypes: {
		/**
		 * Data em formato ISO 8061: iso8061date = "2015-09-01T00:00:00Z"
		 */
		iso8061date: React.PropTypes.string,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string,
		/**
		 * pattern de formatação de data 
		 */
		pattern: React.PropTypes.string
	},

	_formatdate: function() {
    	//var txt = this.state.date.getDate() + "/" + (this.state.date.getMonth() + 1) + "/" + this.state.date.getFullYear();
        var txt = moment(this.state.date).utc().format(this.props.pattern);
        return txt;
    },

	getInitialState: function() {
		var utcDate = moment(this.props.iso8061date).utc().format();
		var date = new Date(utcDate);
        return {date: date };
    },

    render: function() {
    	
        return <span>{ this._formatdate() }</span>;
    }

});

module.exports = DateLabel;
