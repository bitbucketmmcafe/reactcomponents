/**
 * Component para link tag
 */
var Link = React.createClass({

	propTypes: {
		/**
		 * Dados do link: data = { name: "text for link", url: "http://mmcafe.com.br" }
		 */
		data: React.PropTypes.object,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string,
		/**
		 * target do link: target = "_blank"
		 */
		target: React.PropTypes.string
	},

    render: function() {
    	var regex = /(&nbsp;|<([^>]+)>)/ig;
        return <a href={this.props.data.url}
        		  title={ this.props.data.name }
        		  className={ this.props.classname }
        		  target={ this.props.data.target }
        		  data-remote={ this.props.data.remote }
        		  dangerouslySetInnerHTML={{ __html: this.props.data.name  }}></a>;
    }

});

module.exports = Link;
