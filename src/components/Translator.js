/**
 * Component para internacionalização de mensagens
 */
var Translator = React.createClass({

    
    render: function() {
        return (
            <span>{ Translator.getI18n(this.props.value) }</span>
        );
    }
});
Translator["langs"] = {};
Translator["setLang"] = function(lang) {
	Translator["lang"] = lang;
};

Translator.getI18n = function(key) {
    if(!Translator.langs[Translator.lang])
        return "no i18n[" + Translator.lang + "]: " + key;

    if(Translator.langs[Translator.lang][key] == undefined)
        return "no i18n[" + key + "]";

    return Translator.langs[Translator.lang][key];
}

module.exports = Translator;