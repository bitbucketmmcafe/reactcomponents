/**
 * Component para favoritar POs, exemplo em https://jsfiddle.net/chrisbenseler/6besahLn/1/
 */
var Favorite = React.createClass({

    _isSubmitting: false,
    pubSub: PubSub,

	propTypes: {
		/**
		 * Object target = { id: 1, favorited: false }
		 */
		target: React.PropTypes.object
	},

    _build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },

    _toggleFav: function() {

        if(!this._isSubmitting) {

            this._isSubmitting = true;

            var url = this.props.source.favorite({id: this.props.target.id, status: !this.state.favorited});
            if(this.state.favorited)
                this.pubSub.publish("components.loader.add", { key: "po.favorite.remove" } );
            else
                this.pubSub.publish("components.loader.add", { key: "po.favorite.add" } );

            var builded_data = this._build_data(url)

    		$
            .ajax({url: builded_data.url, data: builded_data.data, method: 'post' })
            .then(function(response) {
            	this.setState({favorited: !this.state.favorited, count: response.favorites});
                this._isSubmitting = false;
             }.bind(this))
            .fail(function(jqXHR) {
                this.pubSub.publish("components.loader.remove", { key: "po.favorite.remove", type: "error", error: jqXHR } );
                this.pubSub.publishSync( 'mm.favorite.error', { error: jqXHR } );
                this._isSubmitting = false;
            }.bind(this))
            .done(function() {
                if(!this.state.favorited)
                    this.pubSub.publish("components.loader.remove", { key: "po.favorite.remove" } );
                else
                    this.pubSub.publish("components.loader.remove", { key: "po.favorite.add" } );
            }.bind(this))
        }
    },

	getInitialState: function() {
		var favorited = this.props.target.favorited || false;
        return {favorited: favorited, count: this.props.count };
    },

    render: function() {
    	
        var classname = this.state.favorited ? "favorited mm-tooltip" : "mm-tooltip";
        return <div>
                    <div 
                        className={ classname }
                        onClick={this._toggleFav}
                        data-favorited={ this.state.favorited }
                        data-desc={ MMCafeReact.Components.Translator.getI18n('favorite.tooltip') }
                    >
        	        </div>
                    <span>{ this.state.count }</span>
                </div>;
    }

});

module.exports = Favorite;