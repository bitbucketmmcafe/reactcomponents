var CalendarWeek = React.createClass({

    selectDate: function(date) {
        
        var str = date.date.date().toString() + "_" + date.date.month();
        if($(".mm-day-" + str).hasClass("hasevent")) {
            this.props.selectDate(date);
        }
        if($(".mm-day-" + str).hasClass("clickable")) {
            PubSub.publish(this.props.pubsub.dayclicked, {date: date});
        }

    },

    render: function() {
        var days = [],
            date = this.props.date,
            month = this.props.month;

           date.utc();
        for (var i = 0; i < 7; i++) {

            var day = {
                name: date.format("dd").substring(0, 1),
                number: date.date(),
                isCurrentMonth: date.month() === month.month(),
                isToday: date.isSame(new Date(), "day"),
                date: date
            };
            var str = " mm-day-" + day.number.toString() + "_" + date.month().toString();

            days.push(<span
                        key={day.date.toString()}
                        className={"mm-day" + (day.isToday ? " mm-today" : "") + 
                                    (day.isCurrentMonth ? "" : " mm-different-month") + 
                                    //(day.date.isSame(this.props.selected) ? " selected" : "") +
                                    str
                                  }
                        onClick={this.selectDate.bind(null, day)}>
                            {day.number}
                       </span>);
            date = date.clone();
            date.add(1, "d");

        }

        return <div className="week" key={days[0].toString()}>
            {days}
        </div>
    }
});

/**
 * Component para calendário por mês, navegável
 */
var Calendar = React.createClass({
    
    propTypes: {
        /**
         * Mês e ano selecionados/atual: selected = "09/2015"
         */
        selected: React.PropTypes.object
    },

    
    getInitialState: function() {
        return {
           month: this.props.selected.clone()
        };
    },

    /**
     *
     */
    previous: function() {
       
        var month = this.state.month;
        month.add(-1, "M");

        this.setState({ month: month });
        if(this.props.handleChangeCalendar)
            this.props.handleChangeCalendar(month);

    },

    next: function() {
        var month = this.state.month;
        month.add(1, "M");
        this.setState({ month: month });
        if(this.props.handleChangeCalendar)
            this.props.handleChangeCalendar(month);
    },

    select: function(day) {
        this.props.selected = day.date;
        this.forceUpdate();
    },

    renderWeeks: function() {
        
        var weeks = [],
            done = false,
            date = this.state.month.clone().startOf("month").day("Sunday"),
            monthIndex = date.month(),
            count = 0;


        if(date.date() == 2) {
        	date = date.date(1);
        }

        while (!done) {
            weeks.push(<CalendarWeek
                        key={"week_" + count}
                        date={date.clone()}
                        month={this.state.month}
                        select={this.select}
                        dayswithevents={ this.state.dayswithevents }
                        pubsub = { this.props.pubsub }
                        selectDate={this.props.selectDate} />);
            date.add(1, "w");
            done = count++ > 2 && monthIndex !== date.month();
            monthIndex = date.month();
        }

        return weeks;
    },

    renderMonthLabel: function() {
        return <span>{this.state.month.format("MMMM YYYY")}</span>;
    },

    componentDidMount: function() {


        var self = this, node = $(this.getDOMNode());
        //PubSub.subscribe( 'widget.calendar.dayswithevents', function(msg, data) {
        PubSub.subscribe( this.props.subscribedays, function(msg, data) {
            var month = (self.state.month.month() - 1);

            if(data.days) {
                data.days.forEach(function(day) {
                    node.find(".mm-day-" + day + "_" + (month + 1)).addClass("hasevent");
                }); 
            }
        });

        if(this.props.pubsub && this.props.pubsub.overriderule) {
            PubSub.subscribe( this.props.pubsub.overriderule, function(msg, dates) {
                var month = (self.state.month.month() - 1);
                var days = moment(self.state.month).endOf("month").daysInMonth();

                for(var i = 1; i<= days; i++) {

                    node.find(".mm-day-" + i + "_" + (month + 1)).addClass("clickable");
                }

                
            });
        }
    },


    componentDidUpdate: function() {

    	var self = this, node = $(this.getDOMNode()), month = this.state.month.month();

        /*
    	if(this.state.dayswithevents) {
     		this.state.dayswithevents.forEach(function(day) {
                node.find(".mm-day-" + day + "_" + month).addClass("hasevent");
    		});	
    	}
        */
    	node.find(".mm-day.selected").removeClass("selected");


    	if(this.state.selected_date){
	    	node.find(".mm-day-" + this.state.selected_date.date() + "_" + this.state.selected_date.month()).addClass("selected");           
        }


        if(this.props.pubsub && this.props.pubsub.overriderule) {

                var month = (self.state.month.month() - 1);
                var days = moment(self.state.month).endOf("month").daysInMonth();
                for(var i = 1; i<= days; i++) {

                    node.find(".mm-day-" + i + "_" + (month + 1)).addClass("clickable");
                }

        }
    },

    render: function() {

        var days = moment.weekdaysMin(), d = days[0];
        days.push(d);
        //delete days[0];
        days.pop();
        var header = [];
        days.forEach(function(day, index) {
            header.push (<span className="mm-day" key={ "day_" + index } >{ day }</span>);
        });

        var weeks = this.renderWeeks();

        return <div className="mm-calendar">
            <div className="mm-calendar-header">
                <a onClick={this.previous} className="mm-calendar-previous"></a>
                {this.renderMonthLabel()}
                <a onClick={this.next} className="mm-calendar-next"></a>
            </div>
            <div className="mm-calendar-week">
               { header }
            </div>
           { weeks }
        </div>;
    }
});

module.exports = Calendar;

