/**
 * Component para dar rate em POs, exemplo em http://jsfiddle.net/chrisbenseler/hdnmgybh/
 */
var Rating = React.createClass({

	pubSub: PubSub,

	propTypes: {
		/**
		 * Object target = { id: 1 }
		 */
		target: React.PropTypes.object,

		/**
		 * 
		 */
		source: React.PropTypes.object,

		/**
		 * Amount of buttons to rate (1..size)
		 */
		size: React.PropTypes.number,

		/**
		 * Current rating average (should be an integer between 0 and this.props.size)
		 */
		currentrating: React.PropTypes.number
	},

   
    getInitialState: function() {
    
    	if(window && window.localStorage) {
        	var hasrated = localStorage.getItem("rateobject_" + this.props.target.id);
            return {hasrated: hasrated || false };
        } else {
        	return {hasrated: false };
        }
        
    },

    _build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },
    
    _rate: function(index) {
    	var self = this;
    	if(this.state.hasrated) {

    	} else {

    		var url = this.props.source.rate({id: this.props.target.id, rate: index });
    		self.pubSub.publish("components.loader.add", { key: "po.rating" } );
    		var builded_data = this._build_data(url)
			$
	        .ajax({url: builded_data.url, data: builded_data.data, method: 'post' })
	        .then(function() {
	        	self.setState({hasrated: true, rated: index});
	            if(window && window.localStorage) {
	            	localStorage.setItem("rateobject_" + self.props.target.id, true);
	            	localStorage.setItem("ratingobject_" + self.props.target.id, index);
	            }
	            PubSub.publishSync("components.rating.rated", { rate: index, id: self.props.target.id });
	            if(PubSub)
		            PubSub.publishSync( 'mm.rating.rated', { target: self.props.target } );
	        })
	        .done(function() {
	        	self.pubSub.publish("components.loader.remove", { key: "po.rating" } );
	        })
    	}
        
    },

    render: function() {
    
    	var classname = this.state.hasrated ? " mm-rating-disabled" : " ";
	    
		
        var nodes = [];
		for(var i=1; i<=this.props.size; i++) {

			if(this.state.rated)
				var btnclass = i <= this.state.rated ? "mm-rating-btn-fill" : "mm-rating-btn-unfill"; 
			else
	            var btnclass = i <= this.props.currentrating ? "mm-rating-btn-fill" : "mm-rating-btn-unfill"; 
            nodes.push(<button
            				title={ MMCafeReact.Components.Translator.getI18n("rating.level." + i) }
            				className={ "mm-rating-btn " + btnclass + " mm-rating-btn_" + i }
            				onClick={ this._rate.bind(null, i) }
            				key={ "ratingitem_" + i} >
                        </button>)
        }
        return (
            <div className={"mm-rating" + classname }>
                { nodes }
            </div>
        );
    }
});

module.exports = Rating;