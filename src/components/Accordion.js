
/**
 * Componente para accordion
 */
var Accordion = React.createClass({

    propTypes: {
        /**
         * Array com items do accordion, cada lista é um json: data = [ { label:"nome do accordeon", title:"título dentro do accordeon", external: false, thumb: "http://urldeimagem", description: "texto adicional", date: "2015-09-01T00:00:00Z", "info":array } ]
         * O array info espera o seguinte json {name:'titulo', date:'data', link:'url', description:'texto descritivo', observation:'texto', publishedBy:'publicador/organizador', guests:'convidados/texto', external: 'boolean para link externo ou interno' }
         */
        items: React.PropTypes.array,
        /**
         * id para identificar o accordion
         */
        id: React.PropTypes.string
    },


    
	render: function() {

        var heading = this.props.items.map(function(item, index) {
            return  <div className="panel-heading">
                        <h4 className="panel-title">
                            <a data-toggle="collapse" data-parent={"#list_"+item.parent_id} data-target={ "#collapse_" + item.parent_id+"_"+item.key } dangerouslySetInnerHTML={{ __html: item.label  }} className="collapsed"></a>
                        </h4>
                    </div>
        });

        var items = this.props.items.map(function(item, index) {

            var span = null, thumb = null, datelabel = null, bodyItems = null, title = null, target = item.external ? "_blank" : "";
            if(item.description)
                span = <span dangerouslySetInnerHTML={{ __html: item.description  }}></span>;

            if(item.thumb)
                thumb = <img src={ item.thumb } />;

            if(item.date) {
                datelabel = <MMCafeReact.Components.DateLabel iso8061date={ item.date } pattern={ item.date_pattern || "DD/MM/YYYY" } />
            }

            if(item.title) {
                title = <a href={ bodyItem.link } target={ target } dangerouslySetInnerHTML={{ __html: item.title  }} title={ item.title }></a>;
            }

            if(item.info){
                
                bodyItems = item.info.map(function(bodyItem, b_index) {
                    var info_date = null, info_link = null, info_description = null, info_observation = null, info_publishedBy = null, info_guests = null,  target = bodyItem.external ? "_blank" : "";
                    
                    if(bodyItem.date)
                        info_date = <p dangerouslySetInnerHTML={{ __html: bodyItem.date  }}></p>;
                    
                    if(bodyItem.link)
                        info_link = <a href={ bodyItem.link } target={ target } dangerouslySetInnerHTML={{ __html: bodyItem.name  }} title={ bodyItem.name }></a>;
                    
                    if(bodyItem.description)
                        info_description = <p className="description" dangerouslySetInnerHTML={{ __html: bodyItem.description  }}></p>;
                    
                    if(bodyItem.observation)
                        info_description = <p className="observation" dangerouslySetInnerHTML={{ __html: bodyItem.observation  }}></p>;
                    
                    if(bodyItem.publishedBy && bodyItem.publishedBy.indexOf("null") <= -1)
                        info_publishedBy = <p className="publishedBy" dangerouslySetInnerHTML={{ __html: bodyItem.publishedBy  }}></p>;
                    
                    if(bodyItem.guests && bodyItem.guests.indexOf("null") <= -1)
                        info_guests = <p className="guests" dangerouslySetInnerHTML={{ __html: bodyItem.guests  }}></p>;
                                        
                    return <div key={ "info_" + b_index }>
                                {info_date}
                                {info_link}
                                {info_publishedBy}
                                {info_description}
                                {info_guests}
                                {info_observation}
                            </div>;
                });
            }

        	
            return <div key={ "item_" + index } id={"collapse_" + item.parent_id+"_"+item.key }  className="panel-collapse collapse ">
                        <div className="panel-body">
                            { bodyItems==null &&
                                <div>
                                    { thumb }
                                    { datelabel }
                                    { title }
                                    { span }
                                </div>
                            }
                            {bodyItems}
                        </div>
                    </div>
        });
        return <div className="panel panel-default">
                {heading}
                {items}
                </div>;

	}

});

module.exports = Accordion;
