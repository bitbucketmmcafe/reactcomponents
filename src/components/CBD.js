/**
 * Component para selects aninhados de Company/Branch/Department
 */
var CBD = React.createClass({
    _firstrender : true,

	query_params: {
        companyId: null,
        branchId: null,
        departmentId: null,
    },

	propTypes: {
		/**
		 * Object with sources = { source: { branchesbycompany: "someurl", companies: "otherurl", departmentsbybranch: "anotherurl" } }
		 */
		data: React.PropTypes.object
	},

	getCompanies: function() {

        var self = this;
        this.query_params.companyId = self.props.data.company

        $
        .ajax({ url: self.props.data.source.companies })
        .then(function(response) {
            self.state.branches = [];
            self.state.departments = [];

            self.setState({ companies: response });

            if(response.length == 1) {
                var node = React.findDOMNode(self.refs.companyselect);
                $(node).prop('selectedIndex', 1);
                var event = document.createEvent('Event');
                event.initEvent('change', true, true);
                node.dispatchEvent(event);
            } 
            else if(self.props.data.company) {
                var node = React.findDOMNode(self.refs.companyselect);
                
            }
            else if(self._firstrender && self.props.data.initializers && self.props.data.initializers.company) {
                var node = React.findDOMNode(self.refs.companyselect), usercompany = self.props.data.initializers.company;


                $(node).find("option").each(function(index, item) {
                    if($(item).val() == usercompany.id) {
                        $(node).prop('selectedIndex', index);
                        var event = document.createEvent('Event');
                        event.initEvent('change', true, true);
                        node.dispatchEvent(event);
                    }
                })
            }
        });

    },

    getBranches: function(event) {
        var self = this;

        this.query_params.companyId = event.target.value;
        this.query_params.branchId = null;
        this.query_params.departmentId = null;

        $
        .ajax({ url: self.props.data.source.branchesbycompany + "companyId=" + event.target.value})
        .then(function(response) {
            self.setState({ branches: response, departments: [] });

            if(response.length == 1) {
                var node = React.findDOMNode(self.refs.branchselect);
                $(node).prop('selectedIndex', 1);
                //var event = new Event('change', { bubbles: true });
                //node.dispatchEvent(event);
                var event = document.createEvent('Event');
                event.initEvent('change', true, true);
                node.dispatchEvent(event);
            } else if(self._firstrender && self.props.data.initializers && self.props.data.initializers.branch) {
                var node = React.findDOMNode(self.refs.branchselect), userbranch = self.props.data.initializers.branch;

                $(node).find("option").each(function(index, item) {
                    if($(item).val() == userbranch.id) {
                        $(node).prop('selectedIndex', index);
                        var event = document.createEvent('Event');
                        event.initEvent('change', true, true);
                        node.dispatchEvent(event);
                    }
                })
            }
            

        });
    },

    getDepartments: function(event) {

        var self = this;

        this.query_params.branchId = event.target.value;
        this.query_params.departmentId = null;

        $
        .ajax({ url: this.props.data.source.departmentsbybranch + "branchId=" + event.target.value})
        .then(function(response) {

            self.setState({ departments: response });

            if(response.length == 1) {
                var node = React.findDOMNode(self.refs.departmentselect);
                $(node).prop('selectedIndex', 1);
                //var event = new Event('change', { bubbles: true });
                //node.dispatchEvent(event);
                var event = document.createEvent('Event');
                event.initEvent('change', true, true);
                node.dispatchEvent(event);
            } else if(self._firstrender && self.props.data.initializers && self.props.data.initializers.department) {
                
                var node = React.findDOMNode(self.refs.departmentselect), userdepartment = self.props.data.initializers.department;
                $(node).find("option").each(function(index, item) {
                    if($(item).val() == userdepartment.id) {
                        $(node).prop('selectedIndex', index);
                    }
                });

                self._firstrender = false;
            }

        });
    },

    departmentSelected: function(event) {
        this.query_params.departmentId = event.target.value;
    },

    sourceParams: function() {
        return this.query_params;
    },

    componentWillMount: function() {
    	this.getCompanies();
    },

	getInitialState: function() {
		return {data: this.props.data, companies: null, branches: null, departments: null };
    },

    render: function() {

    	var data = {}, companies = [], branches = [], departments = [];
        
        if(this.state && this.state.companies) {
            companies = this.state.companies.map(function(company) {
                return <option value={ company.id } key={ "company_" + company.id }>{ company.name }</option>;
            });

        }

        if(this.state && this.state.branches) {
            branches = this.state.branches.map(function(branch) {
                return <option value={ branch.id } key={ "branch_" + branch.id }>{ branch.name }</option>;
            });
        }

        if(this.state && this.state.departments) {
            departments = this.state.departments.map(function(department) {
                return <option value={ department.id } key={ "department_" + department.id }>{ department.name }</option>;
            });
        }
    	
        return <div>
                    { companies.length > 0 && 
	        		<div className={ companies.length == 1 ? "hidden": "" }>
	                <label><MMCafeReact.Components.Translator value={ "cbd.company" } /></label>
	                <select name="companyId" ref="companyselect" onChange={ this.getBranches }>
	                    <option></option>
	                    { companies }
	                </select>
	                </div>
                    }
	                
                    { branches.length > 0 && 
	                <div className={ branches.length == 1 ? "hidden": "" }>
	                <label><MMCafeReact.Components.Translator value={ "cbd.branch" } /></label>
	                <select name="branchId" ref="branchselect" onChange={ this.getDepartments }>
	                    <option></option>
	                    { branches }
	                </select>
	                </div>
	                }

	                { departments.length > 0 &&
	                <div className={ departments.length == 1 ? "hidden": "" }>
	                <label><MMCafeReact.Components.Translator value={ "cbd.department" } /></label>
	                <select name="departmentId" ref="departmentselect" onChange={ this.departmentSelected }>
	                    <option></option>
	                    { departments }
	                </select>
	                </div>
	                }
        </div>;
    }

});

module.exports = CBD;
