
/**
 * Component para lista
 */
var List = React.createClass({

    propTypes: {
        /**
         * Array com items da lista, cada lista é um json: data = [ { text: "texto do link" }, external: false, thumb: "http://urldeimagem", description: "texto adicional", "date": "2015-09-01T00:00:00Z" } ]
         */
        items: React.PropTypes.array,
        /**
         * classe css: classname = "col-md-3" 
         */
        classname: React.PropTypes.string
    },

	render: function() {

        var items = this.props.items.map(function(item, index) {
            var span = null, thumb = null, datelabel= null;

            if(item.description)
                span = <span dangerouslySetInnerHTML={{ __html: item.description  }}></span>;

            if(item.thumb){
                thumb = <img src={ item.thumb } />;
            }else{
                if(item.image && item.image.url)
                    thumb = <img src={ item.image.url } />;
            }


            if(item.date) {
                datelabel = <MMCafeReact.Components.DateLabel iso8061date={ item.date } pattern={ item.date_pattern || "DD/MM/YYYY" } />
            }

        	if(item.link) {
            	var target = item.external ? "_blank" : "";
            	return <li key={ "item_" + index }>{ thumb }
                        { datelabel }
                        <a href={ item.link } target={ target } dangerouslySetInnerHTML={{ __html: item.text  }} title={ item.text }></a>
                        { span }</li>
            }            
            else
	        	return <li key={ "item_" + index }>{ thumb }
                    { datelabel }
                    <span dangerouslySetInnerHTML={{ __html: item.text  }}></span>
                    { span }
                </li>
        });
        return <ul className={ this.props.classname }>
        	{ items }
        </ul>;
	}

});

module.exports = List;
