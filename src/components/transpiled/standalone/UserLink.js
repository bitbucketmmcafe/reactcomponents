/**
 * Component para link de usuário
 */
window.UserLink = React.createClass({

    propTypes: {
        /**
         * Usuário, é um json
         */
        user: React.PropTypes.object,
        /**
         * classe css: classname = "col-md-3" 
         */
        classname: React.PropTypes.string
    },

    render: function() {
        var month = (new Date()).getMonth()+1, day = (new Date()).getDate(), classname = this.props.classname || "mm-link-user";

        if(this.props.user.birthdate) {
            var nodes = this.props.user.birthdate.split("/").map(function(item) {
                return parseInt(item);
            });

            if(nodes[0] == day && nodes[1] == month) {
                classname += " mm-user-birthday";
            }
        }

        //var str = this.props.user.fullname;
        //if(this.props.user.nickname)
        //    str += "&nbsp;<span>(" + this.props.user.nickname + ")</span>";

        var data = {name: this.props.user.preferedname, url: "/m/usuario/" + this.props.user.id};
        return <MMCafeReact.Components.Link data={ data } classname={ classname } />;
    }

});


