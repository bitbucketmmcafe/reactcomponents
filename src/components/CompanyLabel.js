/**
 * Component para companhia do usuário
 */
var CompanyLabel = React.createClass({

	propTypes: {
		/**
		 * Dados da company: data = { name: "text for link", id: 1 }
		 */
		data: React.PropTypes.object,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string
	},

    render: function() {
    	if(this.props.data && this.props.data.name)
	        return <span className={ this.props.classname || "mm-label-company" }>{ this.props.data.name }</span>;
    	else
    		return <span></span>;
    }

});

module.exports = CompanyLabel;
