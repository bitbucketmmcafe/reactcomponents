/**
 * Component para adicionar/remover usuário como contato
 */
var ContactUser = React.createClass({

	_isSubmitting: false,

	propTypes: {
		/**
		 * Object target = { id: 1, status: false }
		 */
		target: React.PropTypes.object
	},

	_build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },

    
    _toggleContact: function() {

    	if(!this._isSubmitting) {
    	
	    	this._isSubmitting = true;

	    	var self = this;
	        var url = this.props.source.contactuser({id: this.props.target.id, status: !this.state.contacted, authenticity: this.props.target.authenticity});

	        var str = this.state.contacted ? "remove" : "add";
	        window.PubSub.publish("components.loader.add", { key: "mm.contactuser.toggle." + str, type: "loading"});

	        var builded_data = this._build_data(url)
			$
	        .ajax({url: builded_data.url, data: builded_data.data, method: 'post' })
	        .then(function(responseText) {
	        	self.setState({contacted: !self.state.contacted});
	        	self._isSubmitting = false;
	        	window.PubSub.publish("components.loader.remove", { key: "mm.contactuser.toggle." + str, type: "success"});
	        })
	        .fail(function(jqXHR) {
	        	self._isSubmitting = false;
	        	if(window.PubSub)
				//window.PubSub.publishSync( 'mm.contactuser.error', { error: jqXHR } );
		        	window.PubSub.publish("components.loader.remove", { key: "mm.contactuser.toggle." + str, type: "error", error: jqXHR});
	        });
    	}
    },

	getInitialState: function() {
		var contacted = this.props.target.contacted || false;
        return {contacted: contacted };
    },

    render: function() {
    	
        var classname = this.state.contacted ? "contacted" : "";
        
        return <div className={ classname } onClick={this._toggleContact} data-contacted={ this.state.contacted }>
        	
        </div>;
    }

});

module.exports = ContactUser;
