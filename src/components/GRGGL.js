/**
 * Component para selects de GRs e GGLs.
 */
var GRGGL = React.createClass({

    _firstrender: true,
    _isDashboard: true,

    query_params: {
        grName: null,
        gglName: null,
        storeName: null,
        taskId: null
    },

    propTypes: {
        /**
         * Object with sources = { source: { listGgls: "someurl", listGrs: "otherurl", listStores: "anotherurl", listTasks: "onemoreurl" } }
         */
        data: React.PropTypes.object
    },

    getGRs: function () {

        var self = this;

        $.ajax({url: self.props.data.source.listGrs})
            .then(function (response) {

                self.state.ggls = [];

                self.setState({grs: response});

                if (response.length === 1) {
                    var node = React.findDOMNode(self.refs.grselect);
                    $(node).prop('selectedIndex', 1);
                    var event = document.createEvent('Event');
                    event.initEvent('change', true, true);
                    node.dispatchEvent(event);
                } else if (self._firstrender && self.props.data.initializers && self.props.data.initializers.gr) {
                    var node = React.findDOMNode(self.refs.grselect), usergr = self.props.data.initializers.gr;

                    $(node).find("option").each(function (index, item) {
                        if ($(item).val() === usergr.id) {
                            $(node).prop('selectedIndex', index);
                            var event = document.createEvent('Event');
                            event.initEvent('change', true, true);
                            node.dispatchEvent(event);
                        }
                    })
                }
            });
    },

    getGGLs: function (event) {
        var self = this;
        if (event.target) {
            this.query_params.grName = event.target.value;
        } else {
            this.query_params.grName = event.name;
        }
        this.query_params.gglName = null;
        this.query_params.store = null;

        if (this.query_params.grName === "") {
            self.setState({ggls: null});
            self.setState({stores: null});
            self.setState({tasks: null});
        }


        $.ajax({
            url: self.props.data.source.listGgls,
            data: {
                gr: this.query_params.grName
            }
        })
            .then(function (response) {

                self.setState({ggls: response});

                if (response.length === 1) {
                    var node = React.findDOMNode(self.refs.gglselect);
                    $(node).prop('selectedIndex', 1);
                    var event = document.createEvent('Event');
                    event.initEvent('change', true, true);
                    node.dispatchEvent(event);
                } else if (self._firstrender && self.props.data.initializers && self.props.data.initializers.ggl) {
                    var node = React.findDOMNode(self.refs.gglselect), userggl = self.props.data.initializers.ggl;

                    $(node).find("option").each(function (index, item) {
                        if ($(item).val() === userggl.id) {
                            $(node).prop('selectedIndex', index);
                            var event = document.createEvent('Event');
                            event.initEvent('change', true, true);
                            node.dispatchEvent(event);
                        }
                    })
                }
            });
    },

    getStores: function (event) {

        var self = this;

        if (event.target) {
            this.query_params.gglName = event.target.value;
        } else {
            this.query_params.gglName = event.name;
            this.query_params.grName = null
        }
        this.query_params.store = null;

        if (this.query_params.gglName === "") {
            self.setState({stores: null});
            self.setState({tasks: null});
        }


        self.props.handleQueryParams(this.query_params);

        $.ajax({
            url: self.props.data.source.listStores,
            data: {
                gr: this.query_params.grName,
                ggl: this.query_params.gglName
            }
        })
            .then(function (response) {

                self.setState({stores: response});

                if (response.length === 1) {
                    var node = React.findDOMNode(self.refs.storeselect);
                    $(node).prop('selectedIndex', 1);
                    var event = document.createEvent('Event');
                    event.initEvent('change', true, true);
                    node.dispatchEvent(event);
                } else if (self._firstrender && self.props.data.initializers && self.props.data.initializers.store) {
                    var node = React.findDOMNode(self.refs.storeselect), userstore = self.props.data.initializers.store;

                    $(node).find("option").each(function (index, item) {
                        if ($(item).val() === userstore.id) {
                            $(node).prop('selectedIndex', index);
                            var event = document.createEvent('Event');
                            event.initEvent('change', true, true);
                            node.dispatchEvent(event);
                        }
                    })
                }
            });
    },


    getTasks: function (event) {
        var self = this;

        if (event.target) {
            this.query_params.store = event.target.value;
        } else {
            this.query_params.store = event.name;
            this.query_params.grName = null
            this.query_params.gglName = null
        }
        this.query_params.taskId = null;

        if (this.query_params.store === "") {
            self.setState({tasks: null});
        }

        $.ajax({
            url: self.props.data.source.listTasks,
            data: {
                gr: this.query_params.grName,
                ggl: this.query_params.gglName,
                storeId: this.query_params.store
            }
        })
            .then(function (response) {

                self.setState({tasks: response});

                if (response.length === 1) {
                    var node = React.findDOMNode(self.refs.tasksselect);
                    $(node).prop('selectedIndex', 1);
                    var event = document.createEvent('Event');
                    event.initEvent('change', true, true);
                    node.dispatchEvent(event);
                } else if (self._firstrender && self.props.data.initializers && self.props.data.initializers.task) {
                    var node = React.findDOMNode(self.refs.storeselect), usertask = self.props.data.initializers.task;

                    $(node).find("option").each(function (index, item) {
                        if ($(item).val() === userstore.id) {
                            $(node).prop('selectedIndex', index);
                            var event = document.createEvent('Event');
                            event.initEvent('change', true, true);
                            node.dispatchEvent(event);
                        }
                    })
                }
            });
    },

    gglSelected: function (event) {
        this.query_params.gglId = event.target.value;
    },

    storeSelected: function (event) {
        this.query_params.store = event.target.value;
    },

    taskSelected: function (event) {
        this.query_params.taskId = event.target.value;
    },

    sourceParams: function () {
        return this.query_params;
    },

    componentWillMount: function () {
        if (this.state.data.source.isGGL) {
            this.getStores(this.state.data.source.loggedUser)
        } else if (this.state.data.source.isGr) {
            this.getGGLs(this.state.data.source.loggedUser)
        } else {
            this.getGRs();
        }
    },

    getInitialState: function () {
        return {data: this.props.data, grs: null, ggls: null, stores: null, tasks: null};
    },

    render: function () {

        var data = {}, listGrs = [], listGgls = [], listStores = [], listTasks = [];

        if (this.state && this.state.grs) {
            listGrs = this.state.grs.map(function (gr) {
                return <option value={gr.name} key={"gr_" + gr.id}>{gr.fullname}</option>;
            });
        }

        if (this.state && this.state.ggls) {
            listGgls = this.state.ggls.map(function (ggl) {
                return <option value={ggl.name} key={"ggl_" + ggl.id}>{ggl.fullname}</option>;
            });
        }

        if (this.state && this.state.stores) {
            listStores = this.state.stores.map(function (store) {
                return <option value={store.id} key={"store_" + store.id}>{store.fullname}</option>;
            });
        }

        if (this.state && this.state.tasks) {
            listTasks = this.state.tasks.map(function (task, index) {
                return <option value={task.name} key={"task_" + index}>{task.name}</option>;
            });
        }

        return <div>
            {listGrs.length > 0 && (this.state.data.source.isGr || this.state.data.source.loggedUser.admin) &&
            <div className={listGrs.length === 1 ? "hidden" : ""}>
                <label><MMCafeReact.Components.Translator value={"dashboard.gr.label"}/></label>
                <select name="gr" ref="grselect" onChange={this.getGGLs}>
                    <option value=""></option>
                    {listGrs}
                </select>
            </div>
            }

            {listGgls.length > 0 && (this.state.data.source.isGGL || this.state.data.source.isGr || this.state.data.source.loggedUser.admin) &&
            <div className={listGgls.length === 1 ? "hidden" : ""}>
                <label><MMCafeReact.Components.Translator value={"dashboard.ggl.label"}/></label>
                <select name="ggl" ref="gglselect" onChange={this.getStores}>
                    <option value=""></option>
                    {listGgls}
                </select>
            </div>
            }

            {listStores.length > 0 && !this.state.data.source.isDashboard && (this.state.data.source.isGGL || this.state.data.source.isGr || this.state.data.source.loggedUser.admin) &&
            <div className={listStores.length === 1 ? "hidden" : ""}>
                <label><MMCafeReact.Components.Translator value={"dashboard.store.label"}/></label>
                <select name="storeId" ref="storeselect" onChange={this.getTasks}>
                    <option></option>
                    {listStores}
                </select>
            </div>
            }

            {listTasks.length > 0 && !this.state.data.source.isDashboard && (this.state.data.source.isGGL || this.state.data.source.isGr || this.state.data.source.loggedUser.name === 'admin') &&
            <div className={listTasks.length === 1 ? "hidden" : ""}>
                <label><MMCafeReact.Components.Translator value={"dashboard.task.label"}/></label>
                <select name="task" ref="taskselect">
                    <option></option>
                    {listTasks}
                </select>
            </div>
            }

        </div>;
    }

});

module.exports = GRGGL;
