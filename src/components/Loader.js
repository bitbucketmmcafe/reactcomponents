var Loader = React.createClass({

	componentDidMount: function() {

		var self = this;
 		PubSub.subscribe("components.loader.add", function(msg, data) {
 			var msgs = this.state.messages;
 			msgs[data.key] = { key: data.key, type: data.type ? data.type : "loading" };
      this.setState({ messages: msgs });
 		}.bind(this));

 		PubSub.subscribe("components.loader.remove", function(msg,  data) {
 			var msgs = this.state.messages;
      this.setState({ messages: msgs });
 			if(data.type) {
        delete msgs[data.key];
        msgs[data.key + "." + data.type] = { key: data.key + "." + data.type, type: data.type };
        this.setState({ messages: msgs });
        var timeout = data.type == "success" ? 2000 : 4000;

        setTimeout(function() {
          delete msgs[data.key + "." + data.type];
          this.setState({ messages: msgs });
        }.bind(this), timeout);

        if(data.type == "error") {
          console.error("Error on", data.key, data.error);
        }

      } else {
        delete msgs[data.key];
        this.setState({ messages: msgs });
      }
 		}.bind(this));
  },

	getInitialState: function() {
    	var msg = { };
    	return { messages: msg };
    },
    
    render: function() {
  		var messages = this.state.messages,
          nodes = [];
  		for(key in messages) {
              
         	nodes.push(<li key={ key } className={ "mmloader-item-" + messages[key].type }>
         			<MMCafeReact.Components.Translator key={ "translateloading_" + key } value={ "loading." + key } />
         			</li>)
    }

    if(nodes.length > 0) {
    return <ul>
       	{ nodes }
        </ul>;
    } else { 
      return <span></span>;
    }
  }
});

module.exports = Loader;