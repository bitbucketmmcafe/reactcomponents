/**
 * Component para branch do usuário
 */
var BranchLabel = React.createClass({

	propTypes: {
		/**
		 * Dados da company: data = { name: "text for link", id: 1 }
		 */
		data: React.PropTypes.object,
		/**
		 * classe css: classname = "col-md-3" 
		 */
		classname: React.PropTypes.string
	},

    render: function() {
        return <span className={ this.props.classname }>{ this.props.data.name }</span>;
    }

});

module.exports = BranchLabel;
