var Likeable = React.createClass({

    _isSubmitting: false,
    pubSub: PubSub,

	propTypes: {
		/**
		 * Object target = { id: 1, likeable: false }
		 */
		target: React.PropTypes.object
	},

    _toggleLike: function() {

        if(!this._isSubmitting) {
        
            this._isSubmitting = true;
            
            var method = this.state.likeId?  "removeLike" : "like"

            if(this.state.liked)
                this.pubSub.publish("components.loader.add", { key: "po.likeable.remove" } );
            else
                this.pubSub.publish("components.loader.add", { key: "po.likeable.add" } );

    		$
            .ajax({
                url: "/ajax/likeable/user.view",
                data:{method: method, objectId: this.props.target.id, objectType: this.props.type, likeId: this.state.likeId}
            })
            .then(function(response) {
                this.setState({liked: !this.state.liked, count: response.likes, likeId: response.likeId});
                this._isSubmitting = false;
             }.bind(this))
            .fail(function(jqXHR) {
                this.pubSub.publish("components.loader.remove", { key: "po.likeable.remove", type: "error", error: jqXHR } );
                this.pubSub.publishSync( 'mm.likeable.error', { error: jqXHR } );
                this._isSubmitting = false;
            }.bind(this))
            .done(function() {
                if(!this.state.liked)
                    this.pubSub.publish("components.loader.remove", { key: "po.likeable.remove" } );
                else
                    this.pubSub.publish("components.loader.remove", { key: "po.likeable.add" } );
            }.bind(this))
        }
    },

	getInitialState: function() {
		var liked = this.props.target.likeable || false ;
        return {liked: liked, count: this.props.count, likeId: this.props.target.likeId};
    },

    render: function() {
    	
        var classname = this.state.liked ? "liked " : "";
        var qtlikes = this.state.count > 0 && !this.state.liked ? "muchlikes" : "";
        return (<div className={"like-container " + qtlikes}>
                    {this.props.viewOnly==true &&
                        <div className={"ico-like " + classname } data-liked={ this.state.liked } data-liked={this.state.likeId}></div>
                    }
                    {!this.props.viewOnly &&
                        <div className={ "ico-like " + classname } onClick={this._toggleLike} data-liked={ this.state.liked } data-liked={this.state.likeId}></div>
                    }
                        <span>{ this.state.count }</span>
                </div>);
    }

});

module.exports = Likeable;