/**
 * Component para autocomplete de usuário
 */

var AutocompleteUser = React.createClass({

	propTypes: {
		
		//source: React.PropTypes.object,
		
	},

    _randString: function(x){
        var s = "";
        while(s.length<x&&x>0){
            var r = Math.random();
            s+= (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
        }
        return s;
    },

    innerId: null,

	timeout: null,

	_keyUp: function(event) {
        if(this.props.disableautocomplete)
            return false;

    	var value = event.target.value;

        var input = this.refs.input.getDOMNode();
        console.log("1 - "+ input)

    	var self = this;
        var userClick = this._userClick;

        PubSub.publish("components.loader.add", { key: "componente.autocompleteuser.search" } );

        if(this.props.pubsub && this.props.pubsub)
            PubSub.publishSync( this.props.pubsub.keyupinput, { value: value } );
            console.log("2 - "+ key)

    	if(value == "") {
    		clearTimeout(this.timeout);
    		this.setState({ suggestions: [] });
    		event.stopPropagation();
    	}

    	clearTimeout(this.timeout);

    	this.timeout = setTimeout(function() {
            var query = "";

            if(self.props.contextualSearch && self.props.business_fields==false){
              query += "pageSize=3&search=true&autocomplete=true&keywords=" + value + "&searchFieldsUser=" + self.props.sourceParams2;
                console.log("3 -" + query)
                console.log("4 -" + value)
              
            }else{
                
                var pars = self.props.sourceParams();

                for(key in pars) {
                    if(pars[key]!=null)
                        query+= key + "=" + pars[key] + "&"                        
                }

                if(self.props.contextualSearch && self.props.business_fields==true){
                    query += "pageSize=3&search=true&autocomplete=true&keywords=" + value + "&searchFieldsUser=" + self.props.sourceParams2;
                    
                }else{
                    query += "keywords=" + value  + "&searchFieldsUser=" + self.props.sourceParams2;
                  
                }

            }

    		$
	    	.ajax({ url: self.props.source + query })
	    	.then(function(response) {
                $("#mm-autocomplete-clone")
                .css({
                    top: $(input).offset().top + $(input).outerHeight(),
                    left: $(input).offset().left,
                    width: $(input).outerWidth()
                });


                var suggestions = response.map(function(s) {

                    console.log(s);

                    return <li key={ "suggestion_" + s.id } data-userid={ s.id } onClick={ userClick.bind(null, s)  }>
                        <img src={ s.avatar } />
                            <span className="mm-username">{ s.preferedname }</span>
                            { s.phone && 
                            <span>
                                <MMCafeReact.Components.Translator value={ "component.autocomplete.phone" } />: <span className="mm-phone">{ s.areaPrefix && (s.areaPrefix) } { s.phone }</span>
                            </span>
                            }
                            { s.extension &&
                            <span>
                                <MMCafeReact.Components.Translator value={ "component.autocomplete.extension" } />: <span className="mm-phone-extension">{ s.extension }</span>
                            </span>
                        }
                    </li>;
                });

                var root = <ul>
                     { suggestions }
                   </ul>;

                React.render(root, document.getElementById('mm-autocomplete-clone'));


                PubSub.publish("components.loader.remove", { key: "componente.autocompleteuser.search" } );
	    	})
    	}, 200);

    	
    },

    _userClick: function(user, event) {
		PubSub.publishSync( ( this.props.pubsub && this.props.pubsub.selectuser ?  this.props.pubsub.selectuser : "component.autocompleteuser.selected"), { user: user } );
        $("#mm-autocomplete-clone").html("");
   	},
    
    getInitialState: function() {
        return {suggestions: [], required: this.props.required };
    },


    componentDidMount: function() {

        if($("#mm-autocomplete-clone").length == 0) {
            $("body").append("<div id='mm-autocomplete-clone'></div>");
            $("body").on("click.mmautocomplete", function(event) {
                var target = $(event.target); 
                if(target.parents('div#mm-autocomplete-clone').length == 0) {
                    $("#mm-autocomplete-clone").html("");
                }
                
            });
        }
    },


    render: function() {
        
        return <div className={ "mm-autocomplete-user" } >
        			<input name={ this.props.inputname || "keywords" } ref="input" maxLength={ this.props.maxlength } required={ this.state.required } onKeyUp={ this._keyUp } />
               </div>;
    }
});

module.exports = AutocompleteUser;