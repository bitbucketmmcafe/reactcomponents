var Widget = React.createClass({

    WidgetHeader : React.createClass({

        toggleMinimize: function() {
            this.setState({ collapsed: !this.state.collapsed });
            this.props.updateCollapse(!this.state.collapsed);
        },
        removeWidget: function() {
            PubSub.publishSync( 'widget.removed', { widget_id: this.props.widget_id } );
        },
        getInitialState: function() {
            return {label: "", buttons: {}, collapsed: this.props.collapsed, overridetooltip: null, type: this.props.type };
        },
        componentDidMount: function() {
            this.setState( {label: "" } );
        },
        render: function() {
            var buttons = [],
                div = React.createFactory('div');
            if(this.state.buttons.minimize) {
                var classname = "wigdet_header_minimize";

                this.state.collapsed ? classname += " widget_header_collapsed" : classname += " widget_header_contracted";
                
                buttons.push(<div className={ classname } key="wigdet_header_minimize" onClick={this.toggleMinimize} ></div>); 
            }
            if(this.state.buttons.tooltip)
                buttons.push(div({ className: "wigdet_header_tooltip highlight", key: "wigdet_header_tooltip", "data-overridetooltip": this.state.overridetooltip, "data-desc":  MMCafeReact.Components.Translator.getI18n('tooltip.'+ this.state.type) }));  
            
            if(this.state.buttons.remove)
                buttons.push(<div className="wigdet_header_close" key="wigdet_header_remove" onClick={this.removeWidget} ></div>); 
            
            return (
                <header className= { this.props.draggable ? "widget-drag-handler" : "" }>
                    { this.state.label || this.props.datatitle }
                    <div>
                    { buttons }
                    </div>
                </header>);
        }
    }),

    WidgetFactory: function(key, opts) {
        var keys = {
            birthdaycalendar: "WidgetContentBirthdayCalendar",
            birthdayofhiringcalendar: "WidgetContentBirthdayCalendar",
            birthdays: "WidgetContentBirthdays",
            birthdaysofhiring: "WidgetContentBirthdays",
            calendar: "WidgetContentCalendar",
            ocurrencecalendar: "WidgetContentOcurrenceCalendar",
            carousel: "WidgetContentCarousel",
            cards: "WidgetContentAbilityCards",
            contacts: "WidgetContentContacts",
            dynamiclinks:"WidgetContentDynamicLinks",
            favorites: "WidgetContentFavorites",
            feed: "WidgetContentFeeds",
            userfeed: "WidgetContentUserFeeds",
            forum: "WidgetContentForum",
            incidents: "WidgetContentIncidents",
            notepad: "WidgetContentNotepad",
            lastcomments: "WidgetContentLastComments",
            lastupdates: "WidgetContentLastUpdates",
            links: "WidgetContentLinks",
            mostviews: "WidgetContentMostViews",
            news: "WidgetContentNews",
            plain: "WidgetContentPlain",
            offerrides: "WidgetContentOfferrides",
            ourteam: "WidgetContentOurTeam",
            outdoor: "WidgetContentOutDoor",
            poll: "WidgetContentPoll",
            praises: "WidgetContentPraises",
            tags: "WidgetContentTags",
            tasks: "WidgetContentTasks",
            tasksmanager: "WidgetContentTasksmanager",
            vacations: "WidgetContentVacations",
            usersearch: "WidgetContentUserSearch",
            warnings: "WidgetContentWarnings",
            weather: "WidgetContentWeather",
            requiredread: "WidgetContentRequiredRead",
            dashboardtasks: "WidgetContentDashboardTasks"
        }
        var W = MMCafeReact.Widgets[keys[key]];
        return <W ref="content" widget_id={ opts.widget_id } key={ opts.key } html={ opts.html } isExpanded={ opts.isExpanded} isVisible={ opts.isVisible} data={ opts.data } />;
    },

    toggleVisible: function() {
        this.setState({ expanded: !this.state.expanded });
    },

    handleToggleClick: function(event) {
        this.setState({ expanded: !this.state.expanded });
    },
    handleUpdateCollapse: function(collapsed) {
        this.setState({ collapsed: collapsed });
    },
    isExpanded: function() {
        return this.state.expanded;
    },
    isVisible: function() {
        return this.state.expanded && this.state.collapsed;
    },
	getInitialState: function() {

        var expanded = (this.props.expanded == undefined) ? true : this.props.expanded;

        if(this.props.remote) {
            return { html: '<span className="mm-widget-notready"></span>',
                     type: null,
                     expanded: expanded,
                     collapsed: true,
                     disabled: false,
                     customicon: this.props.data ? this.props.data.customicon : this.props.customicon,
                     buttons: { minimize : false, tooltip: false } };
        } else {
            return { html: this.props.html || '<span className="mm-widget-notready"></span>',
                     title: this.props.title,
                     type: this.props.type ? this.props.type : "plain",
                     expanded: expanded,
                     collapsed: true,
                     disabled: false,
                     customicon: this.props.data ? this.props.data.customicon : this.props.customicon,
                     buttons: this.props.buttons || { minimize : false, tooltip: false }
                };
        }
        
    	
    },
	componentDidMount: function() {
    	var buttons = {}, title = "", type = "";
        if(this.props.remote && this.state.expanded) {
            $.ajax({
                url: this.props.source, dataType: "json"
            })
            .then(function(response) {
                if(!response)
                    return false;
                if(response.disabled) {
                    this.setState({ disabled: true })
                } else {
                    this.setState({ html: '<span className="mm-widget-notready"></span>',
                         title: response.title,
                         html: response.html,
                         type: response.type ? response.type : "plain",
                         expanded: response.expanded || true,
                         customicon: response.customicon,
                         buttons: response.buttons || { minimize : false, tooltip: false }
                    });

                    if(response.data.html)
                        this.refs.content.setState({ html: response.data.html });
                    if(response.data.customicon)
                        this.setState({ customicon: response.data.customicon });
                    if(this.refs.content.remoteData)
                        this.refs.content.remoteData(response.data);
                    if(this.refs.header)
                        this.refs.header.setState({ label: this.state.title, buttons: this.state.buttons, overridetooltip: response.overridetooltip });
                }
            }.bind(this))
            .fail(function(error) {
                console.error("Error retrieving portlet", this.props.source);
                PubSub.publishSync("widget.async_rendered");
                 this.setState({ failed: true });
            }.bind(this))
            .done(function() {
                PubSub.publishSync("widget.async_rendered");
            });
            
        } else {

            if(this.refs.content.remoteData)
                this.refs.content.remoteData(this.props.data);
            if(this.refs.header)
                this.refs.header.setState({ label: this.state.title, buttons: this.state.buttons });
        }

    },
    render: function() {
        var key = "widget_header_" + this.props.widget_id,
            ckey = "widget_content_" + this.props.widget_id,
            widgetcontent = null,
            classname = "widget widget_plain";


        if(this.state.type && !this.state.failed) {
            widgetcontent = this.WidgetFactory(this.state.type, { key: ckey, html: this.state.html, isExpanded: this.isExpanded, isVisible: this.isVisible, data: this.props.data || null, widget_id: this.props.widget_id });
            classname = "widget widget_"+ this.state.type;
        } else {
            widgetcontent = this.WidgetFactory("plain", { key: ckey, html: this.state.html, isExpanded: this.isExpanded, isVisible: this.isVisible, data: this.props.data || null });
        }
        if(this.state.disabled || this.state.failed) {
            classname = classname += " widget_disabled";
        } else {
            this.state.expanded ? classname += " widget_expanded" : classname += " widget_notexpanded";
            this.state.collapsed ? classname += " widget_collapsed" : classname += " widget_contracted";     
        }

        if(this.state.customicon) {
            classname += " widget_icon_"+this.state.customicon;
        }
        var WidgetHeader = this.WidgetHeader;
        
        return (<div draggable={ this.props.draggable } className={ classname } data-id={ this.props.widget_id} data-type={ this.state.type } >
            {this.state.type && this.state.type!="outdoor" && 
            <WidgetHeader
                ref="header"
                key={ key }
                type={this.state.type}
                widget_id={ this.props.widget_id }
                collapsed={ this.state.collapsed }
                updateCollapse={ this.handleUpdateCollapse }
                removeWidget={ this.props.handleRemoveWidget }
                draggable={ this.props.draggable }
                datatitle={this.props.title}
            ></WidgetHeader>}
            
            { widgetcontent }
            
        </div>);

    }
});

module.exports = Widget;