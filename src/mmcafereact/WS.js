var WS = {};

WS.Column = require("../Column");
WS.ExpandableColumn = require("../ExpandableColumn");
WS.Widget = require("../Widget");
WS.WidgetsManager = require("../WidgetsManager");
WS.Workspace = require("../Workspace");

module.exports = WS;