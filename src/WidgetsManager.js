
var WidgetsManager = React.createClass({

    _addWidget: function(widget) {
        PubSub.publish("widget.added", { widget_id: widget })
    },

	componentWillMount: function() {

        PubSub.subscribe("workspace.not_rendered_widgets", function(msg, data) {
            this.setState({ to_show_widgets: data.widgets}) 
        }.bind(this));

    },

    toggleView: function() {
        this.setState({ expanded: !this.state.expanded })
    },
    
    getInitialState: function() {

        var all_widgets_json = {};
        for(var i=0; i<this.props.allwidgets.length; i++) {
            var widget = this.props.allwidgets[i];
            all_widgets_json[widget.id] = { name: widget.name };
        }
        return {to_show_widgets: null, all_widgets_json: all_widgets_json, expanded: true };

    },
    
 	render: function() {

        var nodes = [], all_widgets_json = this.state.all_widgets_json, addWidget = this._addWidget, className = "";

        if(this.state.to_show_widgets) {
            nodes = this.state.to_show_widgets.map(function(widget) {
                return <li key={ "widget_" + widget } onClick={ addWidget.bind(null, widget) }>
                        { all_widgets_json[widget].name }
                </li>;
            });

            if(this.state.to_show_widgets.length > 3) {
                nodes.push(<li key={ "widget_toggle" } className="togglelink" onClick={ this.toggleView }>
                    <i className="fa fa-ellipsis-h"></i>
                </li>);

            } 
        }

    	return  (
        	<div className={ this.state.expanded ? "expanded" : "" }>
            { this.state.to_show_widgets == null && "nenhum portlet a adicionar"}
            { nodes }
            </div>
        );

    }
});

module.exports = WidgetsManager;