var Workspace = React.createClass({

    widgets_count: null,
    dragSrcEl: null,
    isMovingOn: null,

    _build_data: function(full_url) {
        var arr = full_url.split("?")

        var data = {}
        var params = arr[1].split("&")
        for(var i=0; i<params.length; i++) {
            var d = params[i].split('=')
            data[d[0]] = d[1]
        }

        return { url: arr[0], data: data }
    },

    draghandlers: function(node) {
        var self = this;

        function start(e) {
            self.dragSrcEl = this;
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData('text/html', this.innerHTML);
        }

        function enter(e) {
            console.log("oioi")

            var droppable = $(e.target).parents('.widget');
            if(!$(droppable).hasClass("moving")) {

                if (self.isMovingOn && self.isMovingOn.length > 0 && self.isMovingOn[0]!=self.dragSrcEl) {
                    self.isMovingOn[0].style.marginTop = 0;
                    self.isMovingOn[0].style.marginBottom = "20px";
                    self.isMovingOn.removeClass("moving");
                }

                $(droppable).addClass("moving");

                self.isMovingOn = $(droppable);
                
                if(droppable.length > 0 && droppable[0]!=self.dragSrcEl) {
                    
                    var height = droppable[0].offsetHeight, top = $(droppable).offset().top;

                    if(e.pageY > top - height / 2)
                        droppable[0].style.marginTop = self.dragSrcEl.offsetHeight + "px";
                    else
                        droppable[0].style.marginBottom = (self.dragSrcEl.offsetHeight + 20) + "px";
                }

            } 

           return false;

        }

        function over(e) {
            if (e.preventDefault) {
                e.preventDefault(); // Necessary. Allows us to drop.
            }
            e.dataTransfer.dropEffect = 'move';

            return false;
        }

        function drop(e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            }
     
            return false;
        }

        function end(e) {

        
            //checa se fez drop em cima dele mesmo
           var coordinates= {
                start : {
                    y: $(self.dragSrcEl).offset().top,
                    x: $(self.dragSrcEl).offset().left
                },
                end: {
                    y: $(self.dragSrcEl).offset().top + $(self.dragSrcEl).outerHeight(),
                    x: $(self.dragSrcEl).offset().left + $(self.dragSrcEl).outerWidth()
                }
           }
          
           var isinside = (e.clientY >= coordinates.start.y && e.clientY <= coordinates.end.y) &&  (e.clientX >= coordinates.start.x && e.clientX <= coordinates.end.x);
          
           if(isinside) {
                if (self.isMovingOn && self.isMovingOn.length > 0 && self.isMovingOn[0]!=self.dragSrcEl) {
                    self.isMovingOn[0].style.marginTop = 0;
                    self.isMovingOn[0].style.marginBottom = "20px";
                    self.isMovingOn.removeClass("moving");
                }
                return false;
           }

            //checa se está fazendo drop num local válido     
            if (self.isMovingOn && self.isMovingOn.length && (e.srcElement != self.isMovingOn[0]) && (self.dragSrcEl != self.isMovingOn[0])) {
          
            
                var allcolwidgets = self.state.widgets;
                
                //self.dragSrcEl.style.opacity = "0";


                //remove from previous location
                for(key in self.state.widgets) {
                    var arr = [], colwidgets = self.state.widgets[key];
                    for(var i=0; i<colwidgets.length; i++) {
                       if(colwidgets[i]!=$(self.dragSrcEl).data("id"))
                        arr.push(colwidgets[i]);
                    }
                    allcolwidgets[key] = arr;
                }


                if($(self.isMovingOn).hasClass("widget_ghost")) {
                    allcolwidgets[$(self.isMovingOn).parent().data("id")] = [$(self.dragSrcEl).data("id")];
                } else {
                
                    for(key in self.state.widgets) {
                        var arr = [], colwidgets = self.state.widgets[key];
                        for(var i=0; i<colwidgets.length; i++) {
                   
                            if(colwidgets[i]==$(self.isMovingOn).data("id"))
                                arr.push($(self.dragSrcEl).data("id"));
                               arr.push(colwidgets[i]);
                        }
                        allcolwidgets[key] = arr;
                    }

                }

               self.setState({ widgets: allcolwidgets });

               if (self.isMovingOn && self.isMovingOn.length > 0 && self.isMovingOn[0]!=self.dragSrcEl) {
                    self.isMovingOn[0].style.marginTop = 0;
                    self.isMovingOn[0].style.marginBottom = "20px";
                    self.isMovingOn.removeClass("moving");
                }


            } else {
                if (self.isMovingOn && self.isMovingOn.length > 0 && self.isMovingOn[0]!=self.dragSrcEl) {
                    self.isMovingOn[0].style.marginTop = 0;
                    self.isMovingOn[0].style.marginBottom = "20px";
                    self.isMovingOn.removeClass("moving");
                }
            }
            $(e.srcElement).removeClass("moving")
            self.isMovingOn = null;
        }

        node.removeEventListener('dragstart', start);
        node.removeEventListener('dragenter', enter);
        node.removeEventListener('dragover', over);
        node.removeEventListener('drop', drop);
        node.removeEventListener('dragend', end);


        node.addEventListener('dragstart', start, false);
        node.addEventListener('dragenter', enter, false);
        node.addEventListener('dragover', over, false);
        node.addEventListener('drop', drop, false);
        node.addEventListener('dragend', end, false);

        //node.addEventListener('dragleave', function(e) {
        //}, false);

        
    },
    
    drag: function() {

        var widgets = $('.workspace .widget');
        var self = this;
        [].forEach.call(widgets, function(widget) {
            self.draghandlers(widget);
        });
    },

    reset: function() {
        var initwidgets = this.props.defaultworkspace;
        this.setState({columns: this.props.columns || [], widgets: initwidgets });
    },

    updatePosition: function() {

        var positions = {};
        this.state.columns.forEach(function(column) {
            var node = this.refs["column_" + column].getDOMNode(),
                widgets = node.querySelectorAll(".widget"),
                array = Array.prototype.map.call(widgets, function(item) {
                    if(item.getAttribute("data-id"))
                        return (item.getAttribute("data-id"));
                });
            positions[column] = array;

        }.bind(this));

        var ret = this._build_data(this.props.source.updateposition)

        $.ajax({
            url: ret.url, data: {method: ret.data.method, authenticityToken: ret.data.authenticityToken, key: "positions_" + this.props.homepageId, "valor": JSON.stringify(positions) }, type: "post"
        })
        .done(function(response) {
           //console.log("Workspace updated");
        })
        .fail(function(jqXHR) {
            console.error("Error whyle trying to update workspace positions". jqXHR);
        });
    },

    /*
    doDraggable: function() {

        this.drag();

    },
    */

    componentDidMount: function() {
        var self = this;

        PubSub.subscribe( 'workspace.reset', function(msg, data) {
            this.reset();
        }.bind(this));

        PubSub.subscribe( 'widget.removed', function(msg, data) {
            
            var allcolwidgets = self.state.widgets;
            for(key in self.state.widgets) {
 
                var arr = [], colwidgets = self.state.widgets[key];
                for(var i=0; i<colwidgets.length; i++) {
                   if(colwidgets[i]!=data.widget_id)
                    arr.push(colwidgets[i]);
                }
                allcolwidgets[key] = arr;
             
            }
    
            self.setState({ widgets: allcolwidgets });
        });

        PubSub.subscribe( 'widget.added', function(msg, data) {
            for(key in self.props.defaultworkspace) {

                if(self.props.defaultworkspace[key].indexOf(data.widget_id)>=0) {
                    var allcolwidgets = self.state.widgets;
                    if(!allcolwidgets[key])
                        allcolwidgets[key] = []
                    allcolwidgets[key].push(data.widget_id);
                    self.setState({ widgets: allcolwidgets })
                }
            }

        })

        this.drag();

        var counter = 0;
        PubSub.subscribe("widget.async_rendered", function(msg, data) {
            counter++;
            if(counter == self.widgets_count) {
                PubSub.publish("workspace.async_ready");
            }
        });

        var count = 0,
            widgets = this.state.widgets;
        this.state.columns.forEach(function(column) {
            if(widgets[column] && widgets[column][0]!=null)
                count+= widgets[column].length;
        })
        this.widgets_count = count;


        PubSub.subscribe("widget.hideme", function(msg, data) {
            $(self.getDOMNode()).find("div[data-id='" + data.widget_id + "']").addClass("hidden");
        });
        this.updatePosition();
    },


    componentDidUpdate: function() {


        //this.doDraggable();
        this.drag();
        this.updatePosition();

    },

    getInitialState: function() {
        var userworkCollums = Object.keys(this.props.userworkspace),
            defaultCollums = Object.keys(this.props.defaultworkspace),
            arr = new Array;
        
        defaultCollums.forEach(function(columnid) {

            userworkCollums.filter(function(n){
                if(n == columnid) {arr.push(n)}
            });
            
        });

        var initwidgets = userworkCollums.length > 0 ? this.props.userworkspace : this.props.defaultworkspace;

        return {columns: this.props.columns || [], widgets: initwidgets };
    },

    componentWillUnmount: function() {
        PubSub.unsubscribe( 'widget.removed' );
        PubSub.unsubscribe( 'widget.added' );
    },

    render: function() {


        var cols = [];
        var classname = this.props.column_classname, 
            widgetsource = this.props.source.widgetsource;


        var not_rendered_widgets = [], rendered_widgets = [], all_widgets = [];

        var widgets = []; 
        this.state.columns.map(function(column) {


            widgets = this.state.widgets[column];
        
            var c = <MMCafeReact.WS.Column 
                            key={column}
                            ref={"column_" + column}
                            col_id={column}
                            col_classname={ classname + " mm-ws-col" }
                            widgetRemoved={ this.widgetRemoved }
                            widgetSource={ widgetsource }
                            widgets={ widgets }
                            >
                      </MMCafeReact.WS.Column>
            cols.push(c);

            rendered_widgets.push(widgets);
            all_widgets.push(this.props.defaultworkspace[column]);

           

        }, this);

        rendered_widgets = (Array.prototype.concat.apply([], rendered_widgets));
        all_widgets = (Array.prototype.concat.apply([], all_widgets));

        rendered_widgets = rendered_widgets.map(function(s) {
            return parseInt(s);
        });

        all_widgets.filter(function(item, index) {
            if(rendered_widgets.indexOf(item)<0)
                not_rendered_widgets.push(item);
        })

        PubSub.publish("workspace.not_rendered_widgets", { widgets: not_rendered_widgets });
        
        return  (
            <div className={ "row" }>
            { cols }
            </div>
        );

    }
});

module.exports = Workspace;