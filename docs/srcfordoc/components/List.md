List 
================= 
### Description 
Component para lista 
### Properties 

#### items 
Array com items da lista, cada lista é um json: data = [ { text: "texto do link" }, external: false, thumb: "http://urldeimagem", description: "texto adicional" } ] 
- type     : array 
- required : false- default  : not degined 
- computed : not degined 

#### classname 
classe css: classname = "col-md-3" 
- type     : string 
- required : false- default  : not degined 
- computed : not degined 
